//
//  Result.swift
//  Fleet Management
//
//  Created by Atta Khan on 15/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import Foundation
struct Result : Codable {
	let totalCount : Int?
	let items : [Route]?

	enum CodingKeys: String, CodingKey {

		case totalCount = "totalCount"
		case items = "items"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		totalCount = try values.decodeIfPresent(Int.self, forKey: .totalCount)
		items = try values.decodeIfPresent([Route].self, forKey: .items)
	}

}
