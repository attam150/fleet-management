//
//  ScheduleSpecifics.swift
//  Fleet Management
//
//  Created by Atta Khan on 17/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import Foundation
struct ScheduleSpecifics : Codable {
	var weekday : String = ""
	var pickupTime : String = ""
	var pickupTripId : Int = 0
	var dropoffTime : String = ""
	var dropoffTripId : Int = 0
    var id: String?
}
