//
//  PastScheduleResult.swift
//  Fleet Management
//
//  Created by Atta Khan on 17/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import Foundation
struct PastScheduleResult : Codable {
	let totalCount : Int?
	let items : [PastScheduleItems]?
    let tripExecution : PastTripExecution?
    let tripExecutionId : String?
    let passenger : Passenger?
    let passengerId : Int?
    let schedule : String?
    let scheduleId : String?
    let tripId : Int?
    let trip : Trip?
    let date : String?
    let pickupTime : String?
    let dropTime : String?
    let type : String?
    let pickupLat : Int?
    let pickupLong : Int?
    let dropLat : Int?
    let dropLong : Int?
    let status : Int?
    let tenantId : Int?

	enum CodingKeys: String, CodingKey {

		case totalCount = "totalCount"
		case items = "items"
        case tripExecution = "tripExecution"
        case tripExecutionId = "tripExecutionId"
        case passenger = "passenger"
        case passengerId = "passengerId"
        case schedule = "schedule"
        case scheduleId = "scheduleId"
        case tripId = "tripId"
        case trip = "trip"
        case date = "date"
        case pickupTime = "pickupTime"
        case dropTime = "dropTime"
        case type = "type"
        case pickupLat = "pickupLat"
        case pickupLong = "pickupLong"
        case dropLat = "dropLat"
        case dropLong = "dropLong"
        case status = "status"
        case tenantId = "tenantId"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		totalCount = try values.decodeIfPresent(Int.self, forKey: .totalCount)
		items = try values.decodeIfPresent([PastScheduleItems].self, forKey: .items)
        tripExecution = try values.decodeIfPresent(PastTripExecution.self, forKey: .tripExecution)
        tripExecutionId = try values.decodeIfPresent(String.self, forKey: .tripExecutionId)
        passenger = try values.decodeIfPresent(Passenger.self, forKey: .passenger)
        passengerId = try values.decodeIfPresent(Int.self, forKey: .passengerId)
        schedule = try values.decodeIfPresent(String.self, forKey: .schedule)
        scheduleId = try values.decodeIfPresent(String.self, forKey: .scheduleId)
        tripId = try values.decodeIfPresent(Int.self, forKey: .tripId)
        trip = try values.decodeIfPresent(Trip.self, forKey: .trip)
        date = try values.decodeIfPresent(String.self, forKey: .date)
        pickupTime = try values.decodeIfPresent(String.self, forKey: .pickupTime)
        dropTime = try values.decodeIfPresent(String.self, forKey: .dropTime)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        pickupLat = try values.decodeIfPresent(Int.self, forKey: .pickupLat)
        pickupLong = try values.decodeIfPresent(Int.self, forKey: .pickupLong)
        dropLat = try values.decodeIfPresent(Int.self, forKey: .dropLat)
        dropLong = try values.decodeIfPresent(Int.self, forKey: .dropLong)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        tenantId = try values.decodeIfPresent(Int.self, forKey: .tenantId)
	}

}
