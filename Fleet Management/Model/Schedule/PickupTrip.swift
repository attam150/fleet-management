/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct PickupTrip : Codable {
	let creationTime : String?
	let name : String?
	let tripStartAddress : String?
	let tripEndAddress : String?
	let startTime : String?
	let route : Route?
	let routeId : Int?
	let passengerCount : Int?
	let driverName : String?
	let driverSurname : String?
	let vehicle : String?
	let driverId : Int?
	let isActive : Bool?
	let passengers : String?
	let type : Int?
	let id : Int?

	enum CodingKeys: String, CodingKey {

		case creationTime = "creationTime"
		case name = "name"
		case tripStartAddress = "tripStartAddress"
		case tripEndAddress = "tripEndAddress"
		case startTime = "startTime"
		case route = "route"
		case routeId = "routeId"
		case passengerCount = "passengerCount"
		case driverName = "driverName"
		case driverSurname = "driverSurname"
		case vehicle = "vehicle"
		case driverId = "driverId"
		case isActive = "isActive"
		case passengers = "passengers"
		case type = "type"
		case id = "id"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		creationTime = try values.decodeIfPresent(String.self, forKey: .creationTime)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		tripStartAddress = try values.decodeIfPresent(String.self, forKey: .tripStartAddress)
		tripEndAddress = try values.decodeIfPresent(String.self, forKey: .tripEndAddress)
		startTime = try values.decodeIfPresent(String.self, forKey: .startTime)
		route = try values.decodeIfPresent(Route.self, forKey: .route)
		routeId = try values.decodeIfPresent(Int.self, forKey: .routeId)
		passengerCount = try values.decodeIfPresent(Int.self, forKey: .passengerCount)
		driverName = try values.decodeIfPresent(String.self, forKey: .driverName)
		driverSurname = try values.decodeIfPresent(String.self, forKey: .driverSurname)
		vehicle = try values.decodeIfPresent(String.self, forKey: .vehicle)
		driverId = try values.decodeIfPresent(Int.self, forKey: .driverId)
		isActive = try values.decodeIfPresent(Bool.self, forKey: .isActive)
		passengers = try values.decodeIfPresent(String.self, forKey: .passengers)
		type = try values.decodeIfPresent(Int.self, forKey: .type)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
	}

}