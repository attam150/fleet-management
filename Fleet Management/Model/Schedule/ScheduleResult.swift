//
//  ScheduleResult.swift
//  Fleet Management
//
//  Created by Atta Khan on 17/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import Foundation
struct ScheduleResult : Codable {
	let scheduleStartDate : String?
	let scheduleEndDate : String?
	let serviceType : ServicesType?
	let pickupLat : Double?
	let pickupLong : Double?
	let dropLat : Double?
	let dropLong : Double?
	let userName : String?
	let userId : Int?
	let user : ScheduleUser?
	let scheduleStatus : ScheduleStatus?
	let tenantId : Int?
	let scheduleSpecifics : [UpdateScheduleSpecifics]?
	let id : Int?

	enum CodingKeys: String, CodingKey {

		case scheduleStartDate = "scheduleStartDate"
		case scheduleEndDate = "scheduleEndDate"
		case serviceType = "serviceType"
		case pickupLat = "pickupLat"
		case pickupLong = "pickupLong"
		case dropLat = "dropLat"
		case dropLong = "dropLong"
		case userName = "userName"
		case userId = "userId"
		case user = "user"
		case scheduleStatus = "scheduleStatus"
		case tenantId = "tenantId"
		case scheduleSpecifics = "scheduleSpecifics"
		case id = "id"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		scheduleStartDate = try values.decodeIfPresent(String.self, forKey: .scheduleStartDate)
		scheduleEndDate = try values.decodeIfPresent(String.self, forKey: .scheduleEndDate)
		serviceType = try values.decodeIfPresent(ServicesType.self, forKey: .serviceType)
		pickupLat = try values.decodeIfPresent(Double.self, forKey: .pickupLat)
		pickupLong = try values.decodeIfPresent(Double.self, forKey: .pickupLong)
		dropLat = try values.decodeIfPresent(Double.self, forKey: .dropLat)
		dropLong = try values.decodeIfPresent(Double.self, forKey: .dropLong)
		userName = try values.decodeIfPresent(String.self, forKey: .userName)
		userId = try values.decodeIfPresent(Int.self, forKey: .userId)
		user = try values.decodeIfPresent(ScheduleUser.self, forKey: .user)
		scheduleStatus = try values.decodeIfPresent(ScheduleStatus.self, forKey: .scheduleStatus)
		tenantId = try values.decodeIfPresent(Int.self, forKey: .tenantId)
		scheduleSpecifics = try values.decodeIfPresent([UpdateScheduleSpecifics].self, forKey: .scheduleSpecifics)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
	}

}
