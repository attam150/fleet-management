//
//  UpdateScheduleModel.swift
//  Fleet Management
//
//  Created by Atta khan on 07/06/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import Foundation
struct UpdateScheduleModel : Codable {
    let result : UpdateScheduleResult?
    let targetUrl : String?
    let success : Bool?
    let error : ErrorModel?
    let unAuthorizedRequest : Bool?
    let __abp : Bool?

    enum CodingKeys: String, CodingKey {

        case result = "result"
        case targetUrl = "targetUrl"
        case success = "success"
        case error = "error"
        case unAuthorizedRequest = "unAuthorizedRequest"
        case __abp = "__abp"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        result = try values.decodeIfPresent(UpdateScheduleResult.self, forKey: .result)
        targetUrl = try values.decodeIfPresent(String.self, forKey: .targetUrl)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
        error = try values.decodeIfPresent(ErrorModel.self, forKey: .error)
        unAuthorizedRequest = try values.decodeIfPresent(Bool.self, forKey: .unAuthorizedRequest)
        __abp = try values.decodeIfPresent(Bool.self, forKey: .__abp)
    }

}
struct UpdateScheduleResult : Codable {
    let scheduleStartDate : String?
    let scheduleEndDate : String?
    let serviceType : ServicesType?
    let pickupLat : Double?
    let pickupLong : Double?
    let dropLat : Double?
    let dropLong : Double?
    let userName : String?
    let userId : Int?
    let user : ScheduleUser?
    let scheduleStatus : Int?
    let tenantId : Int?
    let scheduleSpecifics : [UpdateScheduleSpecifics]?
    let id : Int?

    enum CodingKeys: String, CodingKey {

        case scheduleStartDate = "scheduleStartDate"
        case scheduleEndDate = "scheduleEndDate"
        case serviceType = "serviceType"
        case pickupLat = "pickupLat"
        case pickupLong = "pickupLong"
        case dropLat = "dropLat"
        case dropLong = "dropLong"
        case userName = "userName"
        case userId = "userId"
        case user = "user"
        case scheduleStatus = "scheduleStatus"
        case tenantId = "tenantId"
        case scheduleSpecifics = "scheduleSpecifics"
        case id = "id"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        scheduleStartDate = try values.decodeIfPresent(String.self, forKey: .scheduleStartDate)
        scheduleEndDate = try values.decodeIfPresent(String.self, forKey: .scheduleEndDate)
        serviceType = try values.decodeIfPresent(ServicesType.self, forKey: .serviceType)
        pickupLat = try values.decodeIfPresent(Double.self, forKey: .pickupLat)
        pickupLong = try values.decodeIfPresent(Double.self, forKey: .pickupLong)
        dropLat = try values.decodeIfPresent(Double.self, forKey: .dropLat)
        dropLong = try values.decodeIfPresent(Double.self, forKey: .dropLong)
        userName = try values.decodeIfPresent(String.self, forKey: .userName)
        userId = try values.decodeIfPresent(Int.self, forKey: .userId)
        user = try values.decodeIfPresent(ScheduleUser.self, forKey: .user)
        scheduleStatus = try values.decodeIfPresent(Int.self, forKey: .scheduleStatus)
        tenantId = try values.decodeIfPresent(Int.self, forKey: .tenantId)
        scheduleSpecifics = try values.decodeIfPresent([UpdateScheduleSpecifics].self, forKey: .scheduleSpecifics)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
    }

}
struct UpdateScheduleSpecifics : Codable {
    var weekday : String = ""
    let pickupTime : String?
    let pickupTripId : Int?
    let dropoffTime : String?
    let dropoffTripId : Int?
    let pickupTrip : PickupTrip?
    let dropoffTrip : DropoffTrip?
    let id : String?

    enum CodingKeys: String, CodingKey {

        case weekday = "weekday"
        case pickupTime = "pickupTime"
        case pickupTripId = "pickupTripId"
        case dropoffTime = "dropoffTime"
        case dropoffTripId = "dropoffTripId"
        case pickupTrip = "pickupTrip"
        case dropoffTrip = "dropoffTrip"
        case id = "id"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        weekday = try values.decodeIfPresent(String.self, forKey: .weekday)!
        pickupTime = try values.decodeIfPresent(String.self, forKey: .pickupTime)
        pickupTripId = try values.decodeIfPresent(Int.self, forKey: .pickupTripId)
        dropoffTime = try values.decodeIfPresent(String.self, forKey: .dropoffTime)
        dropoffTripId = try values.decodeIfPresent(Int.self, forKey: .dropoffTripId)
        pickupTrip = try values.decodeIfPresent(PickupTrip.self, forKey: .pickupTrip)
        dropoffTrip = try values.decodeIfPresent(DropoffTrip.self, forKey: .dropoffTrip)
        id = try values.decodeIfPresent(String.self, forKey: .id)
    }

}
