//
//  ScheduleModel.swift
//  Fleet Management
//
//  Created by Atta Khan on 17/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import Foundation
struct  ScheduleModel : Codable {
	var scheduleStartDate : String = ""
	var scheduleEndDate : String = ""
	var serviceType : Int = 1
	var scheduleSpecifics : [ScheduleSpecifics] = []
	var userId : Int?
    var id: Int?
}
