//
//  CreateScheduleModel.swift
//  Fleet Management
//
//  Created by Atta Khan on 17/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import Foundation
struct PastTripExecution : Codable {
	let creationTime : String?
	let trip : String?
	let driver : Driver?
	let startTime : String?
	let endTime : String?
	let status : Int?
	let driverName : String?
	let tripExecutionDrivers : [String]?
	let id : String?

	enum CodingKeys: String, CodingKey {

		case creationTime = "creationTime"
		case trip = "trip"
		case driver = "driver"
		case startTime = "startTime"
		case endTime = "endTime"
		case status = "status"
		case driverName = "driverName"
		case tripExecutionDrivers = "tripExecutionDrivers"
		case id = "id"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		creationTime = try values.decodeIfPresent(String.self, forKey: .creationTime)
		trip = try values.decodeIfPresent(String.self, forKey: .trip)
		driver = try values.decodeIfPresent(Driver.self, forKey: .driver)
		startTime = try values.decodeIfPresent(String.self, forKey: .startTime)
		endTime = try values.decodeIfPresent(String.self, forKey: .endTime)
		status = try values.decodeIfPresent(Int.self, forKey: .status)
		driverName = try values.decodeIfPresent(String.self, forKey: .driverName)
		tripExecutionDrivers = try values.decodeIfPresent([String].self, forKey: .tripExecutionDrivers)
		id = try values.decodeIfPresent(String.self, forKey: .id)
	}

}
