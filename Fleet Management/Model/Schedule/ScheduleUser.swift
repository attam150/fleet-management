//
//  ScheduleUser.swift
//  Fleet Management
//
//  Created by Atta Khan on 17/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import Foundation
struct ScheduleUser : Codable {
	let userName : String?
	let name : String?
	let surname : String?
	let emailAddress : String?
	let isActive : Bool?
	let fullName : String?
	let phoneNumber : String?
	let lastLoginTime : String?
	let creationTime : String?
	let roleNames : [String]?
	let employeeId : String?
	let gender : String?
	let city : String?
	let pickupLat : Double?
	let pickupLong : Double?
	let pickupAddress : String?
	let dropLat : Double?
	let dropLong : Double?
	let dropAddress : String?
	let department : String?
	let entity : String?
	let profilePicture : String?
	let cnic : String?
	let cnicNumber : String?
	let license : String?
	let licenseNumber : String?
	let route : Route?
	let routeId : Int?
	let id : Int?

	enum CodingKeys: String, CodingKey {

		case userName = "userName"
		case name = "name"
		case surname = "surname"
		case emailAddress = "emailAddress"
		case isActive = "isActive"
		case fullName = "fullName"
		case phoneNumber = "phoneNumber"
		case lastLoginTime = "lastLoginTime"
		case creationTime = "creationTime"
		case roleNames = "roleNames"
		case employeeId = "employeeId"
		case gender = "gender"
		case city = "city"
		case pickupLat = "pickupLat"
		case pickupLong = "pickupLong"
		case pickupAddress = "pickupAddress"
		case dropLat = "dropLat"
		case dropLong = "dropLong"
		case dropAddress = "dropAddress"
		case department = "department"
		case entity = "entity"
		case profilePicture = "profilePicture"
		case cnic = "cnic"
		case cnicNumber = "cnicNumber"
		case license = "license"
		case licenseNumber = "licenseNumber"
		case route = "route"
		case routeId = "routeId"
		case id = "id"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		userName = try values.decodeIfPresent(String.self, forKey: .userName)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		surname = try values.decodeIfPresent(String.self, forKey: .surname)
		emailAddress = try values.decodeIfPresent(String.self, forKey: .emailAddress)
		isActive = try values.decodeIfPresent(Bool.self, forKey: .isActive)
		fullName = try values.decodeIfPresent(String.self, forKey: .fullName)
		phoneNumber = try values.decodeIfPresent(String.self, forKey: .phoneNumber)
		lastLoginTime = try values.decodeIfPresent(String.self, forKey: .lastLoginTime)
		creationTime = try values.decodeIfPresent(String.self, forKey: .creationTime)
		roleNames = try values.decodeIfPresent([String].self, forKey: .roleNames)
		employeeId = try values.decodeIfPresent(String.self, forKey: .employeeId)
		gender = try values.decodeIfPresent(String.self, forKey: .gender)
		city = try values.decodeIfPresent(String.self, forKey: .city)
		pickupLat = try values.decodeIfPresent(Double.self, forKey: .pickupLat)
		pickupLong = try values.decodeIfPresent(Double.self, forKey: .pickupLong)
		pickupAddress = try values.decodeIfPresent(String.self, forKey: .pickupAddress)
		dropLat = try values.decodeIfPresent(Double.self, forKey: .dropLat)
		dropLong = try values.decodeIfPresent(Double.self, forKey: .dropLong)
		dropAddress = try values.decodeIfPresent(String.self, forKey: .dropAddress)
		department = try values.decodeIfPresent(String.self, forKey: .department)
		entity = try values.decodeIfPresent(String.self, forKey: .entity)
		profilePicture = try values.decodeIfPresent(String.self, forKey: .profilePicture)
		cnic = try values.decodeIfPresent(String.self, forKey: .cnic)
		cnicNumber = try values.decodeIfPresent(String.self, forKey: .cnicNumber)
		license = try values.decodeIfPresent(String.self, forKey: .license)
		licenseNumber = try values.decodeIfPresent(String.self, forKey: .licenseNumber)
		route = try values.decodeIfPresent(Route.self, forKey: .route)
		routeId = try values.decodeIfPresent(Int.self, forKey: .routeId)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
	}

}
