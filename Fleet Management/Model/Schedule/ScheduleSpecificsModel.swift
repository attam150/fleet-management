//
//  ScheduleSpecificsModel.swift
//  Fleet Management
//
//  Created by Atta Khan on 17/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//


import Foundation
struct ScheduleSpecificsModel : Codable {
	let weekday : String?
	let pickupTime : String?
	let pickupTripId : Int?
	let dropoffTime : String?
	let dropoffTripId : Int?
	let pickupTrip : PickupTrip?
	let dropoffTrip : DropoffTrip?

	enum CodingKeys: String, CodingKey {

		case weekday = "weekday"
		case pickupTime = "pickupTime"
		case pickupTripId = "pickupTripId"
		case dropoffTime = "dropoffTime"
		case dropoffTripId = "dropoffTripId"
		case pickupTrip = "pickupTrip"
		case dropoffTrip = "dropoffTrip"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		weekday = try values.decodeIfPresent(String.self, forKey: .weekday)
		pickupTime = try values.decodeIfPresent(String.self, forKey: .pickupTime)
		pickupTripId = try values.decodeIfPresent(Int.self, forKey: .pickupTripId)
		dropoffTime = try values.decodeIfPresent(String.self, forKey: .dropoffTime)
		dropoffTripId = try values.decodeIfPresent(Int.self, forKey: .dropoffTripId)
		pickupTrip = try values.decodeIfPresent(PickupTrip.self, forKey: .pickupTrip)
		dropoffTrip = try values.decodeIfPresent(DropoffTrip.self, forKey: .dropoffTrip)
	}

}
