//
//  CreateScheduleModel.swift
//  Fleet Management
//
//  Created by Atta Khan on 17/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import Foundation
struct PastScheduleItems : Codable {
	let creationTime : String?
	let employeeId : String?
	let fullName : String?
	let tripId : Int?
	let tripName : String?
	let routeName : String?
	let driverName : String?
	let pickupTime : String?
	let dropTime : String?
	let pickupLocation : String?
	let dropLocation : String?
    let routeId: Int?
	let status : PastScheduleTripStatus?
	let id : String?
    let trip: Trip?
    let tripType: TripType?

	enum CodingKeys: String, CodingKey {

		case creationTime = "creationTime"
		case employeeId = "employeeId"
		case fullName = "fullName"
		case tripId = "tripId"
		case tripName = "tripName"
		case routeName = "routeName"
		case driverName = "driverName"
		case pickupTime = "pickupTime"
		case dropTime = "dropTime"
		case pickupLocation = "pickupLocation"
		case dropLocation = "dropLocation"
        case routeId = "routeId"
		case status = "status"
		case id = "id"
        case trip = "trip"
        case tripType = "tripType"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		creationTime = try values.decodeIfPresent(String.self, forKey: .creationTime)
		employeeId = try values.decodeIfPresent(String.self, forKey: .employeeId)
		fullName = try values.decodeIfPresent(String.self, forKey: .fullName)
		tripId = try values.decodeIfPresent(Int.self, forKey: .tripId)
		tripName = try values.decodeIfPresent(String.self, forKey: .tripName)
		routeName = try values.decodeIfPresent(String.self, forKey: .routeName)
		driverName = try values.decodeIfPresent(String.self, forKey: .driverName)
		pickupTime = try values.decodeIfPresent(String.self, forKey: .pickupTime)
		dropTime = try values.decodeIfPresent(String.self, forKey: .dropTime)
		pickupLocation = try values.decodeIfPresent(String.self, forKey: .pickupLocation)
		dropLocation = try values.decodeIfPresent(String.self, forKey: .dropLocation)
        routeId = try values.decodeIfPresent(Int.self, forKey: .routeId)
		status = try values.decodeIfPresent(PastScheduleTripStatus.self, forKey: .status)
		id = try values.decodeIfPresent(String.self, forKey: .id)
        trip = try values.decodeIfPresent(Trip.self, forKey: .trip)
        tripType = try values.decodeIfPresent(TripType.self, forKey: .tripType)
	}

}
