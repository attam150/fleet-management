//
//  UcomingScheduleModel.swift
//  Fleet Management
//
//  Created by Atta Khan on 17/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//


import Foundation
struct UcomingScheduleModel : Codable {
	let result : ScheduleIResult?
	let targetUrl : String?
	let success : Bool?
	let error : String?
	let unAuthorizedRequest : Bool?
	let __abp : Bool?

	enum CodingKeys: String, CodingKey {

		case result = "result"
		case targetUrl = "targetUrl"
		case success = "success"
		case error = "error"
		case unAuthorizedRequest = "unAuthorizedRequest"
		case __abp = "__abp"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		result = try values.decodeIfPresent(ScheduleIResult.self, forKey: .result)
		targetUrl = try values.decodeIfPresent(String.self, forKey: .targetUrl)
		success = try values.decodeIfPresent(Bool.self, forKey: .success)
		error = try values.decodeIfPresent(String.self, forKey: .error)
		unAuthorizedRequest = try values.decodeIfPresent(Bool.self, forKey: .unAuthorizedRequest)
		__abp = try values.decodeIfPresent(Bool.self, forKey: .__abp)
	}

}
struct ScheduleIResult : Codable {
    let totalCount : Int?
    let items : [ScheduleItems]?

    enum CodingKeys: String, CodingKey {

        case totalCount = "totalCount"
        case items = "items"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        totalCount = try values.decodeIfPresent(Int.self, forKey: .totalCount)
        items = try values.decodeIfPresent([ScheduleItems].self, forKey: .items)
    }

}
struct ScheduleItems : Codable {
    let scheduleId : Int?
    let tripDate : String?
    let tripTime : String?
    let name : String?
    let routeId : Int?
    let trip : Trip?
    let status : String?
    let serviceType : Int?
    let driver: Driver?

    enum CodingKeys: String, CodingKey {

        case scheduleId = "scheduleId"
        case tripDate = "tripDate"
        case tripTime = "tripTime"
        case name = "name"
        case routeId = "routeId"
        case trip = "trip"
        case status = "status"
        case serviceType = "serviceType"
        case driver = "driver"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        scheduleId = try values.decodeIfPresent(Int.self, forKey: .scheduleId)
        tripDate = try values.decodeIfPresent(String.self, forKey: .tripDate)
        tripTime = try values.decodeIfPresent(String.self, forKey: .tripTime)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        routeId = try values.decodeIfPresent(Int.self, forKey: .routeId)
        trip = try values.decodeIfPresent(Trip.self, forKey: .trip)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        serviceType = try values.decodeIfPresent(Int.self, forKey: .serviceType)
        driver = try values.decodeIfPresent(Driver.self, forKey: .driver)
    }

}
