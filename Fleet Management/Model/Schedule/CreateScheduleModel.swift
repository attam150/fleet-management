//
//  CreateScheduleModel.swift
//  Fleet Management
//
//  Created by Atta Khan on 17/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import Foundation
struct CreateScheduleModel : Codable {
	let result : ScheduleResult?
	let targetUrl : String?
	let success : Bool?
	let error : ErrorModel?
	let unAuthorizedRequest : Bool?
	let __abp : Bool?

	enum CodingKeys: String, CodingKey {

		case result = "result"
		case targetUrl = "targetUrl"
		case success = "success"
		case error = "error"
		case unAuthorizedRequest = "unAuthorizedRequest"
		case __abp = "__abp"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		result = try values.decodeIfPresent(ScheduleResult.self, forKey: .result)
		targetUrl = try values.decodeIfPresent(String.self, forKey: .targetUrl)
		success = try values.decodeIfPresent(Bool.self, forKey: .success)
		error = try values.decodeIfPresent(ErrorModel.self, forKey: .error)
		unAuthorizedRequest = try values.decodeIfPresent(Bool.self, forKey: .unAuthorizedRequest)
		__abp = try values.decodeIfPresent(Bool.self, forKey: .__abp)
	}

}
