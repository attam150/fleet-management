//
//  Passenger.swift
//  Fleet Management
//
//  Created by Atta Khan on 03/05/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import Foundation
struct Passenger : Codable {
	let userName : String?
	let name : String?
	let surname : String?
	let emailAddress : String?
	let isActive : Bool?
	let fullName : String?
	let phoneNumber : String?
	let lastLoginTime : String?
	let creationTime : String?
	let roleNames : String?
	let employeeId : String?
	let gender : String?
	let pickupLat : Double?
	let pickupLong : Double?
	let pickupAddress : String?
	let dropLat : Double?
	let dropLong : Double?
	let dropAddress : String?
	let city : String?
	let department : String?
	let entity : String?
	let profilePicture : String?
	let route : Route?
	let routeId : Int?
	let id : Int?
    var directionPolyline: String?
    var isProfileApproved: Bool?
    var serviceType: ServicesType?
	enum CodingKeys: String, CodingKey {

		case userName = "userName"
		case name = "name"
		case surname = "surname"
		case emailAddress = "emailAddress"
		case isActive = "isActive"
		case fullName = "fullName"
		case phoneNumber = "phoneNumber"
		case lastLoginTime = "lastLoginTime"
		case creationTime = "creationTime"
		case roleNames = "roleNames"
		case employeeId = "employeeId"
		case gender = "gender"
		case pickupLat = "pickupLat"
		case pickupLong = "pickupLong"
		case pickupAddress = "pickupAddress"
		case dropLat = "dropLat"
		case dropLong = "dropLong"
		case dropAddress = "dropAddress"
		case city = "city"
		case department = "department"
		case entity = "entity"
		case profilePicture = "profilePicture"
		case route = "route"
		case routeId = "routeId"
		case id = "id"
        case directionPolyline = "directionPolyline"
        case isProfileApproved = "isProfileApproved"
        case serviceType = "serviceType"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		userName = try values.decodeIfPresent(String.self, forKey: .userName)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		surname = try values.decodeIfPresent(String.self, forKey: .surname)
		emailAddress = try values.decodeIfPresent(String.self, forKey: .emailAddress)
		isActive = try values.decodeIfPresent(Bool.self, forKey: .isActive)
		fullName = try values.decodeIfPresent(String.self, forKey: .fullName)
		phoneNumber = try values.decodeIfPresent(String.self, forKey: .phoneNumber)
		lastLoginTime = try values.decodeIfPresent(String.self, forKey: .lastLoginTime)
		creationTime = try values.decodeIfPresent(String.self, forKey: .creationTime)
		roleNames = try values.decodeIfPresent(String.self, forKey: .roleNames)
		employeeId = try values.decodeIfPresent(String.self, forKey: .employeeId)
		gender = try values.decodeIfPresent(String.self, forKey: .gender)
		pickupLat = try values.decodeIfPresent(Double.self, forKey: .pickupLat)
		pickupLong = try values.decodeIfPresent(Double.self, forKey: .pickupLong)
		pickupAddress = try values.decodeIfPresent(String.self, forKey: .pickupAddress)
		dropLat = try values.decodeIfPresent(Double.self, forKey: .dropLat)
		dropLong = try values.decodeIfPresent(Double.self, forKey: .dropLong)
		dropAddress = try values.decodeIfPresent(String.self, forKey: .dropAddress)
		city = try values.decodeIfPresent(String.self, forKey: .city)
		department = try values.decodeIfPresent(String.self, forKey: .department)
		entity = try values.decodeIfPresent(String.self, forKey: .entity)
		profilePicture = try values.decodeIfPresent(String.self, forKey: .profilePicture)
		route = try values.decodeIfPresent(Route.self, forKey: .route)
		routeId = try values.decodeIfPresent(Int.self, forKey: .routeId)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
        directionPolyline = try values.decodeIfPresent(String.self, forKey: .directionPolyline)
        isProfileApproved = try values.decodeIfPresent(Bool.self, forKey: .isProfileApproved)
        serviceType = try values.decodeIfPresent(ServicesType.self, forKey: .serviceType)
	}

}
