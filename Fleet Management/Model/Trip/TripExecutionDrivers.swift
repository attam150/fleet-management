//
//  TripExecutionDrivers.swift
//  Fleet Management
//
//  Created by Atta Khan on 03/05/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import Foundation
struct TripExecutionDrivers : Codable {
	let passenger : Passenger?
	let tripExecutionId : String?
	let passengerId : Int?
	let pickupTime : String?
	let dropTime : String?
	let status : TripExecutionDriverStatus?
	let id : String?

	enum CodingKeys: String, CodingKey {

		case passenger = "passenger"
		case tripExecutionId = "tripExecutionId"
		case passengerId = "passengerId"
		case pickupTime = "pickupTime"
		case dropTime = "dropTime"
		case status = "status"
		case id = "id"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		passenger = try values.decodeIfPresent(Passenger.self, forKey: .passenger)
		tripExecutionId = try values.decodeIfPresent(String.self, forKey: .tripExecutionId)
		passengerId = try values.decodeIfPresent(Int.self, forKey: .passengerId)
		pickupTime = try values.decodeIfPresent(String.self, forKey: .pickupTime)
		dropTime = try values.decodeIfPresent(String.self, forKey: .dropTime)
		status = try values.decodeIfPresent(TripExecutionDriverStatus.self, forKey: .status)
		id = try values.decodeIfPresent(String.self, forKey: .id)
	}

}
