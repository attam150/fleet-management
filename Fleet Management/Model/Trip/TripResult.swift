//
//  TripResult.swift
//  Fleet Management
//
//  Created by Atta Khan on 17/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import Foundation
struct TripResult : Codable {
	let totalCount : Int?
	let items : [Trip]?

	enum CodingKeys: String, CodingKey {

		case totalCount = "totalCount"
		case items = "items"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		totalCount = try values.decodeIfPresent(Int.self, forKey: .totalCount)
		items = try values.decodeIfPresent([Trip].self, forKey: .items)
	}

}
