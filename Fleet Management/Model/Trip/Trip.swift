//
//  Trip.swift
//  Fleet Management
//
//  Created by Atta Khan on 17/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import Foundation
struct Trip : Codable {
	let creationTime : String?
	let name : String?
	let startTime : String?
    let tripStartAddress: String?
    let tripEndAddress: String?
	let route : Route?
	let routeId : Int?
	let passengerCount : Int?
	let driverName : String?
	let driverSurname : String?
	let vehicle : Vehicle?
	let driverId : Int?
	let isActive : Bool?
	let passengers : String?
	let id : Int?
    let tripId : Int?
    var type: TripType = .PICKUP
    
	enum CodingKeys: String, CodingKey {

		case creationTime = "creationTime"
		case name = "name"
        case tripStartAddress = "tripStartAddress"
        case tripEndAddress = "tripEndAddress"
		case startTime = "startTime"
		case route = "route"
		case routeId = "routeId"
		case passengerCount = "passengerCount"
		case driverName = "driverName"
		case driverSurname = "driverSurname"
		case vehicle = "vehicle"
		case driverId = "driverId"
		case isActive = "isActive"
		case passengers = "passengers"
		case id = "id"
        case tripId = "tripId"
        case type = "type"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		creationTime = try values.decodeIfPresent(String.self, forKey: .creationTime)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		startTime = try values.decodeIfPresent(String.self, forKey: .startTime)
        tripStartAddress = try values.decodeIfPresent(String.self, forKey: .tripStartAddress)
        tripEndAddress = try values.decodeIfPresent(String.self, forKey: .tripEndAddress)
		route = try values.decodeIfPresent(Route.self, forKey: .route)
		routeId = try values.decodeIfPresent(Int.self, forKey: .routeId)
		passengerCount = try values.decodeIfPresent(Int.self, forKey: .passengerCount)
		driverName = try values.decodeIfPresent(String.self, forKey: .driverName)
		driverSurname = try values.decodeIfPresent(String.self, forKey: .driverSurname)
		vehicle = try values.decodeIfPresent(Vehicle.self, forKey: .vehicle)
		driverId = try values.decodeIfPresent(Int.self, forKey: .driverId)
		isActive = try values.decodeIfPresent(Bool.self, forKey: .isActive)
		passengers = try values.decodeIfPresent(String.self, forKey: .passengers)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
        tripId = try values.decodeIfPresent(Int.self, forKey: .tripId)
        type = try values.decodeIfPresent(TripType.self, forKey: .type)!
	}

}
