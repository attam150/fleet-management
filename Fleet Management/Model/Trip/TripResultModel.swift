//
//  TripResultModel.swift
//  Fleet Management
//
//  Created by Atta Khan on 17/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import Foundation
struct TripResultModel : Codable {
	let tripExecutionId : String?
	let status : TripExecutionPassengerStatus?
	let passenger : ProfileData?

	enum CodingKeys: String, CodingKey {

		case tripExecutionId = "tripExecutionId"
		case status = "status"
		case passenger = "passenger"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		tripExecutionId = try values.decodeIfPresent(String.self, forKey: .tripExecutionId)
		status = try values.decodeIfPresent(TripExecutionPassengerStatus.self, forKey: .status)
		passenger = try values.decodeIfPresent(ProfileData.self, forKey: .passenger)
	}

}
