//
//  Driver.swift
//  Fleet Management
//
//  Created by Atta Khan on 03/05/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//
import Foundation
struct Driver : Codable {
	let userName : String?
	let name : String?
	let surname : String?
	let emailAddress : String?
	let isActive : Bool?
	let fullName : String?
	let phoneNumber : String?
	let lastLoginTime : String?
	let creationTime : String?
	let roleNames : [String]?
	let gender : String?
	let profilePicture : String?
	let city : String?
	let cnic : String?
	let cnicNumber : String?
	let license : String?
	let licenseNumber : String?
	let vehicleId : Int?
	let vehicle : Vehicle?
	let id : Int?

	enum CodingKeys: String, CodingKey {

		case userName = "userName"
		case name = "name"
		case surname = "surname"
		case emailAddress = "emailAddress"
		case isActive = "isActive"
		case fullName = "fullName"
		case phoneNumber = "phoneNumber"
		case lastLoginTime = "lastLoginTime"
		case creationTime = "creationTime"
		case roleNames = "roleNames"
		case gender = "gender"
		case profilePicture = "profilePicture"
		case city = "city"
		case cnic = "cnic"
		case cnicNumber = "cnicNumber"
		case license = "license"
		case licenseNumber = "licenseNumber"
		case vehicleId = "vehicleId"
		case vehicle = "vehicle"
		case id = "id"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		userName = try values.decodeIfPresent(String.self, forKey: .userName)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		surname = try values.decodeIfPresent(String.self, forKey: .surname)
		emailAddress = try values.decodeIfPresent(String.self, forKey: .emailAddress)
		isActive = try values.decodeIfPresent(Bool.self, forKey: .isActive)
		fullName = try values.decodeIfPresent(String.self, forKey: .fullName)
		phoneNumber = try values.decodeIfPresent(String.self, forKey: .phoneNumber)
		lastLoginTime = try values.decodeIfPresent(String.self, forKey: .lastLoginTime)
		creationTime = try values.decodeIfPresent(String.self, forKey: .creationTime)
		roleNames = try values.decodeIfPresent([String].self, forKey: .roleNames)
		gender = try values.decodeIfPresent(String.self, forKey: .gender)
		profilePicture = try values.decodeIfPresent(String.self, forKey: .profilePicture)
		city = try values.decodeIfPresent(String.self, forKey: .city)
		cnic = try values.decodeIfPresent(String.self, forKey: .cnic)
		cnicNumber = try values.decodeIfPresent(String.self, forKey: .cnicNumber)
		license = try values.decodeIfPresent(String.self, forKey: .license)
		licenseNumber = try values.decodeIfPresent(String.self, forKey: .licenseNumber)
		vehicleId = try values.decodeIfPresent(Int.self, forKey: .vehicleId)
		vehicle = try values.decodeIfPresent(Vehicle.self, forKey: .vehicle)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
	}

}
