//
//  Vehicle.swift
//  Fleet Management
//
//  Created by Atta Khan on 17/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import Foundation
struct Vehicle : Codable {
	let make : String?
	let model : String?
	let registrationNumber : String?
	let capacity : Int?
	let type : String?
	let color : String?
	let tenantId : Int?
	let vehicleImage : String?
	let id : Int?

	enum CodingKeys: String, CodingKey {

		case make = "make"
		case model = "model"
		case registrationNumber = "registrationNumber"
		case capacity = "capacity"
		case type = "type"
		case color = "color"
		case tenantId = "tenantId"
		case vehicleImage = "vehicleImage"
		case id = "id"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		make = try values.decodeIfPresent(String.self, forKey: .make)
		model = try values.decodeIfPresent(String.self, forKey: .model)
		registrationNumber = try values.decodeIfPresent(String.self, forKey: .registrationNumber)
		capacity = try values.decodeIfPresent(Int.self, forKey: .capacity)
		type = try values.decodeIfPresent(String.self, forKey: .type)
		color = try values.decodeIfPresent(String.self, forKey: .color)
		tenantId = try values.decodeIfPresent(Int.self, forKey: .tenantId)
		vehicleImage = try values.decodeIfPresent(String.self, forKey: .vehicleImage)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
	}

}
