//
//  ResultModel.swift
//  Fleet Management
//
//  Created by Atta Khan on 15/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import Foundation
struct Items : Codable {
	let name : String?
	let city : String?
	let startingPointLat : Double?
	let startingPointLong : Double?
	let endingPointLat : Double?
	let endingPointLong : Double?
    let lat : Double?
    let long : Double?
	let isActive : Bool?
	let startingAddress : String?
	let endingAddress : String?
	let id : Int?
    let tripsCount: Int?
    

	enum CodingKeys: String, CodingKey {

		case name = "name"
		case city = "city"
		case startingPointLat = "startingPointLat"
		case startingPointLong = "startingPointLong"
		case endingPointLat = "endingPointLat"
		case endingPointLong = "endingPointLong"
		case isActive = "isActive"
		case startingAddress = "startingAddress"
		case endingAddress = "endingAddress"
		case id = "id"
        case long = "long"
        case lat = "lat"
        case tripsCount = "tripsCount"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		city = try values.decodeIfPresent(String.self, forKey: .city)
		startingPointLat = try values.decodeIfPresent(Double.self, forKey: .startingPointLat)
		startingPointLong = try values.decodeIfPresent(Double.self, forKey: .startingPointLong)
		endingPointLat = try values.decodeIfPresent(Double.self, forKey: .endingPointLat)
		endingPointLong = try values.decodeIfPresent(Double.self, forKey: .endingPointLong)
		isActive = try values.decodeIfPresent(Bool.self, forKey: .isActive)
		startingAddress = try values.decodeIfPresent(String.self, forKey: .startingAddress)
		endingAddress = try values.decodeIfPresent(String.self, forKey: .endingAddress)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
        long = try values.decodeIfPresent(Double.self, forKey: .long)
        lat = try values.decodeIfPresent(Double.self, forKey: .lat)
        tripsCount = try values.decodeIfPresent(Int.self, forKey: .tripsCount)
	}

}
