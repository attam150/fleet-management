
//
//  UserProfileModel.swift
//  Fleet Management
//
//  Created by Atta Khan on 17/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import Foundation
struct UserProfileModel : Codable {
	let result : ProfileData?
	let targetUrl : String?
	let success : Bool?
	let error : ErrorModel?
	let unAuthorizedRequest : Bool?
	let __abp : Bool?

	enum CodingKeys: String, CodingKey {

		case result = "result"
		case targetUrl = "targetUrl"
		case success = "success"
		case error = "error"
		case unAuthorizedRequest = "unAuthorizedRequest"
		case __abp = "__abp"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		result = try values.decodeIfPresent(ProfileData.self, forKey: .result)
		targetUrl = try values.decodeIfPresent(String.self, forKey: .targetUrl)
		success = try values.decodeIfPresent(Bool.self, forKey: .success)
		error = try values.decodeIfPresent(ErrorModel.self, forKey: .error)
		unAuthorizedRequest = try values.decodeIfPresent(Bool.self, forKey: .unAuthorizedRequest)
		__abp = try values.decodeIfPresent(Bool.self, forKey: .__abp)
	}

}
struct ProfileData : Codable {
    var userName : String?
    var name : String?
    var surname : String?
    var emailAddress : String?
    var isActive : Bool?
    var fullName : String?
    var phoneNumber : String?
    var lastLoginTime : String?
    var creationTime : String?
    var roleNames : [String]?
    var employeeId : String?
    var gender : String?
    var city : String?
    var pickupLat : Double?
    var pickupLong : Double?
    var pickupAddress : String?
    var dropLat : Double?
    var dropLong : Double?
    var dropAddress : String?
    var department : String?
    var entity : String?
    var profilePicture : String?
    var cnic : String?
    var cnicNumber : String?
    var license : String?
    var licenseNumber : String?
    var route : Route?
    var routeId : Int?
    var id : Int?
    var directionPolyline: String?
    var isProfileApproved: Bool?
    var serviceType: ServicesType?

    enum CodingKeys: String, CodingKey {

        case userName = "userName"
        case name = "name"
        case surname = "surname"
        case emailAddress = "emailAddress"
        case isActive = "isActive"
        case fullName = "fullName"
        case phoneNumber = "phoneNumber"
        case lastLoginTime = "lastLoginTime"
        case creationTime = "creationTime"
        case roleNames = "roleNames"
        case employeeId = "employeeId"
        case gender = "gender"
        case city = "city"
        case pickupLat = "pickupLat"
        case pickupLong = "pickupLong"
        case pickupAddress = "pickupAddress"
        case dropLat = "dropLat"
        case dropLong = "dropLong"
        case dropAddress = "dropAddress"
        case department = "department"
        case entity = "entity"
        case profilePicture = "profilePicture"
        case cnic = "cnic"
        case cnicNumber = "cnicNumber"
        case license = "license"
        case licenseNumber = "licenseNumber"
        case route = "route"
        case routeId = "routeId"
        case id = "id"
        case directionPolyline = "directionPolyline"
        case isProfileApproved = "isProfileApproved"
        case serviceType = "serviceType"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        userName = try values.decodeIfPresent(String.self, forKey: .userName)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        surname = try values.decodeIfPresent(String.self, forKey: .surname)
        emailAddress = try values.decodeIfPresent(String.self, forKey: .emailAddress)
        isActive = try values.decodeIfPresent(Bool.self, forKey: .isActive)
        fullName = try values.decodeIfPresent(String.self, forKey: .fullName)
        phoneNumber = try values.decodeIfPresent(String.self, forKey: .phoneNumber)
        lastLoginTime = try values.decodeIfPresent(String.self, forKey: .lastLoginTime)
        creationTime = try values.decodeIfPresent(String.self, forKey: .creationTime)
        roleNames = try values.decodeIfPresent([String].self, forKey: .roleNames)
        employeeId = try values.decodeIfPresent(String.self, forKey: .employeeId)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        city = try values.decodeIfPresent(String.self, forKey: .city)
        pickupLat = try values.decodeIfPresent(Double.self, forKey: .pickupLat)
        pickupLong = try values.decodeIfPresent(Double.self, forKey: .pickupLong)
        pickupAddress = try values.decodeIfPresent(String.self, forKey: .pickupAddress)
        dropLat = try values.decodeIfPresent(Double.self, forKey: .dropLat)
        dropLong = try values.decodeIfPresent(Double.self, forKey: .dropLong)
        dropAddress = try values.decodeIfPresent(String.self, forKey: .dropAddress)
        department = try values.decodeIfPresent(String.self, forKey: .department)
        entity = try values.decodeIfPresent(String.self, forKey: .entity)
        profilePicture = try values.decodeIfPresent(String.self, forKey: .profilePicture)
        cnic = try values.decodeIfPresent(String.self, forKey: .cnic)
        cnicNumber = try values.decodeIfPresent(String.self, forKey: .cnicNumber)
        license = try values.decodeIfPresent(String.self, forKey: .license)
        licenseNumber = try values.decodeIfPresent(String.self, forKey: .licenseNumber)
        route = try values.decodeIfPresent(Route.self, forKey: .route)
        routeId = try values.decodeIfPresent(Int.self, forKey: .routeId)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        directionPolyline = try values.decodeIfPresent(String.self, forKey: .directionPolyline)
        isProfileApproved = try values.decodeIfPresent(Bool.self, forKey: .isProfileApproved)
        serviceType = try values.decodeIfPresent(ServicesType.self, forKey: .serviceType)
    }

}
