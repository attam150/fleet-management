//
//  DriverResult.swift
//  Fleet Management
//
//  Created by Atta Khan on 17/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//


import Foundation
struct DriverResult : Codable {
	let lat : Double?
	let long : Double?

	enum CodingKeys: String, CodingKey {

		case lat = "lat"
		case long = "long"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		lat = try values.decodeIfPresent(Double.self, forKey: .lat)
		long = try values.decodeIfPresent(Double.self, forKey: .long)
	}

}
