//
//  UserModel.swift
//  Fleet Management
//
//  Created by Atta Khan on 17/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//


import Foundation
struct UserModel : Codable {
	let result : UserData?
	let targetUrl : String?
	let success : Bool?
	let error : ErrorModel?
	let unAuthorizedRequest : Bool?
	let __abp : Bool?

	enum CodingKeys: String, CodingKey {

		case result = "result"
		case targetUrl = "targetUrl"
		case success = "success"
		case error = "error"
		case unAuthorizedRequest = "unAuthorizedRequest"
		case __abp = "__abp"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		result = try values.decodeIfPresent(UserData.self, forKey: .result)
		targetUrl = try values.decodeIfPresent(String.self, forKey: .targetUrl)
		success = try values.decodeIfPresent(Bool.self, forKey: .success)
		error = try values.decodeIfPresent(ErrorModel.self, forKey: .error)
		unAuthorizedRequest = try values.decodeIfPresent(Bool.self, forKey: .unAuthorizedRequest)
		__abp = try values.decodeIfPresent(Bool.self, forKey: .__abp)
	}

}

struct UserData : Codable {
    let accessToken : String?
    let encryptedAccessToken : String?
    let expireInSeconds : Int?
    let userId : Int?
    let userName : String?
    let fullName : String?
    let profilePicture : String?
    let employeeId : String?
    let role: UserRole?
    let message : String?
    let canLogin : Bool?
    let isActive : Bool?
    let sendStatus : Bool?
    let success : Bool?
    enum CodingKeys: String, CodingKey {

       case accessToken = "accessToken"
        case encryptedAccessToken = "encryptedAccessToken"
        case expireInSeconds = "expireInSeconds"
        case userId = "userId"
        case userName = "userName"
        case fullName = "fullName"
        case profilePicture = "profilePicture"
        case employeeId = "employeeId"
        case role   =   "role"
        case message = "message"
        case canLogin = "canLogin"
        case isActive = "isActive"
        case sendStatus = "sendStatus"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        accessToken = try values.decodeIfPresent(String.self, forKey: .accessToken)
        encryptedAccessToken = try values.decodeIfPresent(String.self, forKey: .encryptedAccessToken)
        expireInSeconds = try values.decodeIfPresent(Int.self, forKey: .expireInSeconds)
        userId = try values.decodeIfPresent(Int.self, forKey: .userId)
        userName = try values.decodeIfPresent(String.self, forKey: .userName)
        fullName = try values.decodeIfPresent(String.self, forKey: .fullName)
        profilePicture = try values.decodeIfPresent(String.self, forKey: .profilePicture)
        employeeId = try values.decodeIfPresent(String.self, forKey: .employeeId)
        role = try values.decodeIfPresent(UserRole.self, forKey: .role)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        canLogin = try values.decodeIfPresent(Bool.self, forKey: .canLogin)
        isActive = try values.decodeIfPresent(Bool.self, forKey: .isActive)
        sendStatus = try values.decodeIfPresent(Bool.self, forKey: .sendStatus)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }
}

struct User: Codable {
    var userNameOrEmailAddress: String?
    var password: String?
    var rememberClient: Bool?
}

struct RegisterUser: Codable {
    var name: String?
    var surname: String?
    var userName: String?
    var emailAddress: String?
    var password: String?
    var employeeId: String?
    var phoneNumber: String?
    var gender: String?
    var pickupLat: Double?
    var pickupLong: Double?
    var pickupAddress: String?
    var dropLat: Double?
    var dropLong: Double?
    var dropAddress: String?
    var city: String?
    var department: String?
    var entity: String?
    var routeId: Int?
    var role : Int?
    var directionPolyline: String?
}
