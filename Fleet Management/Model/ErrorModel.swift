//
//  ErrorModel.swift
//  Fleet Management
//
//  Created by Atta Khan on 17/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import Foundation
struct ErrorModel : Codable {
    let code : Int?
    let message : String?
    let details : String?

    enum CodingKeys: String, CodingKey {

        case code = "code"
        case message = "message"
        case details = "details"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        details = try values.decodeIfPresent(String.self, forKey: .details)
    }

}
