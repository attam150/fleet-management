/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Route : Codable {
    let name : String?
    let city : String?
    let startingPointLat : Double?
    let startingPointLong : Double?
    let endingPointLat : Double?
    let endingPointLong : Double?
    let lat : Double?
    let long : Double?
    let isActive : Bool?
    let startingAddress : String?
    let endingAddress : String?
    let id : Int?
    let tripsCount: Int?
    let usersAssociated : Int?
    let passengersCount : Int?
    let driversCount : Int?

    enum CodingKeys: String, CodingKey {

        case name = "name"
        case city = "city"
        case startingPointLat = "startingPointLat"
        case startingPointLong = "startingPointLong"
        case endingPointLat = "endingPointLat"
        case endingPointLong = "endingPointLong"
        case isActive = "isActive"
        case startingAddress = "startingAddress"
        case endingAddress = "endingAddress"
        case tripsCount = "tripsCount"
        case usersAssociated = "usersAssociated"
        case passengersCount = "passengersCount"
        case driversCount = "driversCount"
        case id = "id"
        case long = "long"
        case lat = "lat"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        city = try values.decodeIfPresent(String.self, forKey: .city)
        startingPointLat = try values.decodeIfPresent(Double.self, forKey: .startingPointLat)
        startingPointLong = try values.decodeIfPresent(Double.self, forKey: .startingPointLong)
        endingPointLat = try values.decodeIfPresent(Double.self, forKey: .endingPointLat)
        endingPointLong = try values.decodeIfPresent(Double.self, forKey: .endingPointLong)
        isActive = try values.decodeIfPresent(Bool.self, forKey: .isActive)
        startingAddress = try values.decodeIfPresent(String.self, forKey: .startingAddress)
        endingAddress = try values.decodeIfPresent(String.self, forKey: .endingAddress)
        tripsCount = try values.decodeIfPresent(Int.self, forKey: .tripsCount)
        usersAssociated = try values.decodeIfPresent(Int.self, forKey: .usersAssociated)
        passengersCount = try values.decodeIfPresent(Int.self, forKey: .passengersCount)
        driversCount = try values.decodeIfPresent(Int.self, forKey: .driversCount)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        long = try values.decodeIfPresent(Double.self, forKey: .long)
        lat = try values.decodeIfPresent(Double.self, forKey: .lat)
    }

}

