//
//  RequestDetailsModel.swift
//  Fleet Management
//
//  Created by Atta Khan on 11/06/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import Foundation
struct RequestDetailsModel : Codable {
	let result : RequestDetailResult?
	let targetUrl : String?
	let success : Bool?
	let error : String?
	let unAuthorizedRequest : Bool?
	let __abp : Bool?

	enum CodingKeys: String, CodingKey {

		case result = "result"
		case targetUrl = "targetUrl"
		case success = "success"
		case error = "error"
		case unAuthorizedRequest = "unAuthorizedRequest"
		case __abp = "__abp"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		result = try values.decodeIfPresent(RequestDetailResult.self, forKey: .result)
		targetUrl = try values.decodeIfPresent(String.self, forKey: .targetUrl)
		success = try values.decodeIfPresent(Bool.self, forKey: .success)
		error = try values.decodeIfPresent(String.self, forKey: .error)
		unAuthorizedRequest = try values.decodeIfPresent(Bool.self, forKey: .unAuthorizedRequest)
		__abp = try values.decodeIfPresent(Bool.self, forKey: .__abp)
	}

}
struct RequestDetailResult : Codable {
    let category : RequestCategory?
    let subCategory : RequestSubCategory?
    let status : RequestStatus?
    let requestNumber : Int?
    let creationTime : String?
    let messages : [Messages]?

    enum CodingKeys: String, CodingKey {

        case category = "category"
        case subCategory = "subCategory"
        case status = "status"
        case requestNumber = "requestNumber"
        case creationTime = "creationTime"
       case messages = "messages"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        category = try values.decodeIfPresent(RequestCategory.self, forKey: .category)
        subCategory = try values.decodeIfPresent(RequestSubCategory.self, forKey: .subCategory)
        status = try values.decodeIfPresent(RequestStatus.self, forKey: .status)
        requestNumber = try values.decodeIfPresent(Int.self, forKey: .requestNumber)
        creationTime = try values.decodeIfPresent(String.self, forKey: .creationTime)
        messages = try values.decodeIfPresent([Messages].self, forKey: .messages)
    }

}
struct Messages : Codable {
    let message : String?
    let userType : String?
    let isDeleted : Bool?
    let deleterUserId : String?
    let deletionTime : String?
    let lastModificationTime : String?
    let lastModifierUserId : String?
    let creationTime : String?
    let creatorUserId : String?
    let id : String?

    enum CodingKeys: String, CodingKey {

        case message = "message"
        case userType = "userType"
        case isDeleted = "isDeleted"
        case deleterUserId = "deleterUserId"
        case deletionTime = "deletionTime"
        case lastModificationTime = "lastModificationTime"
        case lastModifierUserId = "lastModifierUserId"
        case creationTime = "creationTime"
        case creatorUserId = "creatorUserId"
        case id = "id"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        userType = try values.decodeIfPresent(String.self, forKey: .userType)
        isDeleted = try values.decodeIfPresent(Bool.self, forKey: .isDeleted)
        deleterUserId = try values.decodeIfPresent(String.self, forKey: .deleterUserId)
        deletionTime = try values.decodeIfPresent(String.self, forKey: .deletionTime)
        lastModificationTime = try values.decodeIfPresent(String.self, forKey: .lastModificationTime)
        lastModifierUserId = try values.decodeIfPresent(String.self, forKey: .lastModifierUserId)
        creationTime = try values.decodeIfPresent(String.self, forKey: .creationTime)
        creatorUserId = try values.decodeIfPresent(String.self, forKey: .creatorUserId)
        id = try values.decodeIfPresent(String.self, forKey: .id)
    }

}
