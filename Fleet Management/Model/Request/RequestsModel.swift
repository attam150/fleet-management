//
//  RequestsModel.swift
//  Fleet Management
//
//  Created by Atta Khan on 11/06/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//
import Foundation
struct RequestsModel : Codable {
	let result : RequestResult?
	let targetUrl : String?
	let success : Bool?
	let error : String?
	let unAuthorizedRequest : Bool?
	let __abp : Bool?

	enum CodingKeys: String, CodingKey {

		case result = "result"
		case targetUrl = "targetUrl"
		case success = "success"
		case error = "error"
		case unAuthorizedRequest = "unAuthorizedRequest"
		case __abp = "__abp"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		result = try values.decodeIfPresent(RequestResult.self, forKey: .result)
		targetUrl = try values.decodeIfPresent(String.self, forKey: .targetUrl)
		success = try values.decodeIfPresent(Bool.self, forKey: .success)
		error = try values.decodeIfPresent(String.self, forKey: .error)
		unAuthorizedRequest = try values.decodeIfPresent(Bool.self, forKey: .unAuthorizedRequest)
		__abp = try values.decodeIfPresent(Bool.self, forKey: .__abp)
	}

}
struct RequestResult : Codable {
    let totalCount : Int?
    let items : [RequestItems]?

    enum CodingKeys: String, CodingKey {

        case totalCount = "totalCount"
        case items = "items"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        totalCount = try values.decodeIfPresent(Int.self, forKey: .totalCount)
        items = try values.decodeIfPresent([RequestItems].self, forKey: .items)
    }

}
struct RequestItems : Codable {
    let category : Int?
    let subCategory : Int?
    let status : RequestStatus?
    let requestNumber : Int?
    let creationTime : String?
    let id : String?

    enum CodingKeys: String, CodingKey {

        case category = "category"
        case subCategory = "subCategory"
        case status = "status"
        case requestNumber = "requestNumber"
        case creationTime = "creationTime"
        case id = "id"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        category = try values.decodeIfPresent(Int.self, forKey: .category)
        subCategory = try values.decodeIfPresent(Int.self, forKey: .subCategory)
        status = try values.decodeIfPresent(RequestStatus.self, forKey: .status)
        requestNumber = try values.decodeIfPresent(Int.self, forKey: .requestNumber)
        creationTime = try values.decodeIfPresent(String.self, forKey: .creationTime)
        id = try values.decodeIfPresent(String.self, forKey: .id)
    }

}
