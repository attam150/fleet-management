//
//  NotificationModel.swift
//  Fleet Management
//
//  Created by Atta khan on 29/05/2020.
//  Copyright © 2020 Atta khan. All rights reserved.
//

import Foundation
struct NotificationModel : Codable {
	let result : NotificationResult?
	let targetUrl : String?
	let success : Bool?
	let error : ErrorModel?
	let unAuthorizedRequest : Bool?
	let __abp : Bool?

	enum CodingKeys: String, CodingKey {

		case result = "result"
		case targetUrl = "targetUrl"
		case success = "success"
		case error = "error"
		case unAuthorizedRequest = "unAuthorizedRequest"
		case __abp = "__abp"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		result = try values.decodeIfPresent(NotificationResult.self, forKey: .result)
		targetUrl = try values.decodeIfPresent(String.self, forKey: .targetUrl)
		success = try values.decodeIfPresent(Bool.self, forKey: .success)
		error = try values.decodeIfPresent(ErrorModel.self, forKey: .error)
		unAuthorizedRequest = try values.decodeIfPresent(Bool.self, forKey: .unAuthorizedRequest)
		__abp = try values.decodeIfPresent(Bool.self, forKey: .__abp)
	}

}
struct NotificationResult : Codable {
    let totalCount : Int?
    let items : [NotificationItems]?

    enum CodingKeys: String, CodingKey {

        case totalCount = "totalCount"
        case items = "items"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        totalCount = try values.decodeIfPresent(Int.self, forKey: .totalCount)
        items = try values.decodeIfPresent([NotificationItems].self, forKey: .items)
    }

}
struct NotificationItems : Codable {
    let userId : Int?
    let message : String?
    let username : String?
    let userType : Int?
    let tripId: Int?
    let creationTime : String?
    let creatorUserId : Int?
    let id : String?
    let type: Int?

    enum CodingKeys: String, CodingKey {

        case userId = "userId"
        case message = "message"
        case username = "username"
        case userType = "userType"
        case tripId = "tripId"
        case creationTime = "creationTime"
        case creatorUserId = "creatorUserId"
        case id = "id"
        case type = "type"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        userId = try values.decodeIfPresent(Int.self, forKey: .userId)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        username = try values.decodeIfPresent(String.self, forKey: .username)
        userType = try values.decodeIfPresent(Int.self, forKey: .userType)
        tripId = try values.decodeIfPresent(Int.self, forKey: .tripId)
        creationTime = try values.decodeIfPresent(String.self, forKey: .creationTime)
        creatorUserId = try values.decodeIfPresent(Int.self, forKey: .creatorUserId)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        type = try values.decodeIfPresent(Int.self, forKey: .type)
    }

}
