//
//  ViewControllerExample.swift
//  Fleet Management
//
//  Created by Atta khan on 17/06/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit

class ViewControllerExample: UIViewController {

    @IBOutlet weak var mainStackView: UIStackView!
    
    @IBOutlet weak var dropOffView1: UIView!
    @IBOutlet weak var pickupView1: UIView!
    
    @IBOutlet weak var dropOffView2: UIView!
    @IBOutlet weak var pickupView2: UIView!
    
    @IBOutlet weak var pickupView3: UIView!
    @IBOutlet weak var dropOffView3: UIView!
    
    @IBOutlet weak var pickupView4: UIView!
    @IBOutlet weak var dropOffView4: UIView!
    
    @IBOutlet weak var heightConstriant: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        let backgroundView = UIView()
//        backgroundView.backgroundColor = UIColor.rgb(red: 241, green: 241, blue: 241, alpha: 1)
//
//        backgroundView.translatesAutoresizingMaskIntoConstraints = false
//
//        // put background view as the most background subviews of stack view
//        mainStackView.insertSubview(backgroundView, at: 0)
//
//        // pin the background view edge to the stack view edge
//        NSLayoutConstraint.activate([
//            backgroundView.leadingAnchor.constraint(equalTo: mainStackView.leadingAnchor),
//            backgroundView.trailingAnchor.constraint(equalTo: mainStackView.trailingAnchor),
//            backgroundView.topAnchor.constraint(equalTo: mainStackView.topAnchor),
//            backgroundView.bottomAnchor.constraint(equalTo: mainStackView.bottomAnchor)
//        ])
//        pickupView1.isHidden = true
//        pickupView2.isHidden = true
//        pickupView3.isHidden = true
//        pickupView4.isHidden = true
//        heightConstriant.constant = 65.0
        
    }
    

    @IBAction func navigateBackBtn(_ sender: Any) {
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
