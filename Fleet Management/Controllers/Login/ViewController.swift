//
//  ViewController.swift
//  Fleet Management
//
//  Created by Atta Khan on 26/03/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit
import PKRevealController

class ViewController: UIViewController {

    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var usernameTF: UITextField!
    
    @IBOutlet weak var passBottomLine: UIView!
    @IBOutlet weak var usernameBottomLine: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.shared.setStatusBarBackgroundColor(view: view, color: UIColor.themeColor)
        
        let isLogin = UserDefaults.standard.bool(forKey: "isLoginIn")
        if isLogin {
            self.initiateContorller()
        }
        usernameTF.text = "demouser@gmail.com"
        //"attam150@gmail.com"
        //"123456"
        passwordTF.text = "123456789"
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func generateToken() {
        let isTokenExist = UserDefaults.standard.bool(forKey: "isTokenExist")
        if isTokenExist {
            print("Token already exists.")
        } else {
            createToekn()
        }
    }
    
    private func initiateContorller() {
        let frontViewController: UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeController")
        let leftViewController: UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LeftMenuViewController")
        let revealController: PKRevealController = PKRevealController(frontViewController: frontViewController, leftViewController: leftViewController)
        self.navigationController?.pushViewController(revealController, animated: true)
    }

    @IBAction func tapOnLoginBtn(_ sender: Any) {
        guard let userNameTxt  = usernameTF.text, !userNameTxt.isEmpty else {
            self.showAlert(title: "Alert", message: "Please enter your first name.", controller: self) {
               self.usernameTF.becomeFirstResponder()
            }
            return
        }
      
        guard let passwordTxt  = passwordTF.text, !passwordTxt.isEmpty else {
            self.showAlert(title: "Alert", message: "Please enter your last name.", controller: self) {
                self.passwordTF.becomeFirstResponder()
            }
            return
        }
        
        var user =  User()
        user.userNameOrEmailAddress = userNameTxt
        user.password = passwordTxt
        user.rememberClient = true
        
        let encode = JSONEncoder()
        let jsonData = try! encode.encode(user)
        print(jsonData)
        
        
        let url = URL(string: USER_LOGIN)!
        showSpinner(onView: self.view)
        WebServiceManager.sharedInstance.postRequest(params: jsonData , url: url, serviceType: "LOGIN", modelType: UserModel.self, success: { (response) in
            let responseObj = response as! UserModel
            self.removeSpinner()
            
                if responseObj.success == false {
                    // print error msg
                     DispatchQueue.main.async {
                        self.showAlert(title: "Alert", message: (responseObj.error?.details)!, controller: self) {
                           
                        }
                    }
                }
                else {
                    if let userRole = responseObj.result?.role, userRole == .PASSENGER {
                        if let user_id = responseObj.result?.userId {
                            UserDefaults.standard.set(user_id, forKey: "userId")
                        }
                        if let employee_id = responseObj.result?.employeeId {
                            UserDefaults.standard.set(employee_id, forKey: "employeeId")
                        }
                        if let user_name = responseObj.result?.userName {
                            UserDefaults.standard.set(user_name, forKey: "username")
                        }
                        if let user_name = responseObj.result?.fullName {
                            UserDefaults.standard.set(user_name, forKey: "fullName")
                        }
                        
                        if let profilePicture = responseObj.result?.profilePicture {
                            let userImg = BASE_URL_MEDIA + profilePicture
                            UserDefaults.standard.set(userImg, forKey: "profilePic")
                        }
                        if let accessToken = responseObj.result?.accessToken {
                           UserDefaults.standard.set(accessToken, forKey: "token")
                        }
                        UserDefaults.standard.set(true, forKey: "isLoginIn")
                        DispatchQueue.main.async { () -> Void in
                            self.generateToken()
                            self.initiateContorller()
                        }
                    }  else {
                        DispatchQueue.main.async { () -> Void in
                            self.showAlert(title: "Alert", message: "Please try again with correct credentials.!", controller: self) {
                            }
                        }
                    }
                    
                    
                }

        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
    }
    //2349036010481
    
    @IBAction func tapOnSignupBtn(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "signup1") as! Signup1ViewController
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func tapOnForgotPassBtn(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "FogotPassViewController") as! FogotPassViewController
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func createToekn() {
        
        print("I am here....")
        let device_id = UIDevice.current.identifierForVendor?.uuidString ?? ""
        let user_id = UserDefaults.standard.integer(forKey: "userId")
        let fcmToken = UserDefaults.standard.string(forKey: "device_id") ?? ""
        let url = URL(string: CREATE_TOKEN + "?userId=\(user_id)&token=\(fcmToken)&deviceId=\(device_id)")!
        //showSpinner(onView: self.view)
        WebServiceManager.sharedInstance.postRequest(params: nil, url: url, serviceType: "Create Token", modelType: UserModel.self, success: { (response) in
            self.removeSpinner()
            let responseObj = response as! UserModel
            if responseObj.result?.success == true {
                UserDefaults.standard.set(true, forKey: "isTokenExist")
            } else {
               UserDefaults.standard.set(false, forKey: "isTokenExist")
            }
        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
    }
    
}
struct Token {
    var user_id: Int
    var device_id: String
    var fcmToken: String
}
