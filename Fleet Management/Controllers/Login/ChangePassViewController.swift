//
//  ChangePassViewController.swift
//  Fleet Management
//
//  Created by Atta Khan on 31/03/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit

class ChangePassViewController: UIViewController {

    
    @IBOutlet weak var currentPasswordTF: UITextField!
    @IBOutlet weak var confirmPasswrodTF: UITextField!
    @IBOutlet weak var newPasswordTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.shared.setStatusBarBackgroundColor(view: view, color: UIColor.themeColor)
    }
    
    @IBAction func navigateBackScreen(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func tapOnChangePasswordBtn(_ sender: Any) {
        guard let currentPasswordTxt  = currentPasswordTF.text, !currentPasswordTxt.isEmpty else {
             self.showAlert(title: "Alert", message: "Please enter your current password.", controller: self) {
                 self.currentPasswordTF.becomeFirstResponder()
             }
             return
         }
        
         guard let newPasswordTxt  = newPasswordTF.text, !newPasswordTxt.isEmpty else {
             self.showAlert(title: "Alert", message: "Please enter your new password.", controller: self) {
                 self.newPasswordTF.becomeFirstResponder()
             }
             return
         }
        if newPasswordTxt.count < 6 {
            self.showAlert(title: "Alert", message: "Please enter password atleast 6 characters.", controller: self) {
                self.newPasswordTF.becomeFirstResponder()
            }
            return
        }
         
         guard let confirmPasswrodTxt  = confirmPasswrodTF.text, !confirmPasswrodTxt.isEmpty else {
             self.showAlert(title: "Alert", message: "Please enter your confirm password.", controller: self) {
                 self.confirmPasswrodTF.becomeFirstResponder()
             }
             return
         }
        
        if confirmPasswrodTxt != newPasswordTxt {
            self.showAlert(title: "Alert", message: "Confirm password mismatch to new password.", controller: self) {
                self.confirmPasswrodTF.becomeFirstResponder()
            }
            return
        }
        
        var pass = ChangePass()
        pass.currentPassword = currentPasswordTxt
        pass.newPassword = confirmPasswrodTxt
        
        let encode = JSONEncoder()
        let jsonData = try! encode.encode(pass)
        
        let url = URL(string: CHANGE_PASSWORD)!
        showSpinner(onView: self.view)
        WebServiceManager.sharedInstance.postRequest(params: jsonData , url: url, serviceType: "CHANGE PASSWORD", modelType: UserModel.self, success: { (response) in
            let responseObj = response as! UserModel
            self.removeSpinner()
            if let message  = responseObj.result?.message {
                DispatchQueue.main.async { () -> Void in
                    self.showAlert(title: "Alert", message: message, controller: self) {
                    }
                }
            }
            
        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
        
        
    }
}

struct ChangePass: Codable {
    var currentPassword: String?
    var newPassword: String?
}
