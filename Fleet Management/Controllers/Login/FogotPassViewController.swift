//
//  FogotPassViewController.swift
//  Fleet Management
//
//  Created by Atta Khan on 31/03/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit

struct UserEmail: Codable {
    var emailAddress: String?
}

class FogotPassViewController: UIViewController {

    @IBOutlet weak var emailTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.shared.setStatusBarBackgroundColor(view: view, color: UIColor.themeColor)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func tapOnSendBtn(_ sender: Any) {
        guard let emailTxt  = emailTF.text, !emailTxt.isEmpty else {
            self.showAlert(title: "Alert", message: "Please enter your email.", controller: self) {
                self.emailTF.becomeFirstResponder()
            }
            return
        }
        if !emailTxt.isValidEmail {
           self.showAlert(title: "Alert", message: "Please enter valid email.", controller: self) {
                self.emailTF.becomeFirstResponder()
            }
            return
        }
        
        let url = URL(string: FORGOT_PASSWORD + "?emailAddress=\(emailTxt)")!
        showSpinner(onView: self.view)
        WebServiceManager.sharedInstance.postRequest(params: nil, url: url, serviceType: "Forgot Password", modelType: UserModel.self, success: { (response) in
            self.removeSpinner()
            let responseObj = response as! UserModel
            if responseObj.result?.sendStatus == true {
                DispatchQueue.main.async { () -> Void in
                    self.showAlert(title: "Alert", message: "Code has been sent to your email.", controller: self) {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VerifyOTPVC") as! VerifyOTPVC
                        vc.verifyEmailAddress = emailTxt
                        self.navigationController?.pushViewController(vc, animated: true)
                        }
                }
            } else {
                DispatchQueue.main.async { () -> Void in
                   self.showAlert(title: "Alert", message: (responseObj.error?.message)!, controller: self) {
                    }
               }
            }
        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
        
        
        
        
        
        
        
//        let vc    = storyboard?.instantiateViewController(withIdentifier: "MailSentVC") as! MailSentVC
//        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func navigateBackBtn(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
  

}
