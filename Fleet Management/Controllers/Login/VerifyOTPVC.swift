//
//  VerifyOTPVC.swift
//  Fleet Management
//
//  Created by Atta Khan on 02/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit

class VerifyOTPVC: UIViewController {

    @IBOutlet weak var txtField1: UITextField!
    @IBOutlet weak var txtField2: UITextField!
    @IBOutlet weak var txtField3: UITextField!
    @IBOutlet weak var txtField4: UITextField!
    @IBOutlet weak var txtField5: UITextField!
    @IBOutlet weak var txtField7: UITextField!
    
    @IBOutlet weak var newPasswordTF: UITextField!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    var verifyEmailAddress = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.shared.setStatusBarBackgroundColor(view: view, color: UIColor.themeColor)
        txtField1.delegate = self
        txtField2.delegate = self
        txtField3.delegate = self
        txtField4.delegate = self
        txtField5.delegate = self
        txtField7.delegate = self
        
        
        txtField1.keyboardType = .asciiCapableNumberPad
        txtField2.keyboardType = .asciiCapableNumberPad
        txtField3.keyboardType = .asciiCapableNumberPad
        txtField4.keyboardType = .asciiCapableNumberPad
        txtField5.keyboardType = .asciiCapableNumberPad
        txtField7.keyboardType = .asciiCapableNumberPad
    }
    
    @IBAction func resetPasswordBtn(_ sender: Any) {
        guard let txtFieldTxt1  = txtField1.text, !txtFieldTxt1.isEmpty else {
            self.showAlert(title: "Alert", message: "Please enter Code.", controller: self) {
                self.txtField1.becomeFirstResponder()
            }
            return
        }
        guard let txtFieldTxt2  = txtField2.text, !txtFieldTxt2.isEmpty else {
            self.showAlert(title: "Alert", message: "Please enter Code.", controller: self) {
                self.txtField2.becomeFirstResponder()
            }
            return
        }
        guard let txtFieldTxt3  = txtField3.text, !txtFieldTxt3.isEmpty else {
            self.showAlert(title: "Alert", message: "Please enter Code.", controller: self) {
                self.txtField3.becomeFirstResponder()
            }
            return
        }
        guard let txtFieldTxt4  = txtField4.text, !txtFieldTxt4.isEmpty else {
            self.showAlert(title: "Alert", message: "Please enter Code.", controller: self) {
                self.txtField4.becomeFirstResponder()
            }
            return
        }
        guard let txtFieldTxt5  = txtField5.text, !txtFieldTxt5.isEmpty else {
            self.showAlert(title: "Alert", message: "Please enter Code.", controller: self) {
                self.txtField5.becomeFirstResponder()
            }
            return
        }
        guard let txtFieldTxt6  = txtField7.text, !txtFieldTxt6.isEmpty else {
            self.showAlert(title: "Alert", message: "Please enter Code.", controller: self) {
                self.txtField7.becomeFirstResponder()
            }
            return
        }
        guard let newPasswordTxt  = newPasswordTF.text, !newPasswordTxt.isEmpty else {
            self.showAlert(title: "Alert", message: "Please enter new password.", controller: self) {
                self.newPasswordTF.becomeFirstResponder()
            }
            return
        }
        
        if newPasswordTxt.count < 6 {
            self.showAlert(title: "Alert", message: "Please enter password atleast 6 characters.", controller: self) {
                self.newPasswordTF.becomeFirstResponder()
            }
            return
        }
        guard let confirmPasswordTxt  = confirmPasswordTF.text, !confirmPasswordTxt.isEmpty else {
            self.showAlert(title: "Alert", message: "Please enter your confirm password.", controller: self) {
                self.confirmPasswordTF.becomeFirstResponder()
            }
            return
        }
        
        
        if confirmPasswordTxt != newPasswordTxt {
            self.showAlert(title: "Alert", message: "Confirm password mismatch.", controller: self) {
                self.confirmPasswordTF.becomeFirstResponder()
            }
            return
        }
        
        let passwordResetCode = txtFieldTxt1 + txtFieldTxt2 + txtFieldTxt3 + txtFieldTxt4 + txtFieldTxt5 + txtFieldTxt6
        var user =  RestPassword()
        user.emailAddress = verifyEmailAddress
        user.password = newPasswordTxt
        user.passwordResetCode = passwordResetCode
        
        let encode = JSONEncoder()
        let jsonData = try! encode.encode(user)
        print(jsonData)
        
        
        let url = URL(string: RESET_PASSWORD)!
        showSpinner(onView: self.view)
        WebServiceManager.sharedInstance.postRequest(params: jsonData , url: url, serviceType: "LOGIN", modelType: UserModel.self, success: { (response) in
            let responseObj = response as! UserModel
            self.removeSpinner()
            if responseObj.result?.sendStatus == true {
                DispatchQueue.main.async { () -> Void in
                    self.showAlert(title: "Alert", message: "Your password has been reset successfully", controller: self) {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "viewController") as! ViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            } else {
                DispatchQueue.main.async { () -> Void in
                    self.showAlert(title: "Alert", message: (responseObj.result?.message)!, controller: self) {
                    }
                }
            }

        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
        
        
        
        
    }
    @IBAction func navigateBackBtn(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "viewController") as! ViewController
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func tapOnResendBtn(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
}

extension VerifyOTPVC: UITextFieldDelegate {
func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

    print("abc here...")
    if ((textField.text?.count)! < 1 ) && (string.count > 0) {
        if textField == txtField1 {
            txtField2.becomeFirstResponder()
        }
        
        else if textField == txtField2 {
            txtField3.becomeFirstResponder()
        }
        
        else if textField == txtField3 {
            txtField4.becomeFirstResponder()
        }
        
        else if textField == txtField4 {
            txtField5.becomeFirstResponder()
        }
        else if textField == txtField5 {
            txtField7.becomeFirstResponder()
        }
        else if textField == txtField7 {
            txtField7.becomeFirstResponder()
        }
        
        textField.text = string
        return false
    } else if ((textField.text?.count)! >= 1) && (string.count == 0) {
        if textField == txtField2 {
            txtField1.becomeFirstResponder()
        }
        else if textField == txtField3 {
            txtField2.becomeFirstResponder()
        }
        else if textField == txtField4 {
            txtField3.becomeFirstResponder()
        }
        else if textField == txtField5 {
            txtField4.becomeFirstResponder()
        }
        else if textField == txtField7 {
            txtField5.becomeFirstResponder()
        }
        else if textField == txtField1 {
            txtField1.becomeFirstResponder()
        }
        textField.text = ""
        return false
    } else if (textField.text?.count)! >= 1 {
        textField.text = string
        return false
    }
    
    return true
    }
}

struct RestPassword: Codable {
    var emailAddress: String?
    var passwordResetCode : String?
    var password: String?
}
