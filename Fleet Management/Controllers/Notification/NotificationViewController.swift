//
//  NotificationViewController.swift
//  Fleet Management
//
//  Created by Atta Khan on 20/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var notificationItems: [NotificationItems]?
    private let refreshControl = UIRefreshControl()
    let cellSpacingHeight: CGFloat = 10
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.shared.setStatusBarBackgroundColor(view: view, color: UIColor.themeColor)
        // Do any additional setup after loading the view.
        getNotification()
        //refreshControl.tintColor = UIColor.themeColor
        refreshControl.addTarget(self, action: #selector(refreshTableViewData(_:)), for: .valueChanged)
        if #available(iOS 10, *){
          tableView.refreshControl = refreshControl
        } else {
          tableView.addSubview(refreshControl)
        }

    }
    @objc private func refreshTableViewData(_ sender: Any) {
        getNotification()
       
    }
    func setUpTableView() {
        tableView.register(NotificationTableViewCell.nib, forCellReuseIdentifier: NotificationTableViewCell.identifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    func getNotification() {
        let user_id = UserDefaults.standard.integer(forKey: "userId")
        let url = URL(string: GET_ALL_NOTIFICATION + "?UserId=\(user_id)&SkipCount=0&MaxResultCount=100")!
        showSpinner(onView: self.view)
        WebServiceManager.sharedInstance.getRequest(params: nil, url: url, serviceType: "Get Upcoming schedule", modelType: NotificationModel.self, success: { (response) in
            let responseObj = response as! NotificationModel
            self.removeSpinner()
            if responseObj.success == true {
                self.notificationItems = responseObj.result?.items
                DispatchQueue.main.async {
                    self.refreshControl.endRefreshing()
                    self.setUpTableView()
                }
                
            }
        }, fail: { (error) in
            self.removeSpinner()
            DispatchQueue.main.async { () -> Void in
                self.refreshControl.endRefreshing()
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
    }
    @IBAction func backNavigateBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension NotificationViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if notificationItems?.count ?? 0 == 0 {
            tableView.setEmptyView(message: "No Notification Available", image: UIImage(named: "nochat")!)
        } else {
            tableView.restore()
        }
        return notificationItems?.count ?? 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }

    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
   
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NotificationTableViewCell.identifier) as! NotificationTableViewCell
        cell.data = notificationItems?[indexPath.section]
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88
    }
    
}
