//
//  Signup1ViewController.swift
//  Fleet Management
//
//  Created by Atta Khan on 31/03/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit

class Signup1ViewController: UIViewController {
    @IBOutlet weak var fourthLineBtn: UIButton!
    @IBOutlet weak var fourthRadioBtn: UIButton!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var employeeIDTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var firstNameTF: UITextField!
    
    @IBOutlet weak var stepLbl: UILabel!
    var routeSelectionStatus: Bool?
    var isValidLength : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.shared.setStatusBarBackgroundColor(view: view, color: UIColor.themeColor)
        lastNameTF.delegate = self
        firstNameTF.delegate = self
        employeeIDTF.delegate = self
        //passwordTF.delegate = self
        firstNameTF.keyboardType = .asciiCapable
        lastNameTF.keyboardType = .asciiCapable
        //emailTF.keyboardType = .asciiCapable
        passwordTF.keyboardType = .asciiCapable
        employeeIDTF.keyboardType = .asciiCapableNumberPad
        checkSettingForMobile()
        
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func tapOnNextBtn(_ sender: Any) {
        
       guard let firstNameTxt  = firstNameTF.text, !firstNameTxt.isEmpty else {
            self.showAlert(title: "Alert", message: "Please enter your first name.", controller: self) {
                self.firstNameTF.becomeFirstResponder()
            }
            return
        }
       
        guard let lstNameTxt  = lastNameTF.text, !lstNameTxt.isEmpty else {
            self.showAlert(title: "Alert", message: "Please enter your last name.", controller: self) {
                self.lastNameTF.becomeFirstResponder()
            }
            return
        }
        
        guard let employeeIdTxt  = employeeIDTF.text, !employeeIdTxt.isEmpty else {
            self.showAlert(title: "Alert", message: "Please enter your employee Id.", controller: self) {
                self.employeeIDTF.becomeFirstResponder()
            }
            return
        }
        
        guard let emailTxt  = emailTF.text, !emailTxt.isEmpty else {
            self.showAlert(title: "Alert", message: "Please enter your email.", controller: self) {
                self.emailTF.becomeFirstResponder()
            }
            return
        }
        if !emailTxt.isValidEmail {
           self.showAlert(title: "Alert", message: "Please enter valid email.", controller: self) {
                self.emailTF.becomeFirstResponder()
            }
            return
        }
        
        guard let passwordTxt  = passwordTF.text, !passwordTxt.isEmpty else {
            self.showAlert(title: "Alert", message: "Please enter your password.", controller: self) {
                self.passwordTF.becomeFirstResponder()
            }
            return
        }
        
        if passwordTxt.count < 6 {
            self.showAlert(title: "Alert", message: "Please enter password atleast 6 characters.", controller: self) {
                self.passwordTF.becomeFirstResponder()
            }
            return
        }
        UserDefaults.standard.set(firstNameTxt, forKey: "firstName")
        UserDefaults.standard.set(lstNameTxt, forKey: "lastName")
        UserDefaults.standard.set(emailTxt, forKey: "email")
        UserDefaults.standard.set(passwordTxt, forKey: "password")
        UserDefaults.standard.set(employeeIdTxt, forKey: "employeeId")
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "Signup2ViewController") as! Signup2ViewController
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func navigateBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func checkSettingForMobile() {
        let url = URL(string: GET_SETTING_MOBILE)!
        WebServiceManager.sharedInstance.getRequest(params: nil, url: url, serviceType: "Get setting", modelType: SettingModel.self, success: { (response) in
            let responseObj = response as! SettingModel
            if responseObj.result?.count ?? 0 > 0 {
                self.routeSelectionStatus = responseObj.result?[0].value
                UserDefaults.standard.set(self.routeSelectionStatus, forKey: "routeSelectionStatus")
                if self.routeSelectionStatus == false {
                    DispatchQueue.main.async { () -> Void in
                        self.fourthLineBtn.isHidden = true
                        self.fourthRadioBtn.isHidden = true
                        self.stepLbl.text = "Step 1/3"
                    }
                }
            }
        }, fail: { (error) in
            
        }, showHUD: true)
    }
}
extension Signup1ViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == firstNameTF || textField == lastNameTF {
            if range.location == 0 && string == " " { // prevent space on first character
                return false
            }

            if textField.text?.last == " " && string == " " { // allowed only single space
                return false
            }

            if string == " " { return true } // now allowing space between name

            if string.rangeOfCharacter(from: CharacterSet.letters.inverted) != nil {
                return false
            }
            return true
        }
        if textField == employeeIDTF {
            let maxLength = 6
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
               currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
//        if textField == passwordTF {
//            let fieldTextLength = textField.text!.count
//             if  fieldTextLength > 5 {
//                 isValidLength = true
//             } else {
//                 isValidLength = false
//             }
//
//             return true
//        }
        return true
    }
}
