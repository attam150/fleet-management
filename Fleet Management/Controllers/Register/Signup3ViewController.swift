//
//  Signup3ViewController.swift
//  Fleet Management
//
//  Created by Atta Khan on 31/03/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit

class Signup3ViewController: UIViewController {

    @IBOutlet weak var entityTF: UITextField!
    @IBOutlet weak var departmentTF: UITextField!
    @IBOutlet weak var phoneNbTF: UITextField!
    @IBOutlet weak var femaleBtn: UIButton!
    @IBOutlet weak var maleBtn: UIButton!
    
    @IBOutlet weak var stepLbl: UILabel!
    
    @IBOutlet weak var fourtLineBtn: UIButton!
    @IBOutlet weak var fourthRadioBtn: UIButton!
    
    @IBOutlet weak var stepNumberLbl: UILabel!
    var entityData: [EntityResult]?
    var departmentData: [EntityResult]?
    var selectedEntity: Int = 0
    var selectedDepartment: Int = 0
    var genderValue = "Male"
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.shared.setStatusBarBackgroundColor(view: view, color: UIColor.themeColor)
        departmentTF.keyboardType = .asciiCapable
        phoneNbTF.keyboardType = .asciiCapableNumberPad
        entityTF.keyboardType = .asciiCapable
        departmentTF.delegate = self
        entityTF.delegate = self
        getEntity()
        let status = UserDefaults.standard.bool(forKey: "routeSelectionStatus")
        if status == false {
            self.fourtLineBtn.isHidden = true
            self.fourthRadioBtn.isHidden = true
            self.stepLbl.text = "Step 3/3"
            self.stepNumberLbl.text = "Step 3"
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    @IBAction func tapOnGenderBtn(_ sender: UIButton) {
        if sender.tag == 2 {
            maleBtn.backgroundColor = UIColor(red: 245.0/255.0, green: 249.0/255.0, blue: 251.0/255.0, alpha: 1.0)
            maleBtn.setTitleColor(UIColor.themeColor, for: .normal)
            femaleBtn.backgroundColor = UIColor.themeColor
            femaleBtn.setTitleColor(UIColor.white, for: .normal)
            genderValue = "Female"
        } else {
            maleBtn.backgroundColor = UIColor.themeColor
            maleBtn.setTitleColor(UIColor.white, for: .normal)
            femaleBtn.backgroundColor = UIColor(red: 245.0/255.0, green: 249.0/255.0, blue: 251.0/255.0, alpha: 1.0)
            femaleBtn.setTitleColor(UIColor.themeColor, for: .normal)
            genderValue = "Male"
        }
        
    }
    func getEntity() {
        let url = URL(string: GET_ENTITY)!
        showSpinner(onView: self.view)
        WebServiceManager.sharedInstance.getRequest(params: nil, url: url, serviceType: "Get City", modelType: EntityModel.self, success: { (response) in
            let responseObj = response as! EntityModel
            self.removeSpinner()
            self.entityData = responseObj.result
            
        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
    }
    func getDepartment(_ entityId: Int) {
        let url = URL(string: GET_DEPARTMENT + "?companyEntityId=\(entityId)")!
        showSpinner(onView: self.view)
        WebServiceManager.sharedInstance.getRequest(params: nil, url: url, serviceType: "Get City", modelType: EntityModel.self, success: { (response) in
            let responseObj = response as! EntityModel
            self.removeSpinner()
            self.departmentData = responseObj.result
        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
    }
    
    private func chooseValue(_ tag: Int, title: String, _ selected: Int) {
        let vc = UIViewController()
        vc.preferredContentSize = CGSize(width: 250,height: 200)
        let pickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: 250, height: 200))
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.tag = tag
        pickerView.selectRow(selected, inComponent:0, animated:true)
        vc.view.addSubview(pickerView)
        let editRadiusAlert = UIAlertController(title: title, message: "", preferredStyle: UIAlertController.Style.alert)
        editRadiusAlert.setValue(vc, forKey: "contentViewController")
        editRadiusAlert.addAction(UIAlertAction(title: "Done", style: .default, handler: { (alert: UIAlertAction!) in
            if tag == 1 {
                self.entityTF.text = self.entityData?[self.selectedEntity].name
                self.getDepartment(self.selectedEntity)
                self.departmentTF.text = ""
                self.selectedDepartment = 0
            } else {
                self.departmentTF.text = self.departmentData?[self.selectedDepartment].name
            }
        }))
        self.present(editRadiusAlert, animated: true)
    }
    
    @IBAction func navigateBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapOnEntityBtn(_ sender: Any) {
        guard let _ = entityData else {
            self.showAlert(title: "Alert", message: "No data found.", controller: self) {
            }
            return
        }
        chooseValue(1, title: "Choose Entity", selectedEntity)
    }
    
    @IBAction func tapOnDepartmentBtn(_ sender: Any) {
        guard let _ = departmentData else {
            self.showAlert(title: "Alert", message: "No data found.", controller: self) {
            }
            return
        }
        if departmentData?.count ?? 0 > 0 {
            chooseValue(2, title: "Choose Department", selectedDepartment)
        } else {
            self.showAlert(title: "Alert", message: "No data found.", controller: self) {
            }
        }
    }
    
    
    @IBAction func tapOnNextBtn(_ sender: Any) {
        let firstName = UserDefaults.standard.string(forKey: "firstName")
        let lastName = UserDefaults.standard.string(forKey: "lastName")
        let password = UserDefaults.standard.string(forKey: "password")
        let email = UserDefaults.standard.string(forKey: "email")
        let city = UserDefaults.standard.string(forKey: "city")
        let office = UserDefaults.standard.string(forKey: "office")
        let pickAddress = UserDefaults.standard.string(forKey: "pickupaddress")
        let employeeId = UserDefaults.standard.string(forKey: "employeeId")
        let pickupLat = UserDefaults.standard.double(forKey: "pickupLat")
        let pickupLng = UserDefaults.standard.double(forKey: "pickupLng")
        let dropOfflat = UserDefaults.standard.double(forKey: "dropOfflat")
        let dropOfflng = UserDefaults.standard.double(forKey: "dropOfflng")
        let routeId = UserDefaults.standard.integer(forKey: "routeId")
        let dropAdress = UserDefaults.standard.double(forKey: "dropAdress")
        let directionPolyline = UserDefaults.standard.string(forKey: "directionPolyline")
        
        guard let phonenumberTxt  = phoneNbTF.text, !phonenumberTxt.isEmpty else {
            self.showAlert(title: "Alert", message: "Please enter your phone number.", controller: self) {
                self.phoneNbTF.becomeFirstResponder()
            }
            return
        }
        if phoneNbTF.text?.validateMobileNumber() == false {
            self.showAlert(title: "Alert", message: "Please enter valid phone number.", controller: self) {
                self.phoneNbTF.becomeFirstResponder()
            }
            return
        }
        guard let entityTxt  = entityTF.text, !entityTxt.isEmpty else {
            self.showAlert(title: "Alert", message: "Please enter your entity.", controller: self) {
                self.entityTF.becomeFirstResponder()
            }
            return
        }
        guard let departmentTxt  = departmentTF.text, !departmentTxt.isEmpty else {
            self.showAlert(title: "Alert", message: "Please enter your department.", controller: self) {
                self.departmentTF.becomeFirstResponder()
            }
            return
        }
        
        
        var user = RegisterUser()
        user.name = firstName
        user.surname = lastName
        user.userName = email
        user.emailAddress = email
        user.password = password
        user.phoneNumber = phonenumberTxt
        user.employeeId = employeeId
        user.gender = genderValue
        user.pickupLat = pickupLat
        user.pickupLong = pickupLng
        user.pickupAddress = pickAddress
        user.dropLat = dropOfflat
        user.dropLong = dropOfflng
        user.dropAddress = office
        user.city = city
        user.department = departmentTxt
        user.entity = entityTxt
        user.routeId = routeId
        user.role = 3
        user.directionPolyline = directionPolyline
        let encode = JSONEncoder()
        let jsonData = try! encode.encode(user)
        
        
        let url = URL(string: REGISTER)!
        showSpinner(onView: self.view)
        WebServiceManager.sharedInstance.postRequest(params: jsonData , url: url, serviceType: "LOGIN", modelType: UserModel.self, success: { (response) in
            let responseObj = response as! UserModel
            self.removeSpinner()
            if responseObj.success == false {
                DispatchQueue.main.async { () -> Void in
                    self.showAlert(title: "Alert", message: (responseObj.error?.message)!, controller: self) {
                    }
                }
            } else {
                DispatchQueue.main.async { () -> Void in
                    self.showAlert(title: "Alert", message: "The account is pending to be active from Admin side.", controller: self) {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "viewController") as! ViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                        }
                }
            }
        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
    }
}
extension Signup3ViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == entityTF || textField == departmentTF {
            if range.location == 0 && string == " " { // prevent space on first character
                return false
            }

            if textField.text?.last == " " && string == " " { // allowed only single space
                return false
            }

            if string == " " { return true } // now allowing space between name

            if string.rangeOfCharacter(from: CharacterSet.letters.inverted) != nil {
                return false
            }
            return true
        }
        
        return true
    }
}
extension Signup3ViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1 {
            return entityData?.count ?? 0
        } else {
            return departmentData?.count ?? 0
        }
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1 {
            return entityData?[row].name
        } else {
            return departmentData?[row].name
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1 {
            entityTF.text = entityData?[row].name
            selectedEntity = row
        } else {
            departmentTF.text = departmentData?[row].name
            selectedDepartment = row
            
        }
    }
}
