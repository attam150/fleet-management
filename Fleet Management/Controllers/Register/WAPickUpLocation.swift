//
//  WAPickUpLocation.swift
//  WhereApp
//
//  Created by Salman on 05/06/2017.
//  Copyright © 2017 Salman. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import SwiftyJSON
import Alamofire

var DEVICE_LAT =  31.5204
var DEVICE_LONG = 74.3587

protocol PickUpLatLnt {
    func pickUpLatitudeAndLong(_ lon:Double, andLatitude lat:Double, title: String)
}

class WAPickUpLocation: UIViewController, LocateOnTheMap, GMSAutocompleteFetcherDelegate , UISearchBarDelegate {
    
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet var pinImage: UIImageView!
    @IBOutlet var tblView: UITableView!
    @IBOutlet var textFieldSearch: UITextField!
    @IBOutlet var dropOffTF: UITextField!
    @IBOutlet var viewOfMaps: GMSMapView!
    var resultsArray = [String]()
    var gmsFetcher: GMSAutocompleteFetcher!
    var searchResultController: SearchResultsController!
    var delegate: PickUpLatLnt!
    var latitude :  Double?
    var lngitude :  Double?
    var currentLatitude :  Double?
    var currentLngitude :  Double?
    var address :  String?
    var profile_data: ProfileData?
    var isPickingLocation: Bool = false
    var isFirstTime: Bool = false
    var isSelectLocatin: Bool = false
    var pickMarker: GMSMarker?
    var dragMarker = GMSMarker()
    var drofOffLoction: String?
    var dropOffLat: Double?
    var dropoffLong: Double?
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var isConfirmed: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.shared.setStatusBarBackgroundColor(view: view, color: UIColor.themeColor)
        //getting the current location
        //viewOfMaps.isMyLocationEnabled = true
        viewOfMaps.settings.myLocationButton = true
        viewOfMaps.padding = UIEdgeInsets(top: 0, left: 0, bottom: 60, right: 0)
        viewOfMaps.mapStyle(withFilename: "mapStyle", andType: "json")
        setDefaultLocationOnMap()
        setupUI()
        textFieldSearch.keyboardType = .asciiCapable
        textFieldSearch.delegate = self
        if let dropOffLoc = drofOffLoction {
            dropOffTF.text = dropOffLoc
        }
        
        if let data = profile_data {
            textFieldSearch.text = data.pickupAddress
            dropOffLat = data.dropLat
            dropoffLong = data.dropLong
            latitude = data.pickupLat
            lngitude = data.pickupLong
            drawPath()
        }
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        Utility.shared.setStatusBarBackgroundColor(view: view, color: UIColor.themeColor)
        tblView.isHidden = true
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        searchResultController = SearchResultsController()
        searchResultController.delegate = self
        
        let nsBoundsCorner = CLLocationCoordinate2D(latitude: 33.738045, longitude: 73.084488)

        let bounds = GMSCoordinateBounds(coordinate: nsBoundsCorner, coordinate: nsBoundsCorner)

        let filter = GMSAutocompleteFilter()
        filter.type = .address
        
        gmsFetcher  = GMSAutocompleteFetcher(bounds: bounds, filter: filter)
        
        
        //gmsFetcher = GMSAutocompleteFetcher()

        gmsFetcher.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: custom methods
    func setupUI() {
        self.viewOfMaps.delegate = self
        self.viewOfMaps?.isMyLocationEnabled = true
        //self.viewOfMaps.mapType = GMSMapViewType(rawValue: 3)!
    }
    
    
    func setDefaultLocationOnMap() {
        if self.latitude != nil  && self.lngitude != nil {
            setLocationOnMap(lat: self.latitude!, long: self.lngitude!)
        } else {
            /*_ =  Location.getLocation(withAccuracy:.block, frequency: .oneShot, onSuccess: { location in
                print("loc \(location.coordinate.longitude)\(location.coordinate.latitude)")
                DEVICE_LAT = location.coordinate.latitude
                DEVICE_LONG = location.coordinate.longitude
                self.latitude = DEVICE_LAT
                self.lngitude = DEVICE_LONG
                
                //setting location on map
                self.setLocationOnMap(lat: self.latitude!, long: self.lngitude!)
                
            }) { (last, error) in
                
                print("Something bad has occurred \(error)")
                
                //setting location on map
                self.setLocationOnMap(lat: DEVICE_LAT, long: DEVICE_LONG)
                
            }
 */
        }
    }
    
    func setLocationOnMap(lat: Double, long: Double) {
        
        DispatchQueue.main.async(execute: {() -> Void in
            self.viewOfMaps.mapType = GMSMapViewType(rawValue: 3)!
            
            let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 16)
            self.viewOfMaps.camera = camera
            
            //#2
//            let position = CLLocationCoordinate2DMake(lat, long)
//            let marker = GMSMarker(position: position)
//            marker.icon = UIImage(named: "pin-3")
//            marker.map = self.viewOfMaps
            
            
        })
        self.viewOfMaps.settings.myLocationButton = false
    }
    
    
    @IBAction func confirmPickBtn(_ sender: Any) {
        if textFieldSearch.text == "" {
            self.showAlert(title: "Alert", message: "Please enter your pickup address.", controller: self) {
            }
        } else {
            if isConfirmed == true {
                if let searchAddress = textFieldSearch.text, !searchAddress.isEmpty  {
                    
                    if let data = profile_data {
                        let status = UserDefaults.standard.bool(forKey: "routeSelectionStatus")
                        if status == false {
                            profile_data?.pickupAddress = searchAddress
                            profile_data?.pickupLong = self.lngitude!
                            profile_data?.pickupLat = self.latitude!
                            let encode = JSONEncoder()
                            let jsonData = try! encode.encode(profile_data)
                            let url = URL(string: UPDATE_USER_PROFILE)!
                            showSpinner(onView: self.view)
                            WebServiceManager.sharedInstance.putRequest(params: jsonData , url: url, serviceType: "Update profile", modelType: UserProfileModel.self, success: { (response) in
                                let responseObj = response as! UserProfileModel
                                self.removeSpinner()
                                if responseObj.success == true {
                                    self.profile_data = responseObj.result
                                    if let isProfileApproved = responseObj.result?.isProfileApproved {
                                       UserDefaults.standard.set(isProfileApproved, forKey: "isProfileApproved")
                                    }
                                    DispatchQueue.main.async { () -> Void in
                                        self.showAlert(title: "Alert", message: "Data updated successfully.", controller: self) {
                                            _ = self.navigationController?.popViewController(animated: true)
                                        }
                                    }
                                } else {
                                    print(responseObj.error?.message)
                                }
                            }, fail: { (error) in
                                self.removeSpinner()
                                print(error)
                                DispatchQueue.main.async { () -> Void in
                                    self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                                    }
                                }
                            }, showHUD: true)
                            
                        } else {
                            let vc = storyboard?.instantiateViewController(withIdentifier: "UpdateRouteViewController") as! UpdateRouteViewController
                            profile_data?.pickupAddress = searchAddress
                            profile_data?.pickupLong = self.lngitude!
                            profile_data?.pickupLat = self.latitude!
                            vc.profile_data = profile_data
                            vc.isComingfrom = "update address"
                            //vc.delegate = self
                            navigationController?.pushViewController(vc, animated: true)
                        }
                        
                    } else {
                        delegate.pickUpLatitudeAndLong( self.lngitude!, andLatitude: self.latitude!, title: searchAddress)
                        _ = self.navigationController?.popViewController(animated: true)
                    }
                }
            } else {
                isConfirmed = true
                confirmBtn.setTitle("Submit Address", for: .normal)
                self.drawPath()
            }
        }
        
    }
    
    @IBAction func goBack(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }

    
    func pickupLocationAddress(_ lon: Double, andLatitude lat: Double) {
        let position = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        self.dragMarker.position = position
        let gooogle = GMSGeocoder()
        gooogle.reverseGeocodeCoordinate(position) { (response, error) in
            self.isPickingLocation = false
            if response != nil {
                let obj = (response?.results()?.first)! as GMSAddress
                for str in obj.lines! {
                    if str.count > 0 {
                        self.address = str
                        self.textFieldSearch.text = str
                        return
                    }
                }
            }
        }
        DispatchQueue.main.async { () -> Void in
            print("lat is a = \(lat)")
            print("lng is a = \(lon)")
            print("position is a = \(position)")
            let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lon, zoom: 16)
            self.viewOfMaps.camera = camera
            self.dragMarker.title = self.address
            self.dragMarker.map = self.viewOfMaps
            
        }
    }
    
    // pickUp Location Delegate method
    
    func locateWithLongitude(_ lon: Double, andLatitude lat: Double, andTitle title: String) {
        
        DispatchQueue.main.async { () -> Void in
            
            let position = CLLocationCoordinate2DMake(lat, lon)
            self.dragMarker.position = position
            let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lon, zoom: 16)
            self.viewOfMaps.camera = camera
            self.dragMarker.icon = UIImage(named: "dragPickerIcon")
            self.dragMarker.title = "Address : \(title)"
            self.latitude = lat
            self.lngitude = lon
            self.address = title
            
            self.dragMarker.map = self.viewOfMaps
            
        }
        
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.resultsArray.removeAll()
        gmsFetcher?.sourceTextHasChanged(searchText)
    }
    
    
    // Google place autocomplete method
    
    /**
     * Called when an autocomplete request returns an error.
     * @param error the error that was received.
     */
    public func didFailAutocompleteWithError(_ error: Error) {
        //        resultText?.text = error.localizedDescription
    }
    
    /**
     * Called when autocomplete predictions are available.
     * @param predictions an array of GMSAutocompletePrediction objects.
     */
    public func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
        //self.resultsArray.count + 1
        
        for prediction in predictions {
            
            if let prediction = prediction as GMSAutocompletePrediction?{
                self.resultsArray.append(prediction.attributedFullText.string)
            }
        }
        //self.searchResultController.reloadDataWithArray(self.resultsArray)
        //   self.searchResultsTable.reloadDataWithArray(self.resultsArray)
        tblView.delegate = self
        tblView.dataSource = self
        tblView.reloadData()
        print(resultsArray)
    }
    
    // Google map Method
    
    
    
}



extension WAPickUpLocation: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.resultsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        if indexPath.row < self.resultsArray.count {
            cell.textLabel?.text = self.resultsArray[indexPath.row]
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isConfirmed = false
        confirmBtn.setTitle("Confirm Pickup", for: .normal)
        isSelectLocatin = true
        tableView.isHidden = true
        address = self.resultsArray[indexPath.row]
        textFieldSearch.text = address
        viewOfMaps.clear()
        dragMarker.isDraggable = false
        /* */
        let urlpath = "https://maps.googleapis.com/maps/api/geocode/json?address=\(address!)&sensor=false&key=\(API_KEY)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                isPickingLocation = true
                let url = URL(string: urlpath!)
                self.resultsArray.removeAll()
                print(url!)
                let task = URLSession.shared.dataTask(with: url! as URL) { (data, response, error) -> Void in
                    // 3
                    self.isPickingLocation = false
                    do {
                        if data != nil {
                            let dic = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as! NSDictionary
                            
                            self.latitude =   (((((dic.value(forKey: "results") as! NSArray).object(at: 0) as! NSDictionary).value(forKey: "geometry") as! NSDictionary).value(forKey: "location") as! NSDictionary).value(forKey: "lat")) as? Double
                            
                            self.lngitude =   (((((dic.value(forKey: "results") as! NSArray).object(at: 0) as! NSDictionary).value(forKey: "geometry") as! NSDictionary).value(forKey: "location") as! NSDictionary).value(forKey: "lng")) as? Double
                            
                            
                            DispatchQueue.main.async { () -> Void in
                                //#1
                                let position = CLLocationCoordinate2DMake(self.latitude!, self.lngitude!)
                                let marker = GMSMarker(position: position)
                                self.dragMarker.position = position
                                //marker.icon = #imageLiteral(resourceName: "pin1")
                                let camera = GMSCameraPosition.camera(withLatitude: self.latitude!, longitude: self.lngitude!, zoom: 16)
                                self.dragMarker.map = self.viewOfMaps
                                self.viewOfMaps.animate(toLocation: CLLocationCoordinate2D(latitude: self.latitude!, longitude: self.lngitude!))
                                self.viewOfMaps.camera = camera
                            }
                            
                        }
                        
                    }catch {
                        print("Error")
                    }
                }
                // 5
                task.resume()
        
    }
    
}

extension WAPickUpLocation: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if tblView.isHidden && textField.text != nil && (textField.text?.count)! > 0 {
            tblView.isHidden = false
        }
        self.resultsArray.removeAll()
        let searchStr = textFieldSearch.text! + string
        gmsFetcher.sourceTextHasChanged(searchStr)
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        tblView.isHidden = true
        return true
    }
}
extension WAPickUpLocation: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if isFirstTime == false {
            //self.pinImage.isHidden = false
            //mapView.clear()
        }
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        
        if isSelectLocatin == true {
            isSelectLocatin = false
            return
        }
        
        isPickingLocation = true
//        self.latitude = position.target.latitude
//        self.lngitude = position.target.longitude
        
        //pickupLocationAddress()
    }
    
    func mapViewSnapshotReady(_ mapView: GMSMapView) {
        isFirstTime = false
    }
    
    func createMarker(iconMarker: UIImage, latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(latitude, longitude)
        marker.title = drofOffLoction
        marker.icon = iconMarker
        marker.map = self.viewOfMaps
    }
    
    func drawPath() {

        self.createMarker(iconMarker: #imageLiteral(resourceName: "markerIcon"), latitude: self.dropOffLat!, longitude: self.dropoffLong!)
        let origin = "\(self.latitude!),\(self.lngitude!)"
        let destination = "\(self.dropOffLat!),\(self.dropoffLong!)"
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(API_KEY)"
        AF.request(url).responseJSON { response in
//            print(response.request as Any)  // original URL request
//            print(response.response as Any) // HTTP URL response
//            print(response.data as Any)     // server data
//            print(response.result as Any)   // result of response serialization
            
            do {
                let json = try JSON(data: response.data!)
                let routes = json["routes"].arrayValue

                // print route using Polyline
                for route in routes
                {
                    let routeOverviewPolyline = route["overview_polyline"].dictionary
                    let points = routeOverviewPolyline?["points"]?.stringValue
                    if let polylinePoints = points {
                        UserDefaults.standard.set(polylinePoints, forKey: "directionPolyline")
                    }
                    let path = GMSPath.init(fromEncodedPath: points!)
                    let polyline = GMSPolyline.init(path: path)
                    polyline.strokeWidth = 4
                    polyline.strokeColor = UIColor.themeColor
                    polyline.map = self.viewOfMaps
                }
            } catch {
                print(error)
            }
            
            
            
            
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {

    }
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        dragMarker.isDraggable = true
        print("Tapped at coordinate: " + String(coordinate.latitude) + " "
        + String(coordinate.longitude))
    }
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
        
    }
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        self.viewOfMaps.clear()
        isConfirmed = false
        confirmBtn.setTitle("Confirm Pickup", for: .normal)
        self.latitude = marker.position.latitude
        self.lngitude = marker.position.longitude
        pickupLocationAddress(self.lngitude!, andLatitude: self.latitude!)
    }
    
    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
        //print(marker.position.latitude)
    }
    
}
extension WAPickUpLocation: CLLocationManagerDelegate {

    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        self.viewOfMaps.clear()
        isConfirmed = false
        confirmBtn.setTitle("Confirm Pickup", for: .normal)
        self.latitude = self.currentLatitude!
        self.lngitude = self.currentLngitude!
//        let position = CLLocationCoordinate2D(latitude: self.currentLatitude!, longitude: self.currentLngitude!)
//        self.dragMarker.position = position
//        self.dragMarker.map = self.viewOfMaps
        pickupLocationAddress(currentLngitude!, andLatitude: currentLatitude!)
        return false
    }
    
  // Handle incoming location events.
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    let location: CLLocation = locations.last!
    currentLatitude = location.coordinate.latitude
    currentLngitude = location.coordinate.longitude
    if let data = profile_data {
        self.latitude = data.pickupLat
        self.lngitude = data.pickupLong
    } else {
        self.latitude = location.coordinate.latitude
        self.lngitude = location.coordinate.longitude
    }
    
    if let lat = self.latitude, let lng = self.lngitude {
        let camera = GMSCameraPosition.camera(withLatitude:  lat,
                                              longitude: lng,
                                              zoom: 16)
        
        dragMarker.position = CLLocationCoordinate2DMake(lat, lng)
        dragMarker.map = self.viewOfMaps
        dragMarker.icon = UIImage(named: "dragPickerIcon")
        self.viewOfMaps.camera = camera
    }
  }

  // Handle location manager errors.
  func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    locationManager.stopUpdatingLocation()
    print("Error: \(error)")
  }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            self.textFieldSearch.isUserInteractionEnabled = true
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            self.textFieldSearch.isUserInteractionEnabled = true
            manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            // If always authorized
            self.textFieldSearch.isUserInteractionEnabled = true
            manager.startUpdatingLocation()
            break
        case .restricted:
            self.textFieldSearch.isUserInteractionEnabled = true
            // If restricted by e.g. parental controls. User can't enable Location Services
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            
            self.textFieldSearch.isUserInteractionEnabled = false
            openSettingApp()
            break
        default:
            break
        }
    }
}
