//
//  Signup2ViewController.swift
//  Fleet Management
//
//  Created by Atta Khan on 31/03/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit

class Signup2ViewController: UIViewController {

    @IBOutlet weak var cityTF: UITextField!
    @IBOutlet weak var addressTF: UITextField!
    @IBOutlet weak var officeTF: UITextField!
    
    @IBOutlet weak var fourthLineBtn: UIButton!
    @IBOutlet weak var fourthRadioBtn: UIButton!
    @IBOutlet weak var officeBtn: UIButton!
    @IBOutlet weak var cityBtn: UIButton!
    @IBOutlet weak var addressBtn: UIButton!
    
    @IBOutlet weak var stepLbl: UILabel!
    var pickupLat: Double?
    var pickupLng: Double?
    var dropOfflat: Double?
    var dropOfflng: Double?
    var selectedCity: Int = 0
    var selectedOffice: Int = 0
    var uniqueCity: [Route]?
    var filterOffice: [Route]?
    var data: [Route]?
    var routeSelectionStatus: Bool?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getOfficeBuilding()
        Utility.shared.setStatusBarBackgroundColor(view: view, color: UIColor.themeColor)
        let status = UserDefaults.standard.bool(forKey: "routeSelectionStatus")
        if status == false {
            self.fourthLineBtn.isHidden = true
            self.fourthRadioBtn.isHidden = true
            self.stepLbl.text = "Step 2/3"
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    func getOfficeBuilding() {
        let url = URL(string: GET_OFFICE_BUILDING)!
        showSpinner(onView: self.view)
        WebServiceManager.sharedInstance.getRequest(params: nil, url: url, serviceType: "Get City", modelType: ResultModel.self, success: { (response) in
            let responseObj = response as! ResultModel
            self.removeSpinner()
            if responseObj.result?.totalCount ?? 0 > 0 {
                self.data = responseObj.result?.items
                self.uniqueCity?.removeAll()
                guard let cities = self.data else {
                   return
                }
                self.uniqueCity = cities.unique(by: { $0.city })
                
            }
        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
    }
    
    private func chooseValue(_ tag: Int, title: String, _ selected: Int) {
        let vc = UIViewController()
        vc.preferredContentSize = CGSize(width: 250,height: 200)
        let pickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: 250, height: 200))
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.tag = tag
        pickerView.selectRow(selected, inComponent:0, animated:true)
        vc.view.addSubview(pickerView)
        let editRadiusAlert = UIAlertController(title: title, message: "", preferredStyle: UIAlertController.Style.alert)
        editRadiusAlert.setValue(vc, forKey: "contentViewController")
        editRadiusAlert.addAction(UIAlertAction(title: "Done", style: .default, handler: { (alert: UIAlertAction!) in
            if tag == 1 {
                self.cityTF.text = self.uniqueCity?[self.selectedCity].city
                let cityName = self.uniqueCity?[self.selectedCity].city
                self.filterOffice?.removeAll()
                self.filterOffice = self.data?.filter( { $0.city == cityName} )
                self.officeTF.text = ""
                self.selectedOffice = 0
            } else {
                self.officeTF.text = self.filterOffice?[self.selectedOffice].name
                self.dropOfflat = self.filterOffice?[self.selectedOffice].lat
                self.dropOfflng = self.filterOffice?[self.selectedOffice].long
            }
        }))
        self.present(editRadiusAlert, animated: true)
    }
    
    @IBAction func tapOnCityBtn(_ sender: Any) {
        guard let _ = uniqueCity else {
            self.showAlert(title: "Alert", message: "No data found.", controller: self) {
            }
            return
        }
        chooseValue(1, title: "City", selectedCity)
        
    }
    
    @IBAction func tapOnOfficebtn(_ sender: Any) {
        guard let _ = filterOffice else {
            self.showAlert(title: "Alert", message: "No data found.", controller: self) {
            }
            return
        }
        chooseValue(2, title: "Office", selectedOffice)
        
    }
    
    @IBAction func tapOnAddressBtn(_ sender: Any) {
        if officeTF.text != "" {
            let vc = storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! WAPickUpLocation
            vc.drofOffLoction = self.filterOffice?[self.selectedOffice].name
            vc.dropOffLat = self.filterOffice?[self.selectedOffice].lat
            vc.dropoffLong = self.filterOffice?[self.selectedOffice].long
            vc.delegate = self
            navigationController?.pushViewController(vc, animated: true)
        } else {
           self.showAlert(title: "Alert", message: "Please select your city.", controller: self) {
                self.cityTF.becomeFirstResponder()
            }
            return
        }
    }
    
    

    @IBAction func navigateBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapOnNextBtn(_ sender: Any) {
        guard let cityTxt  = cityTF.text, !cityTxt.isEmpty else {
            self.showAlert(title: "Alert", message: "Please select your city.", controller: self) {
                self.cityTF.becomeFirstResponder()
            }
            return
        }
        
        guard let officeTxt  = officeTF.text, !officeTxt.isEmpty else {
            self.showAlert(title: "Alert", message: "Please select your office.", controller: self) {
                self.officeTF.becomeFirstResponder()
            }
            return
        }
        
        guard let addressTxt  = addressTF.text, !addressTxt.isEmpty else {
            self.showAlert(title: "Alert", message: "Please enter your address.", controller: self) {
                self.addressTF.becomeFirstResponder()
            }
            return
        }
        UserDefaults.standard.set(cityTxt, forKey: "city")
        UserDefaults.standard.set(officeTxt, forKey: "office")
        UserDefaults.standard.set(addressTxt, forKey: "pickupaddress")
        UserDefaults.standard.set(pickupLat, forKey: "pickupLat")
        UserDefaults.standard.set(pickupLng , forKey: "pickupLng")
        UserDefaults.standard.set(dropOfflat, forKey: "dropOfflat")
        UserDefaults.standard.set(dropOfflng , forKey: "dropOfflng")
        let status = UserDefaults.standard.bool(forKey: "routeSelectionStatus")
        if status == false {
            UserDefaults.standard.set(0, forKey: "routeId")
            UserDefaults.standard.set(officeTxt, forKey: "dropAdress")
            let vc = storyboard?.instantiateViewController(withIdentifier: "Signup3ViewController") as! Signup3ViewController
            navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = storyboard?.instantiateViewController(withIdentifier: "RouteViewController") as! RouteViewController
            vc.city = cityTxt
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension Signup2ViewController: PickUpLatLnt {
    func pickUpLatitudeAndLong(_ lon: Double, andLatitude lat: Double, title: String) {
        addressTF.text = title
        self.pickupLat = lat
        self.pickupLng = lon
    }
}
extension Signup2ViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1 {
            return uniqueCity?.count ?? 0
        } else {
            return filterOffice?.count ?? 0
        }
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1 {
            return uniqueCity?[row].city
        } else {
            return filterOffice?[row].name
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1 {
            cityTF.text = uniqueCity?[row].city
            selectedCity = row
        } else {
            officeTF.text = filterOffice?[row].name
            selectedOffice = row
            
        }
    }
}

