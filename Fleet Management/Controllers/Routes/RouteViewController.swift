//
//  RouteViewController.swift
//  Fleet Management
//
//  Created by Atta Khan on 15/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit

class RouteViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var cityRoutes: [Route]?
    var isSelectedRow: Bool = false
    var city: String?
    private let refreshControl = UIRefreshControl()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getCityRoutes()
        
        Utility.shared.setStatusBarBackgroundColor(view: view, color: UIColor.themeColor)
        refreshControl.addTarget(self, action: #selector(refreshTableViewData(_:)), for: .valueChanged)
        if #available(iOS 10, *){
          tableView.refreshControl = refreshControl
        } else {
          tableView.addSubview(refreshControl)
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @objc private func refreshTableViewData(_ sender: Any) {
        getCityRoutes()
    }
    
    func getCityRoutes() {
        let pickupLat = UserDefaults.standard.string(forKey: "pickupLat") ?? "0.0"
        let pickupLng = UserDefaults.standard.string(forKey: "pickupLng") ?? "0.0"
        let url = URL(string: GET_ROUTES_RADIUS + "?City=\(city!)&Lat=\(pickupLat)&Long=\(pickupLng)")!
        showSpinner(onView: self.view)
        WebServiceManager.sharedInstance.getRequest(params: nil, url: url, serviceType: "Get City", modelType: ResultModel.self, success: { (response) in
            let responseObj = response as! ResultModel
            self.removeSpinner()
            if responseObj.result?.totalCount ?? 0 > 0 {
                self.cityRoutes = responseObj.result?.items
                DispatchQueue.main.async {
                    self.setUpTableView()
                    self.refreshControl.endRefreshing()
                }
            } else {
                DispatchQueue.main.async {
                    self.refreshControl.endRefreshing()
                    self.showAlert(title: "Alert", message: "No Data Found.", controller: self) {
                    }
                }
                
            }
        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                self.refreshControl.endRefreshing()
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
    }
    
    func setUpTableView() {
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 88
        tableView.register(RouteViewCell.nib, forCellReuseIdentifier: RouteViewCell.identifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }

    
    @IBAction func navigateBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapOnNextBtn(_ sender: Any) {
        if isSelectedRow {
            let vc = storyboard?.instantiateViewController(withIdentifier: "Signup3ViewController") as! Signup3ViewController
            navigationController?.pushViewController(vc, animated: true)
        } else {
            self.showAlert(title: "Alert", message: "Please select your route.", controller: self) {
            }
        }
    }
    
}
extension RouteViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if cityRoutes?.count ?? 0 == 0 {
            tableView.setEmptyView(message: "No Route Available", image: UIImage(named: "nochat")!)
        } else {
            tableView.restore()
        }
        return cityRoutes?.count ?? 0
    }
    
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RouteViewCell.identifier) as! RouteViewCell
        cell.data = cityRoutes?[indexPath.row]
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isSelectedRow = true
        let routeId = cityRoutes?[indexPath.row].id
        if let dropAdress = cityRoutes?[indexPath.row].endingAddress {
            UserDefaults.standard.set(dropAdress, forKey: "dropAdress")
        }
        UserDefaults.standard.set(routeId!, forKey: "routeId")
        
    }
    
}
