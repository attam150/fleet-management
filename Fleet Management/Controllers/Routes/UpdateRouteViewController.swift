//
//  UpdateRouteViewController.swift
//  Fleet Management
//
//  Created by Atta Khan on 20/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit

class UpdateRouteViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var cityRoutes: [Route]?
    var isSelectedRow: Bool = false
    var profile_data: ProfileData?
    var selectedRouteId: Int?
    var delegate: UpdatedProfile?
    var isComingfrom: String?
    var city: String?
    private let refreshControl = UIRefreshControl()
    override func viewDidLoad() {
        super.viewDidLoad()
        getCityRoutes()
        setUpTableView()
        Utility.shared.setStatusBarBackgroundColor(view: view, color: UIColor.themeColor)
        refreshControl.addTarget(self, action: #selector(refreshTableViewData(_:)), for: .valueChanged)
        if #available(iOS 10, *){
          tableView.refreshControl = refreshControl
        } else {
          tableView.addSubview(refreshControl)
        }
        print(profile_data?.routeId)
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @objc private func refreshTableViewData(_ sender: Any) {
        getCityRoutes()
    }
    override func viewWillAppear(_ animated: Bool) {
        if let isComing = isComingfrom {
            self.showAlert(title: "Alert", message: "We have noticed a change in your pick & drop locaiton. \n Please select your route to continue.", controller: self) {
            }
        }
    }
        
    func getCityRoutes() {
        let city = profile_data?.city ?? "Lahore"
        let pickupLat = profile_data?.pickupLat ?? 0.0
        let pickupLng = profile_data?.pickupLong ?? 0.0
        let url = URL(string: GET_ROUTES_RADIUS + "?City=\(city)&Lat=\(pickupLat)&Long=\(pickupLng)")!
        showSpinner(onView: self.view)
        WebServiceManager.sharedInstance.getRequest(params: nil, url: url, serviceType: "Get City", modelType: ResultModel.self, success: { (response) in
            let responseObj = response as! ResultModel
            self.removeSpinner()
            if responseObj.result?.totalCount ?? 0 > 0 {
                self.cityRoutes = responseObj.result?.items
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.refreshControl.endRefreshing()
                }
            }
        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                self.refreshControl.endRefreshing()
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
    }
    
    func setUpTableView() {
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 88
        tableView.register(RouteViewCell.nib, forCellReuseIdentifier: RouteViewCell.identifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }

        
    @IBAction func navigateBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapOnNextBtn(_ sender: Any) {
        if isSelectedRow {
            let vc = storyboard?.instantiateViewController(withIdentifier: "Signup3ViewController") as! Signup3ViewController
            navigationController?.pushViewController(vc, animated: true)
        } else {
            self.showAlert(title: "Alert", message: "Please select your route.", controller: self) {
            }
        }
    }
    
    
    @IBAction func updateRoute(_ sender: Any) {
        if isSelectedRow {
            let encode = JSONEncoder()
            let jsonData = try! encode.encode(profile_data)
            let url = URL(string: UPDATE_USER_PROFILE)!
            showSpinner(onView: self.view)
            WebServiceManager.sharedInstance.putRequest(params: jsonData , url: url, serviceType: "LOGIN", modelType: UserProfileModel.self, success: { (response) in
                let responseObj = response as! UserProfileModel
                self.removeSpinner()
                if responseObj.success == true {
                    self.profile_data = responseObj.result
                    DispatchQueue.main.async { () -> Void in
                        self.showAlert(title: "Alert", message: "Data updated successfully.", controller: self) {
                            if let routeId = responseObj.result?.routeId {
                               UserDefaults.standard.set(routeId, forKey: "routeId")
                            }
                            
                            if let isComing = self.isComingfrom {
                                self.navigationController?.viewControllers.remove(at: [0,1])
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                                vc.isComing = self.isComingfrom
                                self.navigationController?.pushViewController(vc, animated: true)
                            } else {
                                self.delegate?.get_updated_data(data: self.profile_data!)
                                self.navigationController?.popViewController(animated: true)
                            }
                        }
                    }
                } else {
                    print(responseObj.error?.message)
                }
            }, fail: { (error) in
                self.removeSpinner()
                print(error)
                DispatchQueue.main.async { () -> Void in
                    self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                    }
                }
            }, showHUD: true)
        } else {
            self.showAlert(title: "Alert", message: "Please select your route.", controller: self) {
            }
        }
    }
    
}
extension UpdateRouteViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if cityRoutes?.count ?? 0 == 0 {
            tableView.setEmptyView(message: "No Route Available", image: UIImage(named: "nochat")!)
        } else {
            tableView.restore()
        }
        return cityRoutes?.count ?? 0
    }
    
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RouteViewCell.identifier) as! RouteViewCell
        cell.data = cityRoutes?[indexPath.row]
        if let route_id = profile_data?.routeId, route_id == cityRoutes?[indexPath.row].id {
            cell.setSelected(true, animated: true)
        }
        
        
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isSelectedRow = true
        selectedRouteId = cityRoutes?[indexPath.row].id
        if let dropAdress = cityRoutes?[indexPath.row].endingAddress {
            UserDefaults.standard.set(dropAdress, forKey: "dropAdress")
        }
        profile_data?.routeId = cityRoutes?[indexPath.row].id
        profile_data?.route = cityRoutes?[indexPath.row]
        print(profile_data)
    }
    
}
