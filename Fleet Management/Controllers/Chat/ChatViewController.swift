//
//  ChatViewController.swift
//  Fleet Management
//
//  Created by Atta khan on 20/08/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseFirestoreSwift

class ChatViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageTF: UITextField!
    @IBOutlet weak var chatView: UIView!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var driverImgView: UIImageView!
    @IBOutlet weak var driverNameLbl: UILabel!
    private let refreshControl = UIRefreshControl()
    var messages : [MessageModel]?
    var tripResult : TripExecutionResult?
    var passengerId: String = ""
    var tripId : String = ""
    var msgData: [MessageModel]?
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.shared.setStatusBarBackgroundColor(view: view, color: UIColor.themeColor)
        print(Date().millisecondsSince1970)
        refreshControl.addTarget(self, action: #selector(refreshTableViewData(_:)), for: .valueChanged)
        if #available(iOS 10, *){
          tableView.refreshControl = refreshControl
        } else {
          tableView.addSubview(refreshControl)
        }
        if let name = tripResult?.driverName {
            driverNameLbl.text = name
        } else {
            driverNameLbl.text = ""
        }
        setUpTableView()
        driverImgView.makeRounded()
        if let driverProfile = tripResult?.driver?.profilePicture {
            let userImg = BASE_URL_MEDIA + driverProfile
            driverImgView.setImage(with: userImg, placeholder: UIImage(named: "placeholder"))
        }
        
        let user_id = UserDefaults.standard.integer(forKey: "userId")
        
        let passenger_detail = tripResult?.tripExecutionDrivers?.filter( { $0.passengerId == user_id } )
        passengerId = passenger_detail?[0].id ?? ""
        tripId      = tripResult?.id ?? ""
        fetchData()
    }
    func setUpTableView() {
        tableView.separatorStyle = .none
        tableView.register(MessageViewCell.nib, forCellReuseIdentifier: MessageViewCell.identifier)
        tableView.register(VoiceViewCell.nib, forCellReuseIdentifier: VoiceViewCell.identifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    func fetchData() {
        var list: [MessageModel]?
        showSpinner(onView: self.view)
        
        
        
        db.collection("trip").document(tripId).collection("passengers").document(passengerId)
            .collection("chat").getDocuments() { (querySnapshot, err) in
                self.removeSpinner()
                if let error = err {
                  print(error)
                } else if let snapshot = querySnapshot {
                    //self.msgData?.removeAll()
                    self.msgData = snapshot.documents.compactMap {
                    return try? $0.data(as: MessageModel.self)
                  }
                }
                self.tableView.reloadData()
                if self.msgData?.count ?? 0 > 0 {
                    DispatchQueue.main.async {
                        let rowToGoTo = (self.msgData?.count ?? 0) - 1
                        let indexPath = NSIndexPath(row: rowToGoTo, section: 0)
                        self.tableView.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
                    }
                }
        }
    }
    
    
    @objc private func refreshTableViewData(_ sender: Any) {
        print("refresh")
        fetchData()
    }
    
    
    @IBAction func tapOnSubmitBtn(_ sender: Any) {
        let date: Int64   = Int64(Date().millisecondsSince1970) * 1000
        let messageTxt  = messageTF.text ?? ""
        let userType    = UserRole.PASSENGER.rawValue
        
        let docData: [String: Any] = [
            "date": date,
            "fileName": NSNull(),
            "message": messageTxt,
            "passengerID": passengerId,
            "platform": "iOS",
            "seen": false,
            "tripID": tripId,
            "uploadedSuccessfully": NSNull(),
            "userType": userType,
            "voice": NSNull(),
            "voiceDuration": NSNull()
            //"isPLayingAudio": false
        ]

        
        db.collection("trip").document(tripId).collection("passengers").document(passengerId).collection("chat").document()
            .setData(docData) { err in
            if let err = err {
                print("Error writing document: \(err)")
            } else {
                print("Document successfully written!")
                self.showAlert(title: "Alert", message: "Message sent successfully!", controller: self) {
                    self.messageTF.text = ""
                    // push notification api call
                    
                    // update document for dirver count
                    
                    let ref = db.collection("trip").document(self.tripId).collection("passengers").document(self.passengerId)
                    ref.updateData([
                        "message": messageTxt,
                        "voice": false,
                        "time": date,
                        "driverCount": FieldValue.increment(Int64(1))
                    ]) { err in
                        if let err = err {
                            print("Error updating document: \(err)")
                        } else {
                            print("Document successfully updated")
                        }
                    }
                    self.sendPushNotification(messageTxt)
                    self.fetchData()
                }
            }
        }
        
    }
    private func sendPushNotification(_ msgTxt: String) {
        let driverId = tripResult?.driver?.id ?? 0
        let username = UserDefaults.standard.string(forKey: "fullName") ?? ""
        let parms = 20
        let encode = JSONEncoder()
        let jsonData = try! encode.encode(parms)
        let urlString = SEND_NOTIFICATION + "?userId=\(driverId)&title=\(username)&body=\(msgTxt)"
        
        if let encodedString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
           let aURL = URL(string: encodedString) {
            print(aURL)
//            let url = URL(string: urlString)!
            WebServiceManager.sharedInstance.postRequest(params: jsonData , url: aURL, serviceType: "Send Message", modelType: UserModel.self, success: { (response) in
                let responseObj = response as! UserModel
                print(responseObj)
                
            }, fail: { (error) in
                self.removeSpinner()
                print(error)
            }, showHUD: true)
        }
    }
    
    @IBAction func backNavigateBtn(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
    
    
    private func estimatedFrameForTxt(text: String) -> CGRect {
        let size = CGSize(width: 250, height: 1000)
        let option = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size, options: option, attributes: [ NSAttributedString.Key.font: UIFont(name: "ProximaNova-Regular", size: 15)], context: nil)
    }
   
}
extension ChatViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return msgData?.count ?? 0
    }
   
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let message = msgData?[indexPath.row].message {
            let cell = tableView.dequeueReusableCell(withIdentifier: MessageViewCell.identifier) as! MessageViewCell
            cell.adminLbl.isHidden = true
            cell.adminMsgIcon.isHidden = true
            cell.userLbl.isHidden = true
            cell.userIcon.isHidden = true
            
            if let messageTxt = msgData?[indexPath.row].message {
                cell.messageTextView.text = messageTxt
                cell.bubbleViewWidth?.constant = estimatedFrameForTxt(text: messageTxt).width + 32
            }
            if let userType = msgData?[indexPath.row].userType {
                if userType == .DRIVER {
                    // admin message display here...
                    cell.bubbleView.backgroundColor = UIColor.white
                    cell.messageTextView.textColor = UIColor.black
                    cell.bubbleViewLeading?.isActive = true
                    cell.bubbleViewTrailing?.isActive = false
                    cell.bubbleView.roundCorners([.layerMaxXMaxYCorner,.layerMaxXMinYCorner, .layerMinXMaxYCorner], radius: 15, borderColor: .lightGray, borderWidth: 0.5)
                    cell.timeLbl.isHidden = false
                    
                    if let createTime = msgData?[indexPath.row].date {
                        print("database date = \(createTime)")
                        let dateTime = createTime * 1000
                        var date = Date(timeIntervalSince1970: (TimeInterval(dateTime)))
    //                    timeSinceDate
                        print("date - \(date.timeSinceDate(fromDate: Date()))")
                        cell.timeLbl.text = date.timeSinceDate(fromDate: Date())
                    }
                } else {
                    cell.bubbleView.backgroundColor = UIColor.themeColor
                    cell.messageTextView.textColor = UIColor.white
                    cell.bubbleViewLeading?.isActive = false
                    cell.bubbleViewTrailing?.isActive = true
                    cell.timeLbl.isHidden = false
                    cell.bubbleView.roundCorners([.layerMinXMinYCorner, .layerMaxXMaxYCorner, .layerMinXMaxYCorner], radius: 15, borderColor: .lightGray, borderWidth: 0.5)
                    if let createTime = msgData?[indexPath.row].date {
                        let dateTime = createTime * 1000
                        var date = Date(timeIntervalSince1970: (TimeInterval(dateTime / 1000)))
                        cell.timeLbl.text = date.timeSinceDate(fromDate: Date())
                    }
                }
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: VoiceViewCell.identifier) as! VoiceViewCell
            return cell
        }
        
        
        
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let messageTxt = msgData?[indexPath.row].message {
            let estimatedFrame = estimatedFrameForTxt(text: messageTxt).height
            return estimatedFrame + 80
        }
        return 88
    }
    
    
}

public struct MessageModel: Codable {
    
    let date: Int
    let fileName: String?
    let message: String?
    let passengerID: String
    let platform: String = "iOS"
    let seen: Bool = false
    let tripID: String
    let uploadedSuccessfully: Bool = false
    let userType: UserRole
    let voice: String?
    let voiceDuration: String?
    let isPLayingAudio: Bool = false
    
    enum CodingKeys: String, CodingKey {
        case date
        case fileName
        case message
        case passengerID
        case platform
        case seen
        case tripID
        case uploadedSuccessfully
        case userType
        case voice
        case voiceDuration
        case isPLayingAudio
    }
    
}

extension Date {
    var millisecondsSince1970:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }

    init(milliseconds:Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
}

struct PushNotification: Codable {
    var userId: Int?
    var title: String?
    var body: String?
}
