//
//  RequestViewController.swift
//  Fleet Management
//
//  Created by Atta Khan on 20/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit

class RequestViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    let cellSpacingHeight: CGFloat = 10
    var requestitems: [RequestItems]?
    private let refreshControl = UIRefreshControl()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Utility.shared.setStatusBarBackgroundColor(view: view, color: UIColor.themeColor)
        refreshControl.addTarget(self, action: #selector(refreshTableViewData(_:)), for: .valueChanged)
        if #available(iOS 10, *){
          tableView.refreshControl = refreshControl
        } else {
          tableView.addSubview(refreshControl)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationRecieved), name: .notificationReceived, object: nil)

    }
    @objc func notificationRecieved(notification: NSNotification){
        print("Fcm received....")
        if let fcmStatus = notification.userInfo?["fcmStatus"] as? String {
        let value = Int(fcmStatus) ?? 0
        if let fcmStatus = FCMSTATUS(rawValue: value) {
            print(fcmStatus)
            switch fcmStatus {
            case .RequestCreated, .RequestApproved, .RequestRejected:
                getRequest()
            default:
                break
            }
        }
        }
        
        
    }
    @objc private func refreshTableViewData(_ sender: Any) {
        getRequest()
       
    }
    override func viewWillAppear(_ animated: Bool) {
        getRequest()
    }
    func setUpTableView() {
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.register(RequestTableViewCell.nib, forCellReuseIdentifier: RequestTableViewCell.identifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    @IBAction func backNavigateBtn(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
    func getRequest() {
        let user_id = UserDefaults.standard.integer(forKey: "userId")
        let url = URL(string: GET_ALL_REQUEST + "?userId=\(user_id)")!
        showSpinner(onView: self.view)
        WebServiceManager.sharedInstance.getRequest(params: nil, url: url, serviceType: "Get request data", modelType: RequestsModel.self, success: { (response) in
            let responseObj = response as! RequestsModel
            self.removeSpinner()
            if responseObj.success == true {
                self.requestitems = responseObj.result?.items
                DispatchQueue.main.async {
                    self.setUpTableView()
                    self.refreshControl.endRefreshing()
                }
            }
        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                self.refreshControl.endRefreshing()
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
    }
    
    func deleteRequstByUser(_ id: String) {
        let url = URL(string: DELETE_REQUEST_BY_USER + "?id=\(id)")!
        showSpinner(onView: self.view)
        WebServiceManager.sharedInstance.deleteRequest(params: nil , url: url, serviceType: "Delete Request", modelType: UserModel.self, success: { (response) in
           let responseObj = response as! UserModel
           self.removeSpinner()
            DispatchQueue.main.async { () -> Void in
                if responseObj.success == true {
                    self.showAlert(title: "Alert", message: "Your request has been deleted successfully.", controller: self) {
                        self.getRequest()
                    }
                } else {
                    self.showAlert(title: "Alert", message: "Please try again, some error occured during your request.", controller: self) {
                    }
                }
                
            }
        }, fail: { (error) in
           self.removeSpinner()
           print(error)
           DispatchQueue.main.async { () -> Void in
               self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
               }
           }
        }, showHUD: true)
    }
}

extension RequestViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if requestitems?.count ?? 0 == 0 {
            tableView.setEmptyView(message: "No Pending Request Available.", image: UIImage(named: "nochat")!)
        } else {
            tableView.restore()
        }
        return requestitems?.count ?? 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }

    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RequestTableViewCell.identifier) as! RequestTableViewCell
        if let id = requestitems?[indexPath.section].requestNumber {
            cell.requestIdLbl.text = "#\(id)"
        }
        
        
        if let status = requestitems?[indexPath.section].status {
            switch status {
            case .PENDING:
                cell.statusBtn.setTitle("Pending", for: .normal)
                cell.statusBtn.backgroundColor = UIColor.rgb(red: 238, green: 238, blue: 238, alpha: 1)
                cell.statusBtn.borderWidth = 0.5
                cell.statusBtn.borderColor = UIColor.rgb(red: 230, green: 230, blue: 230, alpha: 1)
                cell.remvoeBtn.isHidden = false
            case .COMPLETED:
                cell.statusBtn.setTitle("Completed", for: .normal)
                cell.statusBtn.backgroundColor = UIColor.rgb(red: 232, green: 255, blue: 241, alpha: 1)
                cell.statusBtn.borderWidth = 0.5
                cell.statusBtn.borderColor = UIColor.rgb(red: 119, green: 201, blue: 168, alpha: 1)
                cell.remvoeBtn.isHidden = true
            case .REJECTED:
                cell.statusBtn.setTitle("Rejected", for: .normal)
                cell.statusBtn.backgroundColor = UIColor.rgb(red: 255, green: 0, blue: 0, alpha: 1)
                cell.statusBtn.borderWidth = 0.5
                cell.statusBtn.borderColor = UIColor.rgb(red: 241, green: 241, blue: 241, alpha: 1)
                cell.remvoeBtn.isHidden = true
            case .WITHDRAWNBYUSER:
                cell.statusBtn.setTitle("With Drawn by user", for: .normal)
                cell.remvoeBtn.isHidden = true
            }
        }
        cell.remvoeBtn.tag = indexPath.section
        cell.remvoeBtn.addTarget(self, action: #selector(deleteRequst), for: .touchUpInside)
        cell.tapBtn.tag = indexPath.section
        cell.tapBtn.addTarget(self, action: #selector(handleRequst), for: .touchUpInside)
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88
    }
    
    
    
    @objc func handleRequst(_ sender: UIButton) {
        let tag = sender.tag
        let storyboard = UIStoryboard(name: "Schedule", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "RequestDetailsViewController") as! RequestDetailsViewController
        vc.request_id = requestitems?[tag].id
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func deleteRequst(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Alert", message: "Are you sure you want to delete this request?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
              switch action.style{
              case .default:
                    print("default")
                    let tag = sender.tag
                    if let request_id = self.requestitems?[tag].id {
                        self.deleteRequstByUser(request_id)
                    }
              case .cancel:
                    print("cancel")

              case .destructive:
                    print("destructive")


        }}))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)

    }
}
