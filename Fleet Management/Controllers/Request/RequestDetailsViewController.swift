//
//  RequestDetailsViewController.swift
//  Fleet Management
//
//  Created by Atta khan on 10/06/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit

class RequestDetailsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageTF: UITextField!
    @IBOutlet weak var requestStatusBtn: UIButton!
    
    @IBOutlet weak var chatViewHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var chatView: UIView!
    @IBOutlet weak var requestCategoryLbl: UILabel!
    @IBOutlet weak var requstIdLbl: UILabel!
    @IBOutlet weak var submitBtn: UIButton!
    let cellSpacingHeight: CGFloat = 10
    var message: [Messages]?
    var request_details: RequestDetailResult?
    var request_id: String?
    var messageIds: [Int : String] = [:]
    var messageDic: [String: String] = [:]
    var messageUserType: [String: String] = [:]
     private let refreshControl = UIRefreshControl()
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.shared.setStatusBarBackgroundColor(view: view, color: UIColor.themeColor)
        setUpTableView()
        if let id = request_id {
            getRequestDetails(id)
        }
        refreshControl.addTarget(self, action: #selector(refreshTableViewData(_:)), for: .valueChanged)
        if #available(iOS 10, *){
          tableView.refreshControl = refreshControl
        } else {
          tableView.addSubview(refreshControl)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationRecieved), name: .notificationReceived, object: nil)
    }
    
    @objc func notificationRecieved(notification: NSNotification){
        print("Fcm received....")
        if let fcmStatus = notification.userInfo?["fcmStatus"] as? String {
        let value = Int(fcmStatus) ?? 0
        if let fcmStatus = FCMSTATUS(rawValue: value) {
            print(fcmStatus)
            switch fcmStatus {
            case .AdminReplied:
                if let id = request_id {
                    getRequestDetails(id)
                }
            default:
                break
            }
        }
        }
        
        
    }
    @objc private func refreshTableViewData(_ sender: Any) {
        if let id = request_id {
            getRequestDetails(id)
        }
       
    }
    func setUpTableView() {
        tableView.separatorStyle = .none
        tableView.register(MessageViewCell.nib, forCellReuseIdentifier: MessageViewCell.identifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    func getRequestDetails(_ id: String) {
        let url = URL(string: GET_REQUEST_DETAILS + "?id=\(id)")!
        showSpinner(onView: self.view)
        WebServiceManager.sharedInstance.getRequest(params: nil, url: url, serviceType: "Get request data", modelType: RequestDetailsModel.self, success: { (response) in
            let responseObj = response as! RequestDetailsModel
            self.removeSpinner()
                if responseObj.success == true {
                    self.request_details = responseObj.result
                    self.message = self.request_details?.messages
                    if self.message?.count ?? 0 > 0 {
                        for (index, value) in (self.message?.enumerated())! {
                            if let id = self.message?[index].id, let messageTxt = self.message?[index].message, let userType = self.message?[index].userType {
                                self.messageIds[index] = id
                                self.messageDic[id] = messageTxt
                                self.messageUserType[id] = userType
                            }
                        }
                    }
                    DispatchQueue.main.async { () -> Void in
                        self.updateRequestStatus(with: responseObj.result)
                        self.setUpTableView()
                        self.refreshControl.endRefreshing()
                    }
                    
                }
            
        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                self.refreshControl.endRefreshing()
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
    }
    func updateRequestStatus(with responseObj: RequestDetailResult?) {
        if let id = responseObj?.requestNumber {
            requstIdLbl.text = "#\(id)"
        }
        if let status = responseObj?.status {
            switch status {
            case .PENDING:
                requestStatusBtn.setTitle("Pending", for: .normal)
                requestStatusBtn.backgroundColor = UIColor.rgb(red: 238, green: 238, blue: 238, alpha: 1)
                requestStatusBtn.borderWidth = 0.5
                requestStatusBtn.borderColor = UIColor.rgb(red: 230, green: 230, blue: 230, alpha: 1)
                disableChatView(false)
            case .COMPLETED:
                requestStatusBtn.setTitle("Completed", for: .normal)
                requestStatusBtn.backgroundColor = UIColor.rgb(red: 232, green: 255, blue: 241, alpha: 1)
                requestStatusBtn.borderWidth = 0.5
                requestStatusBtn.borderColor = UIColor.rgb(red: 119, green: 201, blue: 168, alpha: 1)
                disableChatView(true)
            case .WITHDRAWNBYUSER:
                requestStatusBtn.setTitle("With drawn by user", for: .normal)
                disableChatView(true)
            case .REJECTED:
                requestStatusBtn.setTitle("Rejected", for: .normal)
                requestStatusBtn.backgroundColor = UIColor.rgb(red: 255, green: 0, blue: 0, alpha: 1)
                requestStatusBtn.borderWidth = 0.5
                requestStatusBtn.borderColor = UIColor.rgb(red: 241, green: 241, blue: 241, alpha: 1)
                disableChatView(true)
            }
        }
        
        if let categoryStatus = responseObj?.subCategory {
            switch categoryStatus {
            case .PassengerPickupLocationChange:
                requestCategoryLbl.text = "Location Change Request (Pickup & Dropoff)"
            case .PassengerRouteChange:
                requestCategoryLbl.text = "Route Change Request"
            case .PassengerRouteAndAddressChange:
                requestCategoryLbl.text = "Location & Route Change Request"
            case .ScheduleUpdate:
                break
            case .ScheduleServiceTypeUpdate:
                break
            }
        }
    }
    func disableChatView(_ flag: Bool) {
        chatView.isHidden = flag
        if flag == true {
            chatViewHeightConstrain.constant = 0
        } else {
            chatViewHeightConstrain.constant = 55
        }
        
    }
    @IBAction func tapOnSubmitBtn(_ sender: Any) {
        guard let messageTxt  = messageTF.text, !messageTxt.isEmpty else {
            self.showAlert(title: "Alert", message: "Please enter your message.", controller: self) {
                self.messageTF.becomeFirstResponder()
            }
            return
        }
        
        var msg =  MessageRequst()
        msg.message = messageTxt
        msg.requestId = request_id!
        
        
        
        let encode = JSONEncoder()
        let jsonData = try! encode.encode(msg)
        print(jsonData)
        
        
        let url = URL(string: SEND_MESSAGE)!
        showSpinner(onView: self.view)
        WebServiceManager.sharedInstance.postRequest(params: jsonData , url: url, serviceType: "Send Message", modelType: UserModel.self, success: { (response) in
            let responseObj = response as! UserModel
            self.removeSpinner()
            DispatchQueue.main.async { () -> Void in
                
                if responseObj.success == true {
                    self.getRequestDetails(self.request_id!)
//                    let indexPath = NSIndexPath(item: self.message?.count ?? 0, section: 0)
//                    self.tableView.scrollToRow(at: indexPath as IndexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                    self.messageTF.text = ""
                }
            }
        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
    }
    
    @IBAction func backNavigateBtn(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
    
    private func estimatedFrameForTxt(text: String) -> CGRect {
        let size = CGSize(width: 250, height: 1000)
        let option = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size, options: option, attributes: [ NSAttributedString.Key.font: UIFont(name: "ProximaNova-Regular", size: 15)], context: nil)
    }
    
}

extension RequestDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if messageDic.count == 0 {
            tableView.setEmptyView(message: "No Chat Available", image: UIImage(named: "nochat")!)
        } else {
            tableView.restore()
        }
        return messageDic.count
    }
   
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MessageViewCell.identifier) as! MessageViewCell
        
        if let key = messageIds[indexPath.row], let messageTxt = messageDic[key] {
            cell.messageTextView.text = messageTxt
            cell.bubbleViewWidth?.constant = estimatedFrameForTxt(text: messageTxt).width + 32
        }
        
        if let key = messageIds[indexPath.row], let userType = messageUserType[key] {
            if userType == "Admin" {
                // admin message display here...
                cell.bubbleView.backgroundColor = UIColor.white
                cell.messageTextView.textColor = UIColor.black
                cell.userLbl.isHidden = true
                cell.userIcon.isHidden = true
                cell.adminLbl.isHidden = false
                cell.adminMsgIcon.isHidden = false
                cell.bubbleViewLeading?.isActive = true
                cell.bubbleViewTrailing?.isActive = false
                cell.bubbleView.roundCorners([.layerMaxXMaxYCorner,.layerMaxXMinYCorner, .layerMinXMaxYCorner], radius: 15, borderColor: .lightGray, borderWidth: 0.5)
                cell.timeLbl.isHidden = false
                if let createTime = message?[indexPath.row].creationTime?.toDateString()?.toString(format: "hh:mm a") {
                    cell.timeLbl.text = createTime
                }
            } else {
                cell.bubbleView.backgroundColor = UIColor.themeColor
                cell.messageTextView.textColor = UIColor.white
                cell.userLbl.isHidden = false
                cell.userIcon.isHidden = false
                cell.adminLbl.isHidden = true
                cell.adminMsgIcon.isHidden = true
                cell.bubbleViewLeading?.isActive = false
                cell.bubbleViewTrailing?.isActive = true
                cell.timeLbl.isHidden = false
                if let createTime = message?[indexPath.row].creationTime?.toDateString()?.toString(format: "hh:mm a") {
                    cell.timeLbl.text = createTime
                }
                cell.bubbleView.roundCorners([.layerMinXMinYCorner, .layerMaxXMaxYCorner, .layerMinXMaxYCorner], radius: 15, borderColor: .lightGray, borderWidth: 0.5)
            }
        }
        
     
        
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let messageTxt = message?[indexPath.row].message {
            let estimatedFrame = estimatedFrameForTxt(text: messageTxt).height
            return estimatedFrame + 80
        }
        return 88
    }
    
}
struct MessageRequst: Codable {
    var message: String?
    var requestId: String?
}

