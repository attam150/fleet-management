//
//  HomeController.swift
//  Fleet Management
//
//  Created by Atta Khan on 02/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit
import GoogleMaps
import SwiftyJSON
import Alamofire

class HomeController: UIViewController {
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var mapView: GMSMapView!
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    @IBOutlet weak var collectionView: UICollectionView!
    var scheduleItems: [ScheduleItems]?
    @IBOutlet weak var rideStartedView: UIView!
    @IBOutlet weak var noTripView: UIView!
    @IBOutlet weak var bottomLayerView: UIView!
    @IBOutlet weak var checkInStatusView: UIView!
    @IBOutlet weak var rideInprogressView: UIView!
    @IBOutlet weak var driverNameLbl: UILabel!
    @IBOutlet weak var driverVehiclelbl: UILabel!
    @IBOutlet weak var tripStartedTime: UILabel!
    @IBOutlet weak var tripStartedDateLbl: UILabel!
    @IBOutlet weak var driverImgView: UIImageView!
    @IBOutlet weak var driverReportBtn: UIButton!
    @IBOutlet weak var welcomeLbl: UILabel!
    @IBOutlet weak var ETALbl: UILabel!
    @IBOutlet weak var pickTypeLbl: UILabel!
    @IBOutlet weak var rideCompletedView: UIView!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userVehicleLbl: UILabel!
    
    @IBOutlet weak var rideCompletedStatus: UILabel!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var popupView: UIView!
    
    @IBOutlet weak var rideStatusMsgLbl: UILabel!
    @IBOutlet weak var upComingRidelbl: UILabel!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var rideStatusLbl: UILabel!
    var passengarLocation = CLLocation()
    var trip_execution_id: String = ""
    var trip_status_type: TripExecutionDriverStatus = .PENDING
    var trip_type : TripType = .PICKUP
    let carMarker = GMSMarker()
    var estimatedTime: String = "0"
    var estimatedDistance: String = "0"
    var passenger_pickup_time = ""
    var passenger_dropOff_time = ""
    var passenger_trip_execution_id = ""
    var trip_status_completed = false
    var tripExecutionResult : TripExecutionResult?
    var notificationReceived: Bool = false
    deinit {
        // If your app supports iOS 8 or earlier, you need to manually
        // remove the observer from the center. In later versions
        // this is done automatically.
        NotificationCenter.default.removeObserver(self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.shared.setStatusBarBackgroundColor(view: view, color: UIColor.themeColor)
        driverImgView.makeRounded()
        userImg.makeRounded()
        collectionView.backgroundColor = UIColor.clear
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 200, right: 0)
        mapView.mapStyle(withFilename: "mapStyle", andType: "json")
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationRecieved), name: .notificationReceived, object: nil)
        getUserData()
    }
   
    override func viewWillAppear(_ animated: Bool) {
        updateUI()
        self.removeSpinner()
        CheckOngoingTrip()
    }
    
    @objc func notificationRecieved(notification: NSNotification){
        if let fcmStatus = notification.userInfo?["fcmStatus"] as? String {
            let value = Int(fcmStatus) ?? 0
            if let fcmStatus = FCMSTATUS(rawValue: value) {
                tripStatusThroughFCM(on: fcmStatus)
            }
        }
        
        
    }
    
    func tripStatusThroughFCM(on status: FCMSTATUS) {
        updateUI()
        switch status {
        case .CHECKIN, .DESTINATIONARRIVED, .MISSED, .TRIPSTARTED, .CANCELCONFIRMATION, .COMPLETION_CONFRIMATION, .DriverTripCanceled, .PassengerScheduleApproved, .PassengerTripCancelledByDriver:
            if notificationReceived == false {
                print("fcm status is = ", status)
                notificationReceived = true
                CheckOngoingTrip()
            }
        case .RequestCreated:
            UserDefaults.standard.set(false, forKey: "isProfileApproved")
        case .RequestApproved:
            UserDefaults.standard.set(true, forKey: "isProfileApproved")
        default:
            break
        }
 
    }
    func startTimer() {
        Utility.shared.stopTimer()
        guard timer == nil else { return }
        timer = Timer.scheduledTimer(timeInterval: 07.0, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
    }
    
    
    
    @objc func updateTimer() {
        print("timer start.....")
       if trip_execution_id != "" {
           updateDriverLocation(trip_execution_id)
       }
    }
    
    
    func updateUI() {
        self.collectionView.isHidden = true
        self.upComingRidelbl.isHidden = true
        self.bottomLayerView.isHidden = true
        self.rideStartedView.isHidden = true
        self.noTripView.isHidden = true
        self.rideCompletedView.isHidden = true
        self.rideInprogressView.isHidden = true
        self.checkInStatusView.isHidden = true
    }
    
    func setUpCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
    }
    
    @IBAction func tapOnRideCompleteBtn(_ sender: Any) {
        updateTripStatus(.PASSENGER_COMPLETED_CONFIRMATION)
    }
    @IBAction func tapOnRideCancelBtn(_ sender: Any) {
        updateTripStatus(.PASSENGER_CANCEL_CONFIRMATION)
    }
    
    @IBAction func tapOnPhoneCallBtn(_ sender: Any) {
        if let driver_phoneNumber = tripExecutionResult?.driver?.phoneNumber {
            Utility.shared.phoneCall(driver_phoneNumber)
        }
    }
    @IBAction func tapOnMessageBtn(_ sender: Any) {
//        if let driver_phoneNumber = tripExecutionResult?.driver?.phoneNumber {
//            Utility.shared.sendSMS(driver_phoneNumber)
//        }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        vc.tripResult = tripExecutionResult
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func tapOnMenuBtn(_ sender: Any) {
        self.revealController.show(self.revealController.leftViewController)
    }
    
    @IBAction func okBtn(_ sender: Any) {
        Utility.shared.stopTimer()
        updateUI()
        self.trip_status_completed = false
        self.blurView.isHidden = true
        self.popupView.isHidden = true
        self.upComingRidelbl.isHidden = true
        self.mapView.clear()
        self.CheckOngoingTrip()
    }
    
    private func getUserData() {
        let user_id = UserDefaults.standard.integer(forKey: "userId")
        let url = URL(string: GET_PROFILE_DATA + "?id=\(user_id)")!
        WebServiceManager.sharedInstance.getRequest(params: nil, url: url, serviceType: "Get City", modelType: UserProfileModel.self, success: { (response) in
            let responseObj = response as! UserProfileModel
            if responseObj.success == true {
                if let user_id = responseObj.result?.id {
                    UserDefaults.standard.set(user_id, forKey: "userId")
                }
                if let employee_id = responseObj.result?.employeeId {
                    UserDefaults.standard.set(employee_id, forKey: "employeeId")
                }
                if let user_name = responseObj.result?.userName {
                    UserDefaults.standard.set(user_name, forKey: "username")
                }
                if let user_name = responseObj.result?.fullName {
                    UserDefaults.standard.set(user_name, forKey: "fullName")
                }
                
                if let profilePicture = responseObj.result?.profilePicture {
                    let userImg = BASE_URL_MEDIA + profilePicture
                   UserDefaults.standard.set(userImg, forKey: "profilePic")
                }
                if let routeId = responseObj.result?.routeId {
                   UserDefaults.standard.set(routeId, forKey: "routeId")
                }
                if let pickupLat = responseObj.result?.pickupLat, let pickupLng = responseObj.result?.pickupLong {
                    UserDefaults.standard.set(pickupLat, forKey: "pickupLat")
                    UserDefaults.standard.set(pickupLng , forKey: "pickupLng")
                }
                
                if let dropOfflat = responseObj.result?.dropLat, let dropOfflng = responseObj.result?.dropLong {
                    UserDefaults.standard.set(dropOfflat, forKey: "dropOfflat")
                    UserDefaults.standard.set(dropOfflng , forKey: "dropOfflng")
                }
                if let isProfileApproved = responseObj.result?.isProfileApproved {
                   UserDefaults.standard.set(isProfileApproved, forKey: "isProfileApproved")
                }
                if var serviceType = responseObj.result?.serviceType {
                    var value = 3
                    if serviceType.rawValue == 0 || serviceType.rawValue == 3 {
                        value = 3
                    } else if serviceType.rawValue == 1  {
                        value = 1
                    } else if serviceType.rawValue == 2  {
                        value = 2
                    }
                    UserDefaults.standard.set(value, forKey: "serviceType")
                }
            } else {
                print(responseObj.error?.message)
            }
            
            
        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
    }
    
    func getUpcomingSchedules() {
        updateUI()
        let user_id = UserDefaults.standard.integer(forKey: "userId")
        let url = URL(string: GET_UPCOMING_SCHEDULES + "?UserId=\(user_id)&SkipCount=0&MaxResultCount=10")!
        WebServiceManager.sharedInstance.getRequest(params: nil, url: url, serviceType: "Get Upcoming schedule", modelType: UcomingScheduleModel.self, success: { (response) in
            let responseObj = response as! UcomingScheduleModel
            if responseObj.success == true {
                self.notificationReceived = false
                let count = responseObj.result?.totalCount ?? 0
                self.scheduleItems = responseObj.result?.items
                DispatchQueue.main.async {
                    self.collectionView.isHidden = false
                    self.setUpCollectionView()
                }
            }
        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
    }
    
    func CheckOngoingTrip() {
        trip_execution_id = ""
        trip_type = .PICKUP
        Utility.shared.stopTimer()
        let user_id = UserDefaults.standard.integer(forKey: "userId")
        let url = URL(string: CHECK_ONGOING_TRIP + "?passengerID=\(user_id)")!
        showLoaderonView(onView: self.view)
        WebServiceManager.sharedInstance.getRequest(params: nil, url: url, serviceType: "Get Upcoming schedule", modelType: TripExecutionModel.self, success: { (response) in
            let responseObj = response as! TripExecutionModel
            self.removeSpinner()
            if responseObj.success == true {
                if let status = responseObj.result?.status {
                    DispatchQueue.main.async {
                        self.checkTripExecutionPassengerStatus(on: status, with: responseObj.result!)
                    }
                }
            } else {
                self.noActiveTrip()
            }
        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
    }
    
    func checkTripExecutionPassengerStatus(on status: TripExecutionPassengerStatus, with response: TripResultModel) {
        print("trip status is = ", status)
        switch status {
        case .INPROGRESS:
            if let executionId = response.tripExecutionId {
                self.trip_execution_id = executionId
                self.checkTripExecution(executionId)
                self.updateDriverLocation(executionId)
            }
        @unknown default:
            self.mapView.clear()
            self.getUpcomingSchedules()
        }
    }
    
    func checkTripExecution(_ id: String) {
        let user_id = UserDefaults.standard.integer(forKey: "userId")
        let url = URL(string: TRIP_EXECUTION + "?Id=\(id)")!
        WebServiceManager.sharedInstance.getRequest(params: nil, url: url, serviceType: "Get Trip details", modelType: TripExecution.self, success: { (response) in
            let responseObj = response as! TripExecution
            if responseObj.success == true {
                self.notificationReceived = false
                if let result = responseObj.result {
                    self.tripExecutionResult = responseObj.result
                    // must call drawpath func
                    let passenger_detail = responseObj.result?.tripExecutionDrivers?.filter( { $0.passengerId == user_id } )
                    
                    if let status = passenger_detail?[0].status {
                        DispatchQueue.main.async {
                            self.updateBottomViewUI(with: responseObj)
                            self.driverTripStatus(on: status)
                            if status == .PENDING || status == .CHECKIN || status == .DESTINATION_ARRIVED {
                                self.startTimer()
                                self.passengerPathDraw(responseObj: responseObj)
                            } else {
                                self.mapView.clear()
                            }
                            
                        }
                    }
                    if let passenger_id = passenger_detail?[0].id, let dropOffTime = passenger_detail?[0].dropTime, let pickupTime = passenger_detail?[0].pickupTime {
                        self.passenger_pickup_time = pickupTime
                        self.passenger_dropOff_time = dropOffTime
                        self.passenger_trip_execution_id = passenger_id
                    }
                } else {
                    DispatchQueue.main.async {
                        self.noActiveTrip()
                    }
                }
            }
        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
    }
    
    func updateBottomViewUI(with response: TripExecution) {
        if let tripStartedDate = response.result?.startTime?.toDate()?.toString(format: "EEEE, d MMMM YYYY"), let timeString = response.result?.startTime?.toDate() {
            let dateTime = Date().timeSinceDate(fromDate: timeString)
            self.tripStartedTime.text = dateTime
            self.tripStartedDateLbl.text = tripStartedDate
        } else {
            self.tripStartedTime.text = "-"
            self.tripStartedDateLbl.text = "-"
        }
        
        if let driverName = response.result?.driverName {
            self.driverNameLbl.text = driverName
            self.userName.text = driverName
        } else {
            self.driverNameLbl.text = ""
            self.userName.text = ""
        }
        if let make = response.result?.driver?.vehicle?.make, let model = response.result?.driver?.vehicle?.model, let number = response.result?.driver?.vehicle?.registrationNumber {
            self.driverVehiclelbl.text = make + " " + model + " " + number
            self.userVehicleLbl.text  = make + " " + model + " " + number
        } else {
            self.driverVehiclelbl.text = ""
            self.userVehicleLbl.text  = ""
        }
        
        if let driverImg = response.result?.driver?.profilePicture {
            self.driverImgView.setImage(with: BASE_URL_MEDIA + driverImg)
            self.userImg.setImage(with: BASE_URL_MEDIA + driverImg)
        }
    }
    
    func driverTripStatus(on status: TripExecutionDriverStatus) {
        trip_status_type = status
        print("trip driver status is = ", status)
        switch status {
        case .PENDING:
            self.tripInProgress()
        case .CHECKIN:
            self.passengerCheckIn()
        case .DESTINATION_ARRIVED:
            self.rideCompletedState()
        @unknown default:
            self.getUpcomingSchedules()
        }
    }
    
    func passengerPathDraw (responseObj: TripExecution) {
        let user_id = UserDefaults.standard.integer(forKey: "userId")
         let passenger_detail = responseObj.result?.tripExecutionDrivers?.filter( { $0.passengerId == user_id } )
        if let startlatitue =  passenger_detail?[0].passenger?.pickupLat , let startLong = passenger_detail?[0].passenger?.pickupLong,let endLat =  responseObj.result?.trip?.route?.endingPointLat , let endLong = responseObj.result?.trip?.route?.endingPointLong {
                
                DispatchQueue.main.async {
                    if let typeStatus = responseObj.result?.trip?.type {
                        self.trip_type = typeStatus
                        if typeStatus == .PICKUP {
                            let locationStart =  CLLocation(latitude: startlatitue, longitude: startLong)
                            self.passengarLocation = locationStart
                            let locationEnd =  CLLocation(latitude: endLat, longitude: endLong)
                            self.drawPath(startLocation: locationStart, endLocation: locationEnd)
                            self.createMarker(titleMarker: "", iconMarker: #imageLiteral(resourceName: "pin1") , latitude: locationStart.coordinate.latitude, longitude: locationStart.coordinate.longitude, withAnimation: true )
                            self.createMarker(titleMarker: "", iconMarker: #imageLiteral(resourceName: "markerIcon") , latitude: locationEnd.coordinate.latitude, longitude: locationEnd.coordinate.longitude, withAnimation: false)
                        } else {
                            let locationEnd =  CLLocation(latitude: startlatitue, longitude: startLong)
                            let locationStart =  CLLocation(latitude: endLat, longitude: endLong)
                            self.passengarLocation = locationStart
                            self.drawPath(startLocation: locationStart, endLocation: locationEnd)
                            self.createMarker(titleMarker: "", iconMarker: #imageLiteral(resourceName: "pin1") , latitude: locationStart.coordinate.latitude, longitude: locationStart.coordinate.longitude, withAnimation: true)
                            self.createMarker(titleMarker: "", iconMarker: #imageLiteral(resourceName: "markerIcon") , latitude: locationEnd.coordinate.latitude, longitude: locationEnd.coordinate.longitude, withAnimation: false)
                        }
                    }
                    
                    
                }
        }
    }
    
    
    func updateBottomViewConstraint(_ const: CGFloat) {
        self.bottomViewHeightConstraint.constant = const
        self.mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: const, right: 0)
    }
    
    func noActiveTrip() {
        DispatchQueue.main.async {
            self.bottomLayerView.isHidden = false
            self.noTripView.isHidden = false
            self.upComingRidelbl.isHidden = false
        }
    }
    
    func tripInProgress() {
        DispatchQueue.main.async {
            self.updateBottomViewConstraint(130.0)
            self.bottomLayerView.isHidden = false
            self.rideStartedView.isHidden = false
            self.rideInprogressView.isHidden = false
            self.checkInStatusView.isHidden = true
            self.noTripView.isHidden = true
            self.upComingRidelbl.isHidden = true
            
        }
    }
    
    func passengerCheckIn() {
        DispatchQueue.main.async {
            self.updateBottomViewConstraint(130.0)
            self.bottomLayerView.isHidden = false
            self.rideStartedView.isHidden = false
            self.rideInprogressView.isHidden = true
            self.checkInStatusView.isHidden = false
            self.noTripView.isHidden = true
            self.rideCompletedView.isHidden = true
            self.welcomeLbl.text = "Welcome on board"
            self.ETALbl.isHidden = false
            self.ETALbl.text = "ETA \(self.estimatedTime)"
            self.upComingRidelbl.isHidden = true
            self.checkInStatusView.backgroundColor = UIColor(red: 42.0/255.0, green: 127.0/255.0, blue: 246.0/255.0, alpha: 1.0)
        }
    }
    
    func driverArrived() {
        DispatchQueue.main.async {
            self.updateBottomViewConstraint(130.0)
            self.bottomLayerView.isHidden = false
            self.rideStartedView.isHidden = false
            self.rideInprogressView.isHidden = true
            self.checkInStatusView.isHidden = false
            self.noTripView.isHidden = true
            self.rideCompletedView.isHidden = true
            self.welcomeLbl.text = "Your Driver Arrived"
            self.ETALbl.isHidden = true
            self.upComingRidelbl.isHidden = true
            self.checkInStatusView.backgroundColor = UIColor(red: 32.0/255.0, green: 206.0/255.0, blue: 136.0/255.0, alpha: 1.0)

        }
    }
    
    func rideCompletedState() {
        DispatchQueue.main.async {
            self.updateBottomViewConstraint(200.0)
            self.bottomLayerView.isHidden = false
            self.rideStartedView.isHidden = true
            self.rideInprogressView.isHidden = true
            self.checkInStatusView.isHidden = true
            self.noTripView.isHidden = true
            self.rideCompletedView.isHidden = false
            self.upComingRidelbl.isHidden = true
        }
    }
    
    
    
    func createMarker(titleMarker: String, iconMarker: UIImage, latitude: CLLocationDegrees, longitude: CLLocationDegrees, withAnimation: Bool) {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(latitude, longitude)
        marker.title = titleMarker
        marker.icon = iconMarker
        marker.map = self.mapView
        if withAnimation {
            let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2DMake(latitude, longitude), zoom: 16)
            self.mapView.animate(to: camera)
        }
    }
    func drawPath(startLocation: CLLocation, endLocation: CLLocation)
    {
        let origin = "\(startLocation.coordinate.latitude),\(startLocation.coordinate.longitude)"
        let destination = "\(endLocation.coordinate.latitude),\(endLocation.coordinate.longitude)"
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(API_KEY)"
        AF.request(url).responseJSON { response in
            do {
                let json = try JSON(data: response.data!)
                let routes = json["routes"].arrayValue
                print(response.request as Any)  // original URL request
                print(response.response as Any) // HTTP URL response
                print(response.data as Any)     // server data
                print(response.result as Any)   // result of response serialization
                // print route using Polyline
                for route in routes
                {
                    let legs = route["legs"].arrayValue
                    if legs.count > 0 {
                        let duration = legs[0]["duration"]["text"].stringValue
                        self.estimatedTime = duration ?? "0 mins"
                        let distance = legs[0]["distance"]["text"] as? String
                        self.estimatedDistance = distance ?? "0 km"
                    }
                    let routeOverviewPolyline = route["overview_polyline"].dictionary
                    let points = routeOverviewPolyline?["points"]?.stringValue
                    let path = GMSPath.init(fromEncodedPath: points!)
                    let polyline = GMSPolyline.init(path: path)
                    polyline.strokeWidth = 4
                    polyline.strokeColor = UIColor.themeColor
                    polyline.map = self.mapView
                }
            } catch {
                print(error)
            }
            
        }
    }
    
    
    func updateDriverLocation(_ id: String) {
        let url = URL(string: UPDATE_DRIVER_LOCATION + "?tripExecutionId=\(id)")!
        WebServiceManager.sharedInstance.getRequest(params: nil, url: url, serviceType: "Update Driver Location", modelType: DriverModel.self, success: { (response) in
            let responseObj = response as! DriverModel
            if responseObj.success == true {
                if let lat = responseObj.result?.lat, let long = responseObj.result?.long {
                    DispatchQueue.main.async {
                        let driverLocatoin = CLLocation(latitude: lat, longitude: long)
                        let distance = self.passengarLocation.distance(from: driverLocatoin)
                        if distance < 100 && self.trip_status_type == .PENDING {
                            self.driverArrived()
                            print(self.trip_status_type)
                        } else {
                            self.driverTripStatus(on: self.trip_status_type)
                        }
                        self.carMarker.position = CLLocationCoordinate2DMake(lat, long)
                        if self.carMarker.icon == nil {
                            self.carMarker.icon = #imageLiteral(resourceName: "car")
                        }
                        self.carMarker.map = self.mapView
                        //self.createMarker(titleMarker: "", iconMarker: #imageLiteral(resourceName: "car") , latitude: lat, longitude: long)
                        
                    }
                }
            }
        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
    }
    
    
    func updateTripStatus(_ status: TripExecutionDriverStatus) {
        let encode = JSONEncoder()
        var tripJson = TripExecutionJSON()
        tripJson.status = status
        if trip_type == .PICKUP {
            tripJson.dropTime = ""
            tripJson.pickupTime = passenger_pickup_time
        } else {
            tripJson.dropTime = passenger_dropOff_time
            tripJson.pickupTime = ""
        }
        
        
        tripJson.tripExecutionDriverId = passenger_trip_execution_id
        let jsonData = try! encode.encode(tripJson)
        
        let url = URL(string: UPDATE_PASSENGER_BOARDING_STATUS)!
        showSpinner(onView: self.view)
        WebServiceManager.sharedInstance.putRequest(params: jsonData, url: url, serviceType: "Update Trip Status", modelType: UserModel.self, success: { (response) in
            let responseObj = response as! UserModel
            self.removeSpinner()
            if responseObj.success == true {
                DispatchQueue.main.async {
                    if status == .PASSENGER_CANCEL {
                        self.rideStatusLbl.text = "Ride Cancelled"
                        self.rideStatusMsgLbl.text = "Your ride has been cancelled."
                    } else if status == .PASSENGER_COMPLETED_CONFIRMATION {
                        self.rideStatusLbl.text = "Ride Completed"
                        self.rideStatusMsgLbl.text = "Ride Completed Successfully"
                    }
                    self.trip_status_completed = true
                    self.blurView.isHidden = false
                    self.popupView.isHidden = false
                }
            }
        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
    }
    
    @IBAction func createScheduleBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Schedule", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AddScheduleVC") as! AddScheduleVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
extension HomeController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let count = scheduleItems?.count ?? 0
        if count == 0 {
            DispatchQueue.main.async {
                self.bottomLayerView.isHidden = false
                self.noTripView.isHidden = false
                self.upComingRidelbl.isHidden = false
            }
        }
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! UpcomingTripCell
        self.upComingRidelbl.isHidden = false
        cell.data = scheduleItems?[indexPath.row]
        cell.detailBtn.tag = indexPath.row
        cell.userImgView.makeRounded()
        cell.detailBtn.addTarget(self, action: #selector(tapOnDetailsBtn), for: .touchUpInside)
        return cell
    }
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      //2
        print(collectionView.frame.width/2)
            return CGSize(width: self.collectionView.frame.size.width / 1.5, height: self.collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//       let vc = storyboard?.instantiateViewController(identifier: "UpComingScheduleDetailsVC") as! UpComingScheduleDetailsVC
//       vc.schedule_data = scheduleItems?[indexPath.row]
//       navigationController?.pushViewController(vc, animated: true)
    }
    @objc func tapOnDetailsBtn(sender: UIButton) {
        let tag = sender.tag
        let storyboard = UIStoryboard(name: "Schedule", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "UpComingScheduleDetailsVC") as! UpComingScheduleDetailsVC
        vc.schedule_data = scheduleItems?[tag]
        navigationController?.pushViewController(vc, animated: true)
    }
}
extension HomeController: CLLocationManagerDelegate {
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
      let location: CLLocation = locations.last!
      let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                            longitude: location.coordinate.longitude,
                                            zoom: 16)
      self.mapView.camera = camera
    }

  // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            // If always authorized
            manager.startUpdatingLocation()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            openSettingApp()
            break
        default:
            break
        }
    }
}
struct TripExecutionJSON: Codable {
    var tripExecutionDriverId: String?
    var pickupTime: String?
    var dropTime: String?
    var status: TripExecutionDriverStatus?
}
extension GMSMapView {
    func mapStyle(withFilename name: String, andType type: String) {
        do {
             if let styleURL = Bundle.main.url(forResource: name, withExtension: type) {
                self.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
    }
}
