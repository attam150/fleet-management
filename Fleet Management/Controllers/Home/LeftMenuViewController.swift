//
//  LeftMenuViewController.swift
//  Fleet Management
//
//  Created by Atta Khan on 02/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit
import SDWebImage
class LeftMenuViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var menuTitleArray: [String] = [String]()
    var menuImageArray: [String] = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        menuTitleArray = ["Home", "Schedule", "Manage Commute", "Request Details", "Notifications", "Add Schedule", "Logout"]
        menuImageArray = ["home", "iconFeatherCalendar", "iconFeatherSettings", "iconRequest", "iconNotification", "iconIonicIosAddCircleOutline", "iconlogout"]
        // Do any additional setup after loading the view.
        
        Utility.shared.setStatusBarBackgroundColor(view: view, color: UIColor.themeColor)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateUserProfile), name: .updateUserProfile, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateUserProfileImg), name: .udpateUserImg, object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        checkSettingForMobile()
    }
    @objc func updateUserProfile(notification: NSNotification) {
        if let data = notification.userInfo?["userProfile"] as? ProfileData {
            let indexPath = IndexPath(row: 0, section: 0)
            let cell = tableView.cellForRow(at: indexPath) as! MenuHeaderViewCell
            cell.usernameLbl.text = data.fullName
            cell.tripIdLbl.text = data.employeeId
        }
    }
    @objc func updateUserProfileImg(notification: NSNotification) {
        if let userProfile = notification.userInfo?["userProfileImg"] as? String {
            DispatchQueue.main.async {
                let indexPath = IndexPath(row: 0, section: 0)
                let cell = self.tableView.cellForRow(at: indexPath) as! MenuHeaderViewCell
                cell.userImg.setImage(with: userProfile, placeholder: nil)
            }
        }
    }
    func deleteToken() {
        let device_id = UIDevice.current.identifierForVendor?.uuidString ?? ""
        let user_id = UserDefaults.standard.integer(forKey: "userId")
        let url = URL(string: DELETE_TOKEN + "?userId=\(user_id)&deviceId=\(device_id)")!
        showSpinner(onView: self.view)
        WebServiceManager.sharedInstance.deleteRequest(params: nil , url: url, serviceType: "Delete token", modelType: UserModel.self, success: { (response) in
           let responseObj = response as! UserModel
           self.removeSpinner()
           if responseObj.success == true {
               
           } else {
               print(responseObj.error?.message)
           }
        }, fail: { (error) in
           self.removeSpinner()
           print(error)
           DispatchQueue.main.async { () -> Void in
               self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
               }
           }
        }, showHUD: true)
        }
    func checkSettingForMobile() {
        let url = URL(string: GET_SETTING_MOBILE)!
        WebServiceManager.sharedInstance.getRequest(params: nil, url: url, serviceType: "Get setting", modelType: SettingModel.self, success: { (response) in
            let responseObj = response as! SettingModel
            if responseObj.result?.count ?? 0 > 0 {
                UserDefaults.standard.set(responseObj.result?[0].value, forKey: "routeSelectionStatus")
                UserDefaults.standard.set(responseObj.result?[1].value, forKey: "AppPassengerTripSelection")
                
                
            }
        }, fail: { (error) in
            
        }, showHUD: true)
    }
}


extension LeftMenuViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuTitleArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuHeaderViewCell", for: indexPath) as! MenuHeaderViewCell
            if let uername = UserDefaults.standard.string(forKey: "fullName") {
                cell.usernameLbl.text = uername
            }
            if let employee_id = UserDefaults.standard.string(forKey: "employeeId") {
                cell.tripIdLbl.text = employee_id
            }
            cell.userImg.makeRounded()
            if let userProfile = UserDefaults.standard.string(forKey: "profilePic") {
                cell.userImg.setImage(with: userProfile, placeholder: UIImage(named: "placeholder"))
                //cell.userImg.sd_setImage(with: URL(string: userProfile), placeholderImage: UIImage(named: "placeholder"))

            }
            cell.editprofileBtn.addTarget(self, action: #selector(tapOnProileBtn), for: .touchUpInside)
            cell.selectionStyle = .none
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuViewCell", for: indexPath) as! MenuViewCell
            cell.selectionStyle = .none
            let img = menuImageArray[indexPath.row - 1]
            cell.menuImg.image = UIImage(named: img)
            cell.menuLbl.text = menuTitleArray[indexPath.row - 1]
            return cell
        }
    }
    @objc func tapOnProileBtn(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 176.0
        } else {
            return 70.0
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            
        } else if indexPath.row == 1 {
           
            
        }else if indexPath.row == 2 {
            let storyboard = UIStoryboard(name: "Schedule", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ScheduleViewController") as! ScheduleViewController
            navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.row == 3 {
            let storyboard = UIStoryboard(name: "Schedule", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ManageCommuteVC") as! ManageCommuteVC
            navigationController?.pushViewController(vc, animated: true)
            
            
        } else if indexPath.row == 4 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let vc = storyboard.instantiateViewController(withIdentifier: "RequestViewController") as!
//                RequestViewController
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            navigationController?.pushViewController(vc, animated: true)
            
            
        } else if indexPath.row == 5 {
            let storyboard = UIStoryboard(name: "Schedule", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            navigationController?.pushViewController(vc, animated: true)
            
        } else if indexPath.row == 6 {
            let storyboard = UIStoryboard(name: "Schedule", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AddScheduleVC") as! AddScheduleVC
            navigationController?.pushViewController(vc, animated: true)
            
        } else if indexPath.row == 7 {
            // logoout
            print("logout")
            deleteToken()
            let defaults = UserDefaults.standard
            defaults.removeObject(forKey: "username")
            defaults.removeObject(forKey: "employeeId")
            defaults.removeObject(forKey: "profilePic")
            defaults.removeObject(forKey: "userId")
            defaults.removeObject(forKey: "isLoginIn")
            defaults.removeObject(forKey: "token")
            defaults.removeObject(forKey: "isProfileApproved")
            defaults.removeObject(forKey: "isTokenExist")
            defaults.removeObject(forKey: "serviceType")
            defaults.synchronize()
            
            Utility.shared.stopTimer()
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "viewController") as! ViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        }
    }
    
}
