//
//  EditProfileViewController.swift
//  Fleet Management
//
//  Created by Atta Khan on 31/03/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController {
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var phoneNumberTF: UITextField!
    @IBOutlet weak var EntityTF: UITextField!
    @IBOutlet weak var departmentTF: UITextField!
    @IBOutlet weak var cityTF: UITextField!
    @IBOutlet weak var officeTF: UITextField!
    @IBOutlet weak var cityBtn: UIButton!
    var delegate: UpdatedProfile?
    var profile_data: ProfileData?
    var uniqueCity: [Route]?
    var filterOffice: [Route]?
    var data: [Route]?
    var entityData: [EntityResult]?
    var departmentData: [EntityResult]?
    var pickupLat: Double?
    var pickupLng: Double?
    var dropOfflat: Double?
    var dropOfflng: Double?
    var selectedCity: Int = 0
    var selectedOffice: Int = 0
    var selectedEntity: Int = 0
    var selectedDepartment: Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.shared.setStatusBarBackgroundColor(view: view, color: UIColor.themeColor)
        // Do any additional setup after loading the view.
        updateUI()
        getOfficeBuilding()
        firstNameTF.delegate = self
        lastNameTF.delegate = self
        EntityTF.delegate = self
        departmentTF.delegate = self
        
        firstNameTF.keyboardType = .asciiCapable
        lastNameTF.keyboardType = .asciiCapable
        EntityTF.keyboardType = .asciiCapable
        departmentTF.keyboardType = .asciiCapable
        getEntity()
    }
    
    func updateUI() {
        if let data = profile_data {
            if let fname = data.name {
                firstNameTF.text = fname
            }
            if let phoneNumber = data.phoneNumber {
                phoneNumberTF.text = phoneNumber
            }
            if let emailAddress = data.emailAddress{
                emailTF.text = emailAddress
            }
            
            if let lastNameTxt = data.surname {
                lastNameTF.text = lastNameTxt
            }
            if let entity = data.entity {
                EntityTF.text = entity
            }
            
            if let department = data.department {
                departmentTF.text = department
            }
            if let city = data.city {
                cityTF.text = city
            }
            if let dropAddress = data.dropAddress {
                officeTF.text = dropAddress
            }
        }
    }
    func getOfficeBuilding() {
        let url = URL(string: GET_OFFICE_BUILDING)!
        showSpinner(onView: self.view)
        WebServiceManager.sharedInstance.getRequest(params: nil, url: url, serviceType: "Get Office", modelType: ResultModel.self, success: { (response) in
            let responseObj = response as! ResultModel
            self.removeSpinner()
            if responseObj.result?.totalCount ?? 0 > 0 {
                self.data = responseObj.result?.items
                self.uniqueCity?.removeAll()
                guard let cities = self.data else {
                   return
                }
                self.uniqueCity = cities.unique(by: { $0.city })
                
            }
        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
    }
    
    
    private func chooseValue(_ tag: Int, title: String, _ selected: Int) {
        let vc = UIViewController()
        vc.preferredContentSize = CGSize(width: 250,height: 200)
        let pickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: 250, height: 200))
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.tag = tag
        pickerView.selectRow(selected, inComponent:0, animated:true)
        vc.view.addSubview(pickerView)
        let editRadiusAlert = UIAlertController(title: title, message: "", preferredStyle: UIAlertController.Style.alert)
        editRadiusAlert.setValue(vc, forKey: "contentViewController")
        editRadiusAlert.addAction(UIAlertAction(title: "Done", style: .default, handler: { (alert: UIAlertAction!) in
            if tag == 1 {
                self.cityTF.text = self.uniqueCity?[self.selectedCity].city
                let cityName = self.uniqueCity?[self.selectedCity].city
                self.filterOffice?.removeAll()
                self.filterOffice = self.data?.filter( { $0.city == cityName} )
                self.officeTF.text = ""
                self.selectedOffice = 0
            } else if tag == 2 {
                self.officeTF.text = self.filterOffice?[self.selectedOffice].name
                self.dropOfflat = self.filterOffice?[self.selectedOffice].lat
                self.dropOfflng = self.filterOffice?[self.selectedOffice].long
            } else if tag == 3 {
                self.EntityTF.text = self.entityData?[self.selectedEntity].name
                self.getDepartment(self.selectedEntity)
                self.departmentTF.text = ""
                self.selectedDepartment = 0
            } else {
                self.departmentTF.text = self.departmentData?[self.selectedDepartment].name
            }
        }))
        self.present(editRadiusAlert, animated: true)
    }
    func getEntity() {
        let url = URL(string: GET_ENTITY)!
        WebServiceManager.sharedInstance.getRequest(params: nil, url: url, serviceType: "Get City", modelType: EntityModel.self, success: { (response) in
            let responseObj = response as! EntityModel
            self.removeSpinner()
            self.entityData = responseObj.result
            
        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
    }
    func getDepartment(_ entityId: Int) {
        let url = URL(string: GET_DEPARTMENT + "?companyEntityId=\(entityId)")!
        showSpinner(onView: self.view)
        WebServiceManager.sharedInstance.getRequest(params: nil, url: url, serviceType: "Get City", modelType: EntityModel.self, success: { (response) in
            let responseObj = response as! EntityModel
            self.removeSpinner()
            self.departmentData = responseObj.result
        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
    }
    
    @IBAction func tapOnEntityBtn(_ sender: Any) {
        guard let _ = entityData else {
            self.showAlert(title: "Alert", message: "No data found.", controller: self) {
            }
            return
        }
        chooseValue(3, title: "Choose Entity", selectedEntity)
    }
    
    @IBAction func tapOnDepartmentBtn(_ sender: Any) {
        guard let _ = departmentData else {
            self.showAlert(title: "Alert", message: "No data found.", controller: self) {
            }
            return
        }
        if departmentData?.count ?? 0 > 0 {
            chooseValue(4, title: "Choose Department", selectedDepartment)
        } else {
            self.showAlert(title: "Alert", message: "No data found.", controller: self) {
            }
        }
    }
    
    @IBAction func navigateBackBtn(_ sender: Any) {
         _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapOnUpdateBtn(_ sender: Any) {
        guard let firstNameTxt  = firstNameTF.text, !firstNameTxt.isEmpty else {
             self.showAlert(title: "Alert", message: "Please enter your first name.", controller: self) {
                 self.firstNameTF.becomeFirstResponder()
             }
             return
         }
        
         guard let lstNameTxt  = lastNameTF.text, !lstNameTxt.isEmpty else {
             self.showAlert(title: "Alert", message: "Please enter your last name.", controller: self) {
                 self.lastNameTF.becomeFirstResponder()
             }
             return
         }
         
         
         
         guard let emailTxt  = emailTF.text, !emailTxt.isEmpty else {
             self.showAlert(title: "Alert", message: "Please enter your email.", controller: self) {
                 self.emailTF.becomeFirstResponder()
             }
             return
         }
         if !emailTxt.isValidEmail {
            self.showAlert(title: "Alert", message: "Please enter valid email.", controller: self) {
                 self.emailTF.becomeFirstResponder()
             }
             return
         }
        
        guard let phonenumberTxt  = phoneNumberTF.text, !phonenumberTxt.isEmpty else {
            self.showAlert(title: "Alert", message: "Please enter your phone number.", controller: self) {
                self.phoneNumberTF.becomeFirstResponder()
            }
            return
        }
        if phoneNumberTF.text?.validateMobileNumber() == false {
            self.showAlert(title: "Alert", message: "Please enter valid phone number.", controller: self) {
                self.phoneNumberTF.becomeFirstResponder()
            }
            return
        }
        
        guard let entityTxt  = EntityTF.text, !entityTxt.isEmpty else {
            self.showAlert(title: "Alert", message: "Please enter your entity.", controller: self) {
                self.EntityTF.becomeFirstResponder()
            }
            return
        }
        guard let departmentTxt  = departmentTF.text, !departmentTxt.isEmpty else {
            self.showAlert(title: "Alert", message: "Please enter your department.", controller: self) {
                self.departmentTF.becomeFirstResponder()
            }
            return
        }
        
        
        guard let cityTxt  = cityTF.text, !cityTxt.isEmpty else {
            self.showAlert(title: "Alert", message: "Please select your city.", controller: self) {
                self.cityTF.becomeFirstResponder()
            }
            return
        }
        
        guard let officeTxt  = officeTF.text, !officeTxt.isEmpty else {
            self.showAlert(title: "Alert", message: "Please select your office.", controller: self) {
                self.officeTF.becomeFirstResponder()
            }
            return
        }
        profile_data?.name = firstNameTxt
        profile_data?.surname = lstNameTxt
        profile_data?.emailAddress = emailTxt
        profile_data?.phoneNumber = phonenumberTxt
        profile_data?.entity = entityTxt
        profile_data?.department = departmentTxt
        profile_data?.city = cityTxt
        profile_data?.dropAddress = officeTxt
        profile_data?.dropLat = self.dropOfflat
        profile_data?.dropLong = self.dropOfflng
        let encode = JSONEncoder()
        let jsonData = try! encode.encode(profile_data)
        let url = URL(string: UPDATE_USER_PROFILE)!
        showSpinner(onView: self.view)
        WebServiceManager.sharedInstance.putRequest(params: jsonData , url: url, serviceType: "Update Profile", modelType: UserProfileModel.self, success: { (response) in
            let responseObj = response as! UserProfileModel
            self.removeSpinner()
            if responseObj.success == true {
                self.profile_data = responseObj.result
                DispatchQueue.main.async { () -> Void in
                    self.showAlert(title: "Alert", message: "Data updated successfully.", controller: self) {
                        NotificationCenter.default.post(name: .updateUserProfile,
                                                        object: nil, userInfo: ["userProfile": self.profile_data]
                        )

                        self.delegate?.get_updated_data(data: self.profile_data!)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            } else {
                print(responseObj.error?.message)
            }
        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
        
        
    }
    
    
    @IBAction func tapOnOfficeBtn(_ sender: Any) {
        
        
        
        guard let _ = data else {
            self.showAlert(title: "Alert", message: "No data found.", controller: self) {
            }
            return
        }
        
        guard let cityTxt  = cityTF.text, !cityTxt.isEmpty else {
            self.showAlert(title: "Alert", message: "Please select your city.", controller: self) {
                self.cityTF.becomeFirstResponder()
                }
            return
        }
        self.filterOffice = self.data?.filter( { $0.city == cityTxt} )
        chooseValue(2, title: "Office", selectedOffice)
    }
    @IBAction func tapOnCityBtn(_ sender: Any) {
        guard let _ = uniqueCity else {
            self.showAlert(title: "Alert", message: "No data found.", controller: self) {
            }
            return
        }
        chooseValue(1, title: "City", selectedCity)
    }
    
}
extension EditProfileViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1 {
            return uniqueCity?.count ?? 0
        } else if pickerView.tag == 2 {
            return filterOffice?.count ?? 0
        } else if pickerView.tag == 3 {
            return entityData?.count ?? 0
        } else if pickerView.tag == 4 {
            return departmentData?.count ?? 0
        }
        return 0
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1 {
            return uniqueCity?[row].city
        } else if pickerView.tag == 2 {
            return filterOffice?[row].name
        }  else if pickerView.tag == 3 {
            return entityData?[row].name
        }  else if pickerView.tag == 4 {
            return departmentData?[row].name
        }
        return ""
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1 {
            cityTF.text = uniqueCity?[row].city
            selectedCity = row
        } else if pickerView.tag == 1 {
            officeTF.text = filterOffice?[row].name
            selectedOffice = row
            
        } else if pickerView.tag == 3 {
            EntityTF.text = entityData?[row].name
            selectedEntity = row
            
        } else if pickerView.tag == 4 {
            departmentTF.text = departmentData?[row].name
            selectedDepartment = row
            
        }
    }
}
extension EditProfileViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == EntityTF || textField == departmentTF || textField == lastNameTF || textField == firstNameTF {
            if range.location == 0 && string == " " { // prevent space on first character
                return false
            }

            if textField.text?.last == " " && string == " " { // allowed only single space
                return false
            }

            if string == " " { return true } // now allowing space between name

            if string.rangeOfCharacter(from: CharacterSet.letters.inverted) != nil {
                return false
            }
            return true
        }
        
        return true
    }
}
