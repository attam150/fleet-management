//
//  ProfileViewController.swift
//  Fleet Management
//
//  Created by Atta Khan on 31/03/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit
import Alamofire
import MobileCoreServices
import PKRevealController

protocol UpdatedProfile {
    func get_updated_data(data: ProfileData)
}



class ProfileViewController: UIViewController {

    @IBOutlet weak var updateRouteBtn: UIButton!
    @IBOutlet weak var serviceTypePopupView: UIView!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var pickupBtn: UIButton!
    @IBOutlet weak var dropOfBtn: UIButton!
    @IBOutlet weak var bothBtn: UIButton!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var empIdLbl: UILabel!
    @IBOutlet weak var entityLbl: UILabel!
    @IBOutlet weak var departmentLbl: UILabel!
    @IBOutlet weak var routeLbl: UILabel!
    @IBOutlet weak var routeIdLbl: UILabel!
    @IBOutlet weak var startAddressLbl: UILabel!
    @IBOutlet weak var endAddressLbl: UILabel!
    @IBOutlet weak var userImgView: UIImageView!
    
    @IBOutlet weak var serviceTypeLbl: UILabel!
    @IBOutlet weak var dropOffAddressTF: UILabel!
    @IBOutlet weak var pickupAddress: UILabel!
    var profile_data: ProfileData?
    var imagePicker: UIImagePickerController!
    let photoPicker = PhotoPicker()
    var strBase64 : String?
    var image: UIImage? = UIImage()
    var imageURL: String? = ""
    var isUploadImg: Bool = false
    var isComing: String?
    @IBOutlet weak var viewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var errorView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.shared.setStatusBarBackgroundColor(view: view, color: UIColor.themeColor)
        //errorView.roundCorners([.layerMaxXMaxYCorner, .layerMinXMaxYCorner], radius: 8, borderColor: .lightGray, borderWidth: 0.2)
        self.userImgView.makeRounded()
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        getUserData()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        blurView.addGestureRecognizer(tap)
        
        let status = UserDefaults.standard.bool(forKey: "routeSelectionStatus")
        if status == false {
            updateRouteBtn.isUserInteractionEnabled = false
        }
        let serviceType = UserDefaults.standard.integer(forKey: "serviceType")
        if serviceType == 1 {
            serviceTypeLbl.text = "Pickup"
        } else if serviceType == 2 {
            serviceTypeLbl.text = "Drop off"
        } else {
            serviceTypeLbl.text = "Both"
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        blurView.isHidden = true
        serviceTypePopupView.isHidden = true
        let serviceType = UserDefaults.standard.integer(forKey: "serviceType")
        profile_data?.serviceType = ServicesType(rawValue: serviceType)
        let encode = JSONEncoder()
        let jsonData = try! encode.encode(profile_data)
        let url = URL(string: UPDATE_USER_PROFILE)!
        showSpinner(onView: self.view)
        WebServiceManager.sharedInstance.putRequest(params: jsonData , url: url, serviceType: "Update Profile", modelType: UserProfileModel.self, success: { (response) in
            let responseObj = response as! UserProfileModel
            self.removeSpinner()
            if responseObj.success == true {
                self.profile_data = responseObj.result
                DispatchQueue.main.async { () -> Void in
                    
                }
            } else {
                print(responseObj.error?.message)
            }
        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                
            }
        }, showHUD: true)
    }
    
    private func getUserData() {
        let user_id = UserDefaults.standard.integer(forKey: "userId")
        let url = URL(string: GET_PROFILE_DATA + "?id=\(user_id)")!
        showSpinner(onView: self.view)
        WebServiceManager.sharedInstance.getRequest(params: nil, url: url, serviceType: "Get City", modelType: UserProfileModel.self, success: { (response) in
            let responseObj = response as! UserProfileModel
            self.removeSpinner()
            if responseObj.success == true {
                self.profile_data = responseObj.result
                DispatchQueue.main.async { () -> Void in
                    self.updateUI()
                }
            } else {
                print(responseObj.error?.message)
            }
            
            
        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
    }
    
    func updateUI() {
        if let data = profile_data {
            if let city = data.city {
                addressLbl.text = city
            }
            if let name = data.fullName {
                usernameLbl.text = name
            }
            if let phoneNumber = data.phoneNumber {
                phoneLbl.text = phoneNumber
            }
            if let emailAddress = data.emailAddress, let city = data.route?.city {
                emailLbl.text = emailAddress
            }
            
            if let employee_id = data.employeeId {
                empIdLbl.text = employee_id
            }
            if let entity = data.entity {
                entityLbl.text = entity
            }
            
            if let department = data.department {
                departmentLbl.text = department
            }
            if let pickAddress = data.pickupAddress {
                pickupAddress.text = pickAddress
            }
            if let dropAddress = data.dropAddress {
                dropOffAddressTF.text = dropAddress
            }
            
            if let routeName = data.route?.name {
                routeLbl.text = routeName
            }
            if let id = data.route?.id {
                routeIdLbl.text = "\(id)"
            }
            
            if let startingAddress = data.route?.startingAddress {
                startAddressLbl.text = startingAddress
            }
            
            if let endingAddress = data.route?.endingAddress {
                endAddressLbl.text = endingAddress
            }
            if let profile_img = data.profilePicture {
                let userImg = BASE_URL_MEDIA + profile_img
                userImgView.setImage(with: userImg, placeholder: nil)
                //userImgView.sd_setImage(with: URL(string: userImg), placeholderImage: UIImage(named: "placeholder"))
                UserDefaults.standard.set(userImg, forKey: "profilePic")
            }
            
            if let isProfileApproved = data.isProfileApproved, isProfileApproved == false {
                UserDefaults.standard.set(false, forKey: "isProfileApproved")
                errorView.isHidden = false
                viewHeightConstraint.constant = 120
            } else {
                UserDefaults.standard.set(true, forKey: "isProfileApproved")
                errorView.isHidden = true
                viewHeightConstraint.constant = 82
            }
            
            
        }
    }
    
    @IBAction func udpateRouteBtn(_ sender: Any) {
        let isProfileApproved = UserDefaults.standard.bool(forKey: "isProfileApproved")
        if isProfileApproved == false {
            self.showAlert(title: "Alert", message: "Your profile seems to be un approved please check for any pending request. \n Otherwise contact admin", controller: self) {
            }
        } else {
            let vc = storyboard?.instantiateViewController(withIdentifier: "UpdateRouteViewController") as! UpdateRouteViewController
            vc.profile_data = profile_data
            vc.delegate = self
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func udpatePickupAddress(_ sender: Any) {
        let isProfileApproved = UserDefaults.standard.bool(forKey: "isProfileApproved")
        if isProfileApproved == false {
            self.showAlert(title: "Alert", message: "Your profile seems to be un approved please check for any pending request. \n Otherwise contact admin", controller: self) {
            }
        } else {
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! WAPickUpLocation
            vc.drofOffLoction = profile_data?.dropAddress
            vc.dropOffLat =  profile_data?.dropLat//self.filterOffice?[self.selectedOffice].lat
            vc.dropoffLong = profile_data?.dropLong//self.filterOffice?[self.selectedOffice].long
            vc.profile_data = profile_data
            //vc.delegate = self
            navigationController?.pushViewController(vc, animated: true)
        }
       }
    
    @IBAction func tapOnPassChangeBtn(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ChangePassViewController") as! ChangePassViewController
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            imagePicker.delegate = self
            self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self .present(self.imagePicker, animated: true, completion: nil)
        }
        else {
            let alertWarning = UIAlertView(title:"Warning", message: "You don't have camera", delegate:nil, cancelButtonTitle:"OK")
            alertWarning.show()
        }
    }
    func openGallary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
            
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func tapOnUpdateImg(_ sender: Any) {
        let optionMenu = UIAlertController(title: "Choose Your Option", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        let option1 = UIAlertAction(title: "Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openCamera()
        })
        
        let option2 = UIAlertAction(title: "Photo Library ", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openGallary()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        optionMenu.addAction(option1)
        optionMenu.addAction(option2)
        optionMenu.addAction(cancelAction)
        
        
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad )
        {
            if let currentPopoverpresentioncontroller = optionMenu.popoverPresentationController{
                currentPopoverpresentioncontroller.sourceView = userImgView
                currentPopoverpresentioncontroller.sourceRect = userImgView.bounds;
                currentPopoverpresentioncontroller.permittedArrowDirections = UIPopoverArrowDirection.up;
                self.present(optionMenu, animated: true, completion: nil)
            }
        }else{
            self.present(optionMenu, animated: true, completion: nil)
        }
        return
    }
    
    
    
    func createBodyWithParameters(parameters: [String: Any]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            }
        }
        let filename = "user-profile.jpeg"
        let mimetype = "image/jpeg"
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition:form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append(imageDataKey as Data)
        body.append("\r\n".data(using: String.Encoding.utf8)!)
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        
        return body
    }
    
    public override func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("Cancel")
        imagePicker.dismiss(animated: true, completion: nil)
    }

    override func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        //image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        userImgView.contentMode = .scaleAspectFill
        userImgView.image = image
        if let editedImage = info[.editedImage] as? UIImage {
            self.image = editedImage
            self.userImgView.image = editedImage
       } else if let originalImage = info[.originalImage] as? UIImage {
            self.userImgView.image = originalImage
            self.image = originalImage
       }
        myImageUploadRequest()
    }

    func myImageUploadRequest()
    {
        let user_id = UserDefaults.standard.integer(forKey: "userId")
        let myUrl = NSURL(string: UPLOAD_IMAGE)
        let request = NSMutableURLRequest(url:myUrl! as URL)
        request.httpMethod = "POST"
        let param : [String: Any] = [
            "UserId"  : user_id,
            "MediaType"    : 1
        ]
        let boundary = generateBoundaryString()
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        if let capturedImage = self.image {
            let imageData: NSData! = capturedImage.jpegData(compressionQuality: 0.2) as! NSData
            print(imageData.count)
            var imageSize: Int = imageData.count
            print("actual size of image in KB: %f ", Double(imageSize) / 1000.0)
                request.httpBody = createBodyWithParameters(parameters: param as! [String : Any], filePathKey: "File", imageDataKey: imageData as NSData, boundary: boundary) as Data
                //SVProgressHUD.show()
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
                let task = URLSession.shared.dataTask(with: request as URLRequest) {
                    data, response, error in

                    if error != nil {
                        print("error=\(String(describing: error))")
                        return
                    }
                    print("******* response = \(String(describing: response))")
                    let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    print("****** response data = \(responseString!)")
                    let dataString = String(data: data!, encoding: .utf8)
                    guard let responseObj = try? JSONDecoder().decode(ProfileImageModel.self, from: data!) else {
                        return
                    }
                    if responseObj.success == true {
                        
                        let result = responseObj.result
                        if let profilePicture = responseObj.result?.message {
                            let userImg = BASE_URL_MEDIA + profilePicture
                            UserDefaults.standard.set(userImg, forKey: "profilePic")
                            NotificationCenter.default.post(name: .udpateUserImg,
                                                            object: nil, userInfo: ["userProfileImg": userImg]
                            )
                        }
                    }
                    
                    print(responseObj.success)
                }
                task.resume()
        }
        else{
            return
        }
    }

   
   private func generateBoundaryString() -> String {
        return "Boundary-\(UUID().uuidString)"
    }
    
    @IBAction func udpateDropOffLocation(_ sender: Any) {
        let isProfileApproved = UserDefaults.standard.bool(forKey: "isProfileApproved")
        if isProfileApproved == false {
            self.showAlert(title: "Alert", message: "Your profile seems to be un approved please check for any pending request. \n Otherwise contact admin", controller: self) {
            }
        } else {
            let vc = storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
            vc.profile_data = profile_data
            vc.delegate = self
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func tapOnEditProfileBtn(_ sender: Any) {
        let isProfileApproved = UserDefaults.standard.bool(forKey: "isProfileApproved")
        if isProfileApproved == false {
            self.showAlert(title: "Alert", message: "Your profile seems to be un approved please check for any pending request. \n Otherwise contact admin", controller: self) {
            }
        } else {
            let vc = storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
            vc.profile_data = profile_data
            vc.delegate = self
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func navigateBackBtn(_ sender: Any) {
        if let isComing = isComing, isComing == "update address"{
            let frontViewController: UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeController")
            let leftViewController: UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LeftMenuViewController")
            let revealController: PKRevealController = PKRevealController(frontViewController: frontViewController, leftViewController: leftViewController)
            self.navigationController?.pushViewController(revealController, animated: true)
        } else {
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    

    @IBAction func tapOnPickupBtn(_ sender: Any) {
        pickupBtn.setImage(UIImage(named: "selectedRadioBtn"), for: .normal)
        dropOfBtn.setImage(UIImage(named: "unselectedRadioBtn"), for: .normal)
        bothBtn.setImage(UIImage(named: "unselectedRadioBtn"), for: .normal)
        serviceTypeLbl.text = "Pickup"
        UserDefaults.standard.set(ServicesType.pickup.rawValue, forKey: "serviceType")
    }
    
    @IBAction func tapOndropOfBtn(_ sender: Any) {
        pickupBtn.setImage(UIImage(named: "unselectedRadioBtn"), for: .normal)
        dropOfBtn.setImage(UIImage(named: "selectedRadioBtn"), for: .normal)
        bothBtn.setImage(UIImage(named: "unselectedRadioBtn"), for: .normal)
        UserDefaults.standard.set(ServicesType.dropOff.rawValue, forKey: "serviceType")
        serviceTypeLbl.text = "Drop off"
    }
    
    
    @IBAction func tapOnBothBtn(_ sender: Any) {
        pickupBtn.setImage(UIImage(named: "unselectedRadioBtn"), for: .normal)
        dropOfBtn.setImage(UIImage(named: "unselectedRadioBtn"), for: .normal)
        bothBtn.setImage(UIImage(named: "selectedRadioBtn"), for: .normal)
        serviceTypeLbl.text = "Both"
        UserDefaults.standard.set(ServicesType.both.rawValue, forKey: "serviceType")
    }
    
    @IBAction func tapOnServiceTypeBtn(_ sender: Any) {
        blurView.isHidden = false
        serviceTypePopupView.isHidden = false
    }
    
}
extension ProfileViewController: UpdatedProfile {
    func get_updated_data(data: ProfileData) {
        profile_data = data
        self.updateUI()
    }
}

extension ProfileViewController: PickUpLatLnt {
    func pickUpLatitudeAndLong(_ lon: Double, andLatitude lat: Double, title: String) {
        profile_data?.pickupAddress = title
        profile_data?.pickupLat = lat
        profile_data?.pickupLong = lon
        if let polyline = UserDefaults.standard.string(forKey: "directionPolyline") {
            profile_data?.directionPolyline = polyline
        }
        
        
        
        let encode = JSONEncoder()
        let jsonData = try! encode.encode(profile_data)
        
        
        let url = URL(string: UPDATE_USER_PROFILE)!
        showSpinner(onView: self.view)
        WebServiceManager.sharedInstance.putRequest(params: jsonData , url: url, serviceType: "LOGIN", modelType: UserProfileModel.self, success: { (response) in
            let responseObj = response as! UserProfileModel
            self.removeSpinner()
            if responseObj.success == true {
                self.profile_data = responseObj.result
                DispatchQueue.main.async { () -> Void in
                    self.showAlert(title: "Alert", message: "Pickup Address Updated Successfully.", controller: self) {
                        self.updateUI()
                    }
                }
            } else {
                print(responseObj.error?.message)
            }
        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
        
    }
}
extension NSMutableData {

    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}
extension Data {

    /// Append string to Data
    ///
    /// Rather than littering my code with calls to `data(using: .utf8)` to convert `String` values to `Data`, this wraps it in a nice convenient little extension to Data. This defaults to converting using UTF-8.
    ///
    /// - parameter string:       The string to be added to the `Data`.

    mutating func append(_ string: String, using encoding: String.Encoding = .utf8) {
        if let data = string.data(using: encoding) {
            append(data)
        }
    }
}
