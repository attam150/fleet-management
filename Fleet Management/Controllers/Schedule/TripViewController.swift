//
//  TripViewController.swift
//  Fleet Management
//
//  Created by Atta khan on 28/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit

protocol RouteProtocl {
    func selectedRoute(route: Route?, tag: Int, tripId: Int, tripName: String)
    func removeTrip(tag: Int)
}


class TripViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var tripData: [Trip]?
    var delegate: RouteProtocl?
    var tag: Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTableView()
    }
    
    func setUpTableView() {
        tableView.separatorStyle = .singleLine
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 88
        tableView.register(RouteViewCell.nib, forCellReuseIdentifier: RouteViewCell.identifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }

    @IBAction func removeTrip(_ sender: Any) {
        delegate?.removeTrip(tag: tag)
        dismiss(animated: true, completion: nil)
    }
}
extension TripViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tripData?.count ?? 0
    }
    
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RouteViewCell.identifier) as! RouteViewCell
        cell.trip_data = tripData?[indexPath.row]
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let route = tripData?[indexPath.row].route
        let trip_name = tripData?[indexPath.row].name ?? ""
        let trip_id = tripData?[indexPath.row].id ?? 0
        delegate?.selectedRoute(route: route, tag: tag, tripId: trip_id, tripName: trip_name)
        dismiss(animated: true, completion: nil)
    }
    
}
