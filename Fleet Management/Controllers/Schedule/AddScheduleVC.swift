//
//  AddScheduleVC.swift
//  Fleet Management
//
//  Created by Atta khan on 25/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit
import DatePickerDialog
class AddScheduleVC: UIViewController {

    @IBOutlet weak var dateRangelbl: UILabel!
    @IBOutlet weak var pickerView: UIView!
    @IBOutlet weak var mainStackView: UIStackView!
    
    @IBOutlet weak var stackHightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var pickUpView1: UIView!
    @IBOutlet weak var dropOffView1: UIView!
    @IBOutlet weak var pickUpView2: UIView!
    @IBOutlet weak var dropOffView2: UIView!
    @IBOutlet weak var pickUpView3: UIView!
    @IBOutlet weak var dropOffView3: UIView!
    @IBOutlet weak var pickUpView4: UIView!
    @IBOutlet weak var dropOffView4: UIView!
    @IBOutlet weak var pickUpView5: UIView!
    @IBOutlet weak var dropOffView5: UIView!
    @IBOutlet weak var pickUpView6: UIView!
    @IBOutlet weak var dropOffView6: UIView!
    @IBOutlet weak var pickUpView7: UIView!
    @IBOutlet weak var dropOffView7: UIView!
    @IBOutlet weak var datePickerView: UIDatePicker!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var pickUpTF1: UITextField!
    @IBOutlet weak var pickUpTF2: UITextField!
    @IBOutlet weak var pickUpTF3: UITextField!
    @IBOutlet weak var pickUpTF4: UITextField!
    @IBOutlet weak var pickUpTF5: UITextField!
    @IBOutlet weak var pickUpTF6: UITextField!
    @IBOutlet weak var pickUpTF7: UITextField!
    
     @IBOutlet weak var dropOffTF1: UITextField!
     @IBOutlet weak var dropOffTF2: UITextField!
     @IBOutlet weak var dropOffTF3: UITextField!
     @IBOutlet weak var dropOffTF4: UITextField!
     @IBOutlet weak var dropOffTF5: UITextField!
     @IBOutlet weak var dropOffTF6: UITextField!
     @IBOutlet weak var dropOffTF7: UITextField!
    
    
    @IBOutlet weak var pickTripTF1: UITextField!
    @IBOutlet weak var pickTripTF2: UITextField!
    @IBOutlet weak var pickTripTF3: UITextField!
    @IBOutlet weak var pickTripTF4: UITextField!
    @IBOutlet weak var pickTripTF5: UITextField!
    @IBOutlet weak var pickTripTF6: UITextField!
    @IBOutlet weak var pickTripTF7: UITextField!
    
    @IBOutlet weak var dropOffTripTF1: UITextField!
    @IBOutlet weak var dropOffTripTF2: UITextField!
    @IBOutlet weak var dropOffTripTF3: UITextField!
    @IBOutlet weak var dropOffTripTF4: UITextField!
    @IBOutlet weak var dropOffTripTF5: UITextField!
    @IBOutlet weak var dropOffTripTF6: UITextField!
    @IBOutlet weak var dropOffTripTF7: UITextField!
    
    @IBOutlet weak var pickupTripView1: UIView!
    @IBOutlet weak var dropOffTripView1: UIView!
    @IBOutlet weak var pickupTripView2: UIView!
    @IBOutlet weak var dropOffTripView2: UIView!
    @IBOutlet weak var pickupTripView3: UIView!
    @IBOutlet weak var dropOffTripView3: UIView!
    @IBOutlet weak var pickupTripView4: UIView!
    @IBOutlet weak var dropOffTripView4: UIView!
    @IBOutlet weak var pickupTripView5: UIView!
    @IBOutlet weak var dropOffTripView5: UIView!
    @IBOutlet weak var pickupTripView6: UIView!
    @IBOutlet weak var dropOffTripView6: UIView!
    @IBOutlet weak var pickupTripView7: UIView!
    @IBOutlet weak var dropOffTripView7: UIView!
    
    
    
    
    var selectedRoutePick1 = 0
    var selectedRoutedrop1 = 0
    var selectedRoutePick2 = 0
    var selectedRoutedrop2 = 0
    var selectedRoutePick3 = 0
    var selectedRoutedrop3 = 0
    var selectedRoutePick4 = 0
    var selectedRoutedrop4 = 0
    var selectedRoutePick5 = 0
    var selectedRoutedrop5 = 0
    var selectedRoutePick6 = 0
    var selectedRoutedrop6 = 0
    var selectedRoutePick7 = 0
    var selectedRoutedrop7 = 0
    
    
    var selectedTF = UITextField()
    var scheduleModel = ScheduleModel()
    var scheduleArray = [ScheduleSpecifics]()
    var emptyArray =  [Int]()
    var tripData : [Trip]?
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.shared.setStatusBarBackgroundColor(view: view, color: UIColor.themeColor)
        let serviceType = UserDefaults.standard.integer(forKey: "serviceType")
        datePickerView.datePickerMode = .time
        scheduleArray.reserveCapacity(7)
        scheduleModel.scheduleStartDate = ""
        scheduleModel.scheduleEndDate = ""
        scheduleModel.serviceType = serviceType
        let user_id = UserDefaults.standard.integer(forKey: "userId")
        scheduleModel.userId = user_id
        dateRangelbl.text = "Please select your date"
        addScheduleArray()
        pickerView.isHidden = true
        let isProfileApproved = UserDefaults.standard.bool(forKey: "isProfileApproved")
        if isProfileApproved == false {
            self.showAlert(title: "Alert", message: "Your profile seems to be un approved please check for any pending request. \n Otherwise contact admin", controller: self) {
                _ = self.navigationController?.popViewController(animated: true)
            }
        }
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.rgb(red: 241, green: 241, blue: 241, alpha: 1)

        backgroundView.translatesAutoresizingMaskIntoConstraints = false

        // put background view as the most background subviews of stack view
        mainStackView.insertSubview(backgroundView, at: 0)

        // pin the background view edge to the stack view edge
        NSLayoutConstraint.activate([
            backgroundView.leadingAnchor.constraint(equalTo: mainStackView.leadingAnchor),
            backgroundView.trailingAnchor.constraint(equalTo: mainStackView.trailingAnchor),
            backgroundView.topAnchor.constraint(equalTo: mainStackView.topAnchor),
            backgroundView.bottomAnchor.constraint(equalTo: mainStackView.bottomAnchor)
        ])
        if serviceType == 1 {
            stackHightConstraint.constant = 65.0
            toggleView(false, true)
        } else if serviceType == 2 {
            stackHightConstraint.constant = 65.0
            toggleView(true, false)
        } else {
            stackHightConstraint.constant = 130.0
            toggleView(false, false)
        }
        let isTripAllowed = UserDefaults.standard.bool(forKey: "AppPassengerTripSelection")
        
        if isTripAllowed == false {
            pickupTripView1.isHidden = true
            dropOffTripView1.isHidden = true
            pickupTripView2.isHidden = true
            dropOffTripView2.isHidden = true
            pickupTripView3.isHidden = true
            dropOffTripView3.isHidden = true
            pickupTripView4.isHidden = true
            dropOffTripView4.isHidden = true
            pickupTripView5.isHidden = true
            dropOffTripView5.isHidden = true
            pickupTripView6.isHidden = true
            dropOffTripView6.isHidden = true
            pickupTripView7.isHidden = true
            dropOffTripView7.isHidden = true
        }
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
       let tripStatus = UserDefaults.standard.bool(forKey: "AppPassengerTripSelection")
        if tripStatus == true {
            getTrips()
        }
    }
    func toggleView(_ pickViewState: Bool = false, _ dropOffState: Bool = false) {
        pickUpView1.isHidden = pickViewState
        pickUpView2.isHidden = pickViewState
        pickUpView3.isHidden = pickViewState
        pickUpView4.isHidden = pickViewState
        pickUpView5.isHidden = pickViewState
        pickUpView6.isHidden = pickViewState
        pickUpView7.isHidden = pickViewState
        dropOffView1.isHidden = dropOffState
        dropOffView2.isHidden = dropOffState
        dropOffView3.isHidden = dropOffState
        dropOffView4.isHidden = dropOffState
        dropOffView5.isHidden = dropOffState
        dropOffView6.isHidden = dropOffState
        dropOffView7.isHidden = dropOffState
    }
    func addScheduleArray() {
         scheduleArray.removeAll()
        for _ in 0..<7 {
            scheduleArray.append(ScheduleSpecifics())
        }
        scheduleArray[0].weekday = "Monday"
        scheduleArray[1].weekday = "Tuesday"
        scheduleArray[2].weekday = "wednsday"
        scheduleArray[3].weekday = "Thursday"
        scheduleArray[4].weekday = "Friday"
        scheduleArray[5].weekday = "Saturday"
        scheduleArray[6].weekday = "Sunday"
        scheduleModel.scheduleSpecifics = scheduleArray

    }
    
    func getTrips() {
        let routeId = UserDefaults.standard.integer(forKey: "routeId")
        let url = URL(string: GET_TRIPS + "?RouteId=\(routeId)")!
        showSpinner(onView: self.view)
        WebServiceManager.sharedInstance.getRequest(params: nil, url: url, serviceType: "Get Trip", modelType: TripModel.self, success: { (response) in
            let responseObj = response as! TripModel
            self.removeSpinner()
            if responseObj.success == true {
                let count = responseObj.result?.totalCount ?? 0
                if count == 0 {
                    DispatchQueue.main.async {
                        self.showAlert(title: "Alert", message: "No trip avialble please update your profile.", controller: self) {
//                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                            let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
//                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                }
                self.tripData = responseObj.result?.items
            }
        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
    }
    
    func datePickerTapped() {
            
    }
    
    @IBAction func tapOnCancelBtn(_ sender: Any) {
        pickerView.isHidden = true
        selectedTF.text = ""

    }
    @IBAction func tapOnDoneBtn(_ sender: Any) {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        //dd-MM-yyyy hh:mm a
        selectedTF.text = formatter.string(from: datePickerView.date)
        pickerView.isHidden = true
    }
    
    func validateFieldsInput(_ pickUpTimeTF: UITextField,_ pickUpTripTF: UITextField, _ dropOffTimeTF: UITextField, _ dropOffTripTF: UITextField, _ day: String) -> Bool {
        let isTripAllowed = UserDefaults.standard.bool(forKey: "AppPassengerTripSelection")
        let serviceType = UserDefaults.standard.integer(forKey: "serviceType")
        if isTripAllowed == false {
            if serviceType == 1 {
//                if pickUpTimeTF.text != "" {
//                    return true
//                } else if pickUpTimeTF.text == "" {
//                    self.showAlert(title: "Alert", message: "Select pick up time.", controller: self) {
//                    }
//                    return false
//                }
                return true
            }
            else if serviceType == 2 {
//                if dropOffTimeTF.text != "" {
//                    return true
//                } else if dropOffTimeTF.text == "" {
//                    self.showAlert(title: "Alert", message: "Select pick up time.", controller: self) {
//                    }
//                    return false
//                }
                return true
            }
            else {
                if (pickUpTimeTF.text != "" && dropOffTimeTF.text != "")  || (pickUpTimeTF.text == "" && dropOffTimeTF.text == "") {
                    return true
                } else if pickUpTimeTF.text == "" {
                    self.showAlert(title: "Alert", message: "Select pick up time.", controller: self) {
                    }
                    return false
                } else if dropOffTimeTF.text == "" {
                    self.showAlert(title: "Alert", message: "Select drop off time.", controller: self) {
                    }
                    return false
                } else {
                    return true
                }
            }
        }
        else {
            if serviceType == 1 {
                if (pickUpTimeTF.text != "" && pickUpTripTF.text != "") || (pickUpTimeTF.text == "" && pickUpTripTF.text == "") {
                    return true
                } else if pickUpTimeTF.text == "" {
                    self.showAlert(title: "Alert", message: "Select pick up time.", controller: self) {
                    }
                    return false
                } else if pickUpTripTF.text == "" {
                    self.showAlert(title: "Alert", message: "Select pick up trip.", controller: self) {
                    }
                    return false
                }
                return true
            }
            else if serviceType == 2 {
                if (dropOffTimeTF.text != "" && dropOffTripTF.text != "") || (dropOffTimeTF.text == "" && dropOffTripTF.text == "") {
                    return true
                } else if dropOffTimeTF.text == "" {
                    self.showAlert(title: "Alert", message: "Select pick up time.", controller: self) {
                    }
                    return false
                } else if dropOffTripTF.text == "" {
                    self.showAlert(title: "Alert", message: "Select pick up trip.", controller: self) {
                    }
                    return false
                }
                return true
            }
            else {
                if pickUpTimeTF.text != "" && pickUpTripTF.text != "" && dropOffTimeTF.text != "" && dropOffTripTF.text != "" {
                    return true
                } else if pickUpTimeTF.text == "" && pickUpTripTF.text == "" && dropOffTimeTF.text == "" && dropOffTripTF.text == "" {
                    return true
                } else if pickUpTimeTF.text == "" {
                    self.showAlert(title: "Alert", message: "Select pick up time.", controller: self) {
                    }
                                    return false
                } else if pickUpTripTF.text == "" {
                    self.showAlert(title: "Alert", message: "Select pick up trip.", controller: self) {
                    }
                    return false
                } else if dropOffTimeTF.text == "" {
                    self.showAlert(title: "Alert", message: "Select drop off time.", controller: self) {
                    }
                    return false
                } else if dropOffTripTF.text == "" {
                    self.showAlert(title: "Alert", message: "Select drop off trip.", controller: self) {
                    }
                    return false
                } else {
                    return true
                }
            }
        }
        return true
    }
    
    
    
    
    @IBAction func tapOnSubmitBtn(_ sender: Any) {
        emptyArray.removeAll()
        if dateRangelbl.text == "" || dateRangelbl.text == "Please select your date" {
            self.showAlert(title: "Alert", message: "Select date range.", controller: self) {
            }
            return
        }
        
        
        let weeak1Validate = validateFieldsInput(pickUpTF1, pickTripTF1, dropOffTF1, dropOffTripTF1, "Monday")
        let weeak2Validate = validateFieldsInput(pickUpTF2, pickTripTF2, dropOffTF2, dropOffTripTF2, "Monday")
        let weeak3Validate = validateFieldsInput(pickUpTF3, pickTripTF3, dropOffTF3, dropOffTripTF3, "Monday")
        let weeak4Validate = validateFieldsInput(pickUpTF4, pickTripTF4, dropOffTF4, dropOffTripTF4, "Monday")
        let weeak5Validate = validateFieldsInput(pickUpTF5, pickTripTF5, dropOffTF5, dropOffTripTF5, "Monday")
        let weeak6Validate = validateFieldsInput(pickUpTF6, pickTripTF6, dropOffTF6, dropOffTripTF6, "Monday")
        let weeak7Validate = validateFieldsInput(pickUpTF7, pickTripTF7, dropOffTF7, dropOffTripTF7, "Monday")
        
        if weeak1Validate && weeak2Validate && weeak3Validate && weeak4Validate && weeak5Validate && weeak6Validate && weeak7Validate {
            if (pickUpTF1.text == "" && pickUpTF2.text == "" && pickUpTF3.text == "" && pickUpTF4.text == "" && pickUpTF5.text == "" && pickUpTF6.text == "" && pickUpTF7.text == "") && (dropOffTF1.text == "" && dropOffTF2.text == "" && dropOffTF3.text == "" && dropOffTF4.text == "" && dropOffTF5.text == "" && dropOffTF6.text == "" && dropOffTF7.text == "") {
                self.showAlert(title: "Alert", message: "Please enter atleast one day schedule.", controller: self) {
                           }
                return
            }
            if pickUpTF7.text != "" || dropOffTF7.text != "" {
                scheduleArray[6].weekday = "Sunday"
                scheduleArray[6].pickupTime = pickUpTF7.text!
                scheduleArray[6].pickupTripId = selectedRoutePick7
                scheduleArray[6].dropoffTime = dropOffTF7.text!
                scheduleArray[6].dropoffTripId = selectedRoutedrop7
            } else {
                emptyArray.append(6)
            }
            
            if pickUpTF6.text != "" || dropOffTF6.text != "" {
                scheduleArray[5].weekday = "Saturday"
                scheduleArray[5].pickupTime = pickUpTF6.text!
                scheduleArray[5].pickupTripId = selectedRoutePick6
                scheduleArray[5].dropoffTime = dropOffTF6.text!
                scheduleArray[5].dropoffTripId = selectedRoutedrop6
            } else {
                emptyArray.append(5)
            }

            if pickUpTF5.text != "" || dropOffTF5.text != "" {
                scheduleArray[4].weekday = "Friday"
                scheduleArray[4].pickupTime = pickUpTF5.text!
                scheduleArray[4].pickupTripId = selectedRoutePick5
                scheduleArray[4].dropoffTime = dropOffTF5.text!
                scheduleArray[4].dropoffTripId = selectedRoutedrop5
            } else {
                emptyArray.append(4)
            }
            if pickUpTF4.text != "" || dropOffTF4.text != "" {
                scheduleArray[3].weekday = "Thursday"
                scheduleArray[3].pickupTime = pickUpTF4.text!
                scheduleArray[3].pickupTripId = selectedRoutePick4
                scheduleArray[3].dropoffTime = dropOffTF4.text!
                scheduleArray[3].dropoffTripId = selectedRoutedrop4
            } else {
                emptyArray.append(3)
            }
            if pickUpTF3.text != "" || dropOffTF3.text != "" {
                scheduleArray[2].weekday = "Wednesday"
                scheduleArray[2].pickupTime = pickUpTF3.text!
                scheduleArray[2].pickupTripId = selectedRoutePick3
                scheduleArray[2].dropoffTime = dropOffTF3.text!
                scheduleArray[2].dropoffTripId = selectedRoutedrop3
            } else {
                emptyArray.append(2)
            }
            if pickUpTF2.text != "" || dropOffTF2.text != "" {
                scheduleArray[1].weekday = "Tuesday"
                scheduleArray[1].pickupTime = pickUpTF2.text!
                scheduleArray[1].pickupTripId = selectedRoutePick2
                scheduleArray[1].dropoffTime = dropOffTF2.text!
                scheduleArray[1].dropoffTripId = selectedRoutedrop2
            } else {
                emptyArray.append(1)
            }
            
            if pickUpTF1.text != "" || dropOffTF1.text != "" {
                scheduleArray[0].weekday = "Monday"
                scheduleArray[0].pickupTime = pickUpTF1.text!
                scheduleArray[0].pickupTripId = selectedRoutePick1
                scheduleArray[0].dropoffTime = dropOffTF1.text!
                scheduleArray[0].dropoffTripId = selectedRoutedrop1
            } else {
                emptyArray.append(0)
            }
            scheduleArray.remove(at: emptyArray)
            scheduleModel.scheduleSpecifics = scheduleArray
            print(scheduleModel)
            
            // after sumbit success or error
            
            let encode = JSONEncoder()
            let jsonData = try! encode.encode(scheduleModel)
            let url = URL(string: CREATE_SCHEDULE)!
            showSpinner(onView: self.view)
            WebServiceManager.sharedInstance.postRequest(params: jsonData , url: url, serviceType: "LOGIN", modelType: CreateScheduleModel.self, success: { (response) in
                let responseObj = response as! CreateScheduleModel
                self.removeSpinner()
                print(responseObj)
                if responseObj.success == true{
                     DispatchQueue.main.async { () -> Void in
                        self.showAlert(title: "Alert", message: "Schedule has been added successfully.", controller: self) {
                            _ = self.navigationController?.popViewController(animated: true)
                        }
                    }
                    
                } else {
                    DispatchQueue.main.async { () -> Void in
                        self.showAlert(title: "Alert", message: (responseObj.error?.message!)!, controller: self) {
                        }
                    }
                }
                
    
            }, fail: { (error) in
                self.removeSpinner()
                print(error)
                DispatchQueue.main.async { () -> Void in
                    self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                    }
                }
            }, showHUD: true)
        }
        addScheduleArray()
    }
    
    @IBAction func tapOnDropBtn(_ sender: UIButton) {
        pickerView.isHidden = false
        let tag = sender.tag
        if tag == 2 {
            selectedTF = dropOffTF1
        }
        if tag == 4 {
            selectedTF = dropOffTF2
        }
        if tag == 6 {
            selectedTF = dropOffTF3
        }
        if tag == 8 {
            selectedTF = dropOffTF4
        }
        if tag == 10 {
            selectedTF = dropOffTF5
        }
        if tag == 12 {
            selectedTF = dropOffTF6
        }
        if tag == 14 {
            selectedTF = dropOffTF7
        }
    }
    
    @IBAction func tapOnPickBtn(_ sender: UIButton) {
        pickerView.isHidden = false
        let tag = sender.tag
        if tag == 1 {
            selectedTF = pickUpTF1
        }
        if tag == 3 {
            selectedTF = pickUpTF2
        }
        if tag == 5 {
            selectedTF = pickUpTF3
        }
        if tag == 7 {
            selectedTF = pickUpTF4
        }
        if tag == 9 {
            selectedTF = pickUpTF5
        }
        if tag == 11 {
            selectedTF = pickUpTF6
        }
        if tag == 13 {
            selectedTF = pickUpTF7
        }
        
        
    }
    
    
    
    @IBAction func tapOnCalandarBtn(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ScheduleCalendarVC") as! ScheduleCalendarVC
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func backNavigateBtn(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    @IBAction func tapOnDropoffBtn(_ sender: UIButton) {
        let tag = sender.tag
        tripRouteDetails(tag, .DROP)
    }
    @IBAction func tapOnPickTripBtn(_ sender: UIButton) {
        let tag = sender.tag
        tripRouteDetails(tag, .PICKUP)
    }
    func tripRouteDetails(_ tag: Int, _ type: TripType) {
        let storyboard = UIStoryboard(name: "Schedule", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TripViewController") as? TripViewController
        if type == .PICKUP {
            let pickTripData = tripData?.filter( { $0.type == TripType.PICKUP })
            vc?.tripData = pickTripData
        } else {
            let dropOffTripData = tripData?.filter( { $0.type == TripType.DROP })
            vc?.tripData = dropOffTripData
        }
        vc?.tag = tag
        vc?.delegate = self
        if #available(iOS 10.0, *) {
            vc?.modalPresentationStyle = .overCurrentContext
        } else {
            vc?.modalPresentationStyle = .currentContext
        }
        vc?.providesPresentationContextTransitionStyle = true
        present(vc!, animated: true, completion: {() -> Void in
        })
    }
}
extension AddScheduleVC: RouteProtocl {
    func removeTrip(tag: Int) {
        if tag == 1 {
            pickTripTF1.text = ""
            selectedRoutePick1 = 0
        }
        if tag == 2 {
            dropOffTripTF1.text = ""
            selectedRoutedrop1 = 0
        }
        if tag == 3 {
            pickTripTF2.text = ""
            selectedRoutePick2 = 0
        }
        if tag == 4 {
            dropOffTripTF2.text = ""
            selectedRoutedrop2 = 0
        }
        
        if tag == 5 {
            pickTripTF3.text = ""
            selectedRoutePick3 = 0
        }
        if tag == 6 {
            dropOffTripTF3.text = ""
            selectedRoutedrop3 = 0
        }
        
        if tag == 7 {
            pickTripTF4.text = ""
            selectedRoutePick4 = 0
        }
        if tag == 8 {
            dropOffTripTF4.text = ""
            selectedRoutedrop4 = 0
        }
        
        if tag == 9 {
            pickTripTF5.text = ""
            selectedRoutePick5 = 0
        }
        if tag == 10 {
            dropOffTripTF5.text = ""
            selectedRoutedrop5 = 0
        }
        
        if tag == 11 {
            pickTripTF6.text = ""
            selectedRoutePick6 = 0
        }
        if tag == 12 {
            dropOffTripTF6.text = ""
            selectedRoutedrop6 = 0
        }
        
        if tag == 13 {
            pickTripTF7.text = ""
            selectedRoutePick7 = 0
        }
        if tag == 14 {
            dropOffTripTF7.text = ""
            selectedRoutedrop7 = 0
        }
    }
    
    func selectedRoute(route: Route?, tag: Int, tripId: Int, tripName: String) {
        if tag == 1 {
            pickTripTF1.text = tripName
            selectedRoutePick1 = tripId
        }
        if tag == 2 {
            dropOffTripTF1.text = tripName
            selectedRoutedrop1 = tripId
        }
        if tag == 3 {
            pickTripTF2.text = tripName
            selectedRoutePick2 = tripId
        }
        if tag == 4 {
            dropOffTripTF2.text = tripName
            selectedRoutedrop2 = tripId
        }
        
        if tag == 5 {
            pickTripTF3.text = tripName
            selectedRoutePick3 = tripId
        }
        if tag == 6 {
            dropOffTripTF3.text = tripName
            selectedRoutedrop3 = tripId
        }
        
        if tag == 7 {
            pickTripTF4.text = tripName
            selectedRoutePick4 = tripId
        }
        if tag == 8 {
            dropOffTripTF4.text = tripName
            selectedRoutedrop4 = tripId
        }
        
        if tag == 9 {
            pickTripTF5.text = tripName
            selectedRoutePick5 = tripId
        }
        if tag == 10 {
            dropOffTripTF5.text = tripName
            selectedRoutedrop5 = tripId
        }
        
        if tag == 11 {
            pickTripTF6.text = tripName
            selectedRoutePick6 = tripId
        }
        if tag == 12 {
            dropOffTripTF6.text = tripName
            selectedRoutedrop6 = tripId
        }
        
        if tag == 13 {
            pickTripTF7.text = tripName
            selectedRoutePick7 = tripId
        }
        if tag == 14 {
            dropOffTripTF7.text = tripName
            selectedRoutedrop7 = tripId
        }
        
        
        
        
    }
}
extension AddScheduleVC: SelectedDateRang {
    func get_selected_date(_ startDate: Date, _ lastDate: Date) {
        let startDateStr = startDate.toString(format: "dd-MM-yyyy")
        let endDateStr = lastDate.toString(format:"dd-MM-yyyy")
        
        let scheduleStartDate = startDate.toString(format: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
        let scheduleEndDate = lastDate.toString(format: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
        scheduleModel.scheduleEndDate = scheduleEndDate
        scheduleModel.scheduleStartDate = scheduleStartDate
        dateRangelbl.text = startDateStr + " - " + endDateStr
    }
}




extension Array {
  mutating func remove(at indexes: [Int]) {
    for index in indexes.sorted(by: >) {
      remove(at: index)
    }
  }
}
