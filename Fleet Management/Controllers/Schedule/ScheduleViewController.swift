//
//  ScheduleViewController.swift
//  Fleet Management
//
//  Created by Atta Khan on 20/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit

class ScheduleViewController: UIViewController {

    @IBOutlet weak var upcomingBtn: UIButton!
    @IBOutlet weak var pastBtn: UIButton!
    @IBOutlet weak var pastView: UIView!
    @IBOutlet weak var upcomingView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var stackView: UIStackView!
    var scheduleStatus: String = "past"
    var scheduleItems: [ScheduleItems]?
    var pastScheduleItems: [PastScheduleItems]?
    private let refreshControl = UIRefreshControl()
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.shared.setStatusBarBackgroundColor(view: view, color: UIColor.themeColor)
        // Do any additional setup after loading the view.
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.white

        backgroundView.translatesAutoresizingMaskIntoConstraints = false

        // put background view as the most background subviews of stack view
        stackView.insertSubview(backgroundView, at: 0)

        // pin the background view edge to the stack view edge
        NSLayoutConstraint.activate([
            backgroundView.leadingAnchor.constraint(equalTo: stackView.leadingAnchor),
            backgroundView.trailingAnchor.constraint(equalTo: stackView.trailingAnchor),
            backgroundView.topAnchor.constraint(equalTo: stackView.topAnchor),
            backgroundView.bottomAnchor.constraint(equalTo: stackView.bottomAnchor)
        ])
        getPastSchedules()
        upcomingView.isHidden = true
        upcomingBtn.setTitleColor(UIColor.lightGray, for: .normal)
        refreshControl.addTarget(self, action: #selector(refreshTableViewData(_:)), for: .valueChanged)
        if #available(iOS 10, *){
          tableView.refreshControl = refreshControl
        } else {
          tableView.addSubview(refreshControl)
        }
    }
    @objc private func refreshTableViewData(_ sender: Any) {
        if scheduleStatus == "upcoming" {
            getUpcomingSchedules()
        } else {
            getPastSchedules()
        }
       
    }
    func setUpTableView() {
        tableView.register(ScheduleTableViewCell.nib, forCellReuseIdentifier: ScheduleTableViewCell.identifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    func getUpcomingSchedules() {
        let user_id = UserDefaults.standard.integer(forKey: "userId")
        let url = URL(string: GET_UPCOMING_SCHEDULES + "?UserId=\(user_id)&SkipCount=0&MaxResultCount=100")!
        showSpinner(onView: self.view)
        WebServiceManager.sharedInstance.getRequest(params: nil, url: url, serviceType: "Get Upcoming schedule", modelType: UcomingScheduleModel.self, success: { (response) in
            let responseObj = response as! UcomingScheduleModel
            self.removeSpinner()
            if responseObj.success == true {
                self.scheduleItems = responseObj.result?.items
                DispatchQueue.main.async {
                    self.setUpTableView()
                    self.refreshControl.endRefreshing()
                }
                
            } else {
                // print error msg
                DispatchQueue.main.async {
                }
            }
        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                self.refreshControl.endRefreshing()
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
    }
    
    func getPastSchedules() {
        let user_id = UserDefaults.standard.integer(forKey: "userId")
        let url = URL(string: TRIP_HISTORY + "?PassengerId=\(user_id)&SkipCount=0&MaxResultCount=100")!
        showSpinner(onView: self.view)
        WebServiceManager.sharedInstance.getRequest(params: nil, url: url, serviceType: "Get Past schedule", modelType: PastScheduleModel.self, success: { (response) in
            let responseObj = response as! PastScheduleModel
            self.removeSpinner()
            if responseObj.success == true {
                self.pastScheduleItems = responseObj.result?.items
                DispatchQueue.main.async {
                    self.setUpTableView()
                    self.refreshControl.endRefreshing()
                }
                
            } else {
                // print error msg
                 DispatchQueue.main.async {
                    self.refreshControl.endRefreshing()
                    self.showAlert(title: "Alert", message: (responseObj.error?.message)!, controller: self) {
                       
                    }
                }
            }
        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
    }
    
    
    
    
    @IBAction func tapOnUpcomingBtn(_ sender: Any) {
        pastView.isHidden = true
        pastBtn.setTitleColor(UIColor.lightGray, for: .normal)
        upcomingView.isHidden = false
        upcomingBtn.setTitleColor(UIColor.themeColor, for: .normal)
        upcomingBtn.isHighlighted = true
        scheduleStatus = "upcoming"
        if scheduleItems?.count ?? 0 == 0 {
            getUpcomingSchedules()
        } else {
            tableView.reloadData()
        }
    }
    @IBAction func tapOnPastBtn(_ sender: Any) {
        scheduleStatus = "past"
        upcomingView.isHidden = true
        upcomingBtn.setTitleColor(UIColor.lightGray, for: .normal)
        pastView.isHidden = false
        pastBtn.setTitleColor(UIColor.themeColor, for: .normal)
        
        if pastScheduleItems?.count ?? 0 == 0 {
            getPastSchedules()
        } else {
            tableView.reloadData()
        }
    }
    @IBAction func scheduleBtn(_ sender: Any) {
    }
    @IBAction func backNavigateBtn(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
}

extension ScheduleViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSections: Int = 0
        if scheduleStatus == "upcoming" {
            if scheduleItems?.count ?? 0 == 0 {
                tableView.setEmptyView(message: "No Schedule Yet", image: UIImage(named: "noSchedule")!)
            } else {
                numOfSections = 1
                tableView.restore()
            }
        } else {
            if pastScheduleItems?.count ?? 0 == 0 {
                tableView.setEmptyView(message: "No Schedule Yet", image: UIImage(named: "noSchedule")!)
            } else {
                numOfSections = 1
                tableView.restore()
            }
        }
        return numOfSections
       }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if scheduleStatus == "upcoming" {
            return scheduleItems?.count ?? 0
        } else {
            return pastScheduleItems?.count ?? 0
        }
    }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ScheduleTableViewCell.identifier) as! ScheduleTableViewCell
        if scheduleStatus == "upcoming" {
            cell.data = scheduleItems?[indexPath.row]
        } else {
            cell.past_data = pastScheduleItems?[indexPath.row]
            
        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if scheduleStatus == "upcoming" {
            let vc = storyboard?.instantiateViewController(withIdentifier: "UpComingScheduleDetailsVC") as! UpComingScheduleDetailsVC
            vc.schedule_data = scheduleItems?[indexPath.row]
            navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = storyboard?.instantiateViewController(withIdentifier: "PastScheduleVC") as! PastScheduleVC
            vc.schedule_id = pastScheduleItems?[indexPath.row].id ?? ""
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
