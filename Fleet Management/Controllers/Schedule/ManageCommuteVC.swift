//
//  ManageCommuteVC.swift
//  Fleet Management
//
//  Created by Atta khan on 13/05/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit

class ManageCommuteVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pickBtn: UIButton!
    @IBOutlet weak var bothBtn: UIButton!
    @IBOutlet weak var dropOffBtn: UIButton!
    private let refreshControl = UIRefreshControl()
    var all_schedule_data: AllScheduleResult?
    var schedule_items : [ScheduleResult]?
    let cellSpacingHeight: CGFloat = 8
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTableView()
        Utility.shared.setStatusBarBackgroundColor(view: view, color: UIColor.themeColor)
        
        refreshControl.addTarget(self, action: #selector(refreshTableViewData(_:)), for: .valueChanged)
        if #available(iOS 10, *){
          tableView.refreshControl = refreshControl
        } else {
          tableView.addSubview(refreshControl)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        getALLSchedules()
    }
    @objc private func refreshTableViewData(_ sender: Any) {
       getALLSchedules()
    }
    func setUpTableView() {
        
        tableView.register(CommuteViewCell.nib, forCellReuseIdentifier: CommuteViewCell.identifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    func getALLSchedules() {
        let user_id = UserDefaults.standard.integer(forKey: "userId")
        let url = URL(string: GET_ALL_SCHEDULE + "?UserId=\(user_id)&FutureTrips=true&PastTrips=false&Sorting=scheduleStartDate&SkipCount=0&MaxResultCount=100")!
        print(url)
        showSpinner(onView: self.view)
        WebServiceManager.sharedInstance.getRequest(params: nil, url: url, serviceType: "Get All schedule", modelType: AllScheduleModel.self, success: { (response) in
            let responseObj = response as! AllScheduleModel
            self.removeSpinner()
            if responseObj.success == true {
                if responseObj.result?.totalCount ?? 0 <= 0 {
                    DispatchQueue.main.async {
                        self.showAlert(title: "Alert", message: "Currently you have no schedule.", controller: self) {
//                            let storyboard = UIStoryboard(name: "Schedule", bundle: nil)
//                            let vc = storyboard.instantiateViewController(withIdentifier: "AddScheduleVC") as! AddScheduleVC
//                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                }
                self.all_schedule_data = responseObj.result
                self.schedule_items = self.all_schedule_data?.items
                DispatchQueue.main.async {
                    self.refreshControl.endRefreshing()
                    self.setUpTableView()
                }
                
            }
        }, fail: { (error) in
            self.removeSpinner()
            print(error)
            DispatchQueue.main.async { () -> Void in
                self.refreshControl.endRefreshing()
                self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                }
            }
        }, showHUD: true)
    }
        

    @IBAction func navigateBackBtn(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapOnPickBtn(_ sender: Any) {
        pickBtn.setTitleColor(UIColor.white, for: .normal)
        pickBtn.backgroundColor = UIColor.themeColor
        dropOffBtn.setTitleColor(UIColor.black, for: .normal)
        dropOffBtn.backgroundColor = UIColor.white
        bothBtn.setTitleColor(UIColor.black, for: .normal)
        bothBtn.backgroundColor = UIColor.white
//        schedule_items = self.all_schedule_data?.items?.filter( { $0.serviceType == ServicesType.pickup} )
//        print(schedule_items)
//        self.setUpTableView()
        UserDefaults.standard.set(ServicesType.pickup.rawValue, forKey: "serviceType")
        
    }
    @IBAction func tapOndropOffBtn(_ sender: Any) {
        dropOffBtn.setTitleColor(UIColor.white, for: .normal)
        dropOffBtn.backgroundColor = UIColor.themeColor
        pickBtn.setTitleColor(UIColor.black, for: .normal)
        pickBtn.backgroundColor = UIColor.white
        bothBtn.setTitleColor(UIColor.black, for: .normal)
        bothBtn.backgroundColor = UIColor.white
        UserDefaults.standard.set(ServicesType.dropOff.rawValue, forKey: "serviceType")
    }
    @IBAction func tapOnBothBtn(_ sender: Any) {
        bothBtn.setTitleColor(UIColor.white, for: .normal)
        bothBtn.backgroundColor = UIColor.themeColor
        pickBtn.setTitleColor(UIColor.black, for: .normal)
        pickBtn.backgroundColor = UIColor.white
        dropOffBtn.setTitleColor(UIColor.black, for: .normal)
        dropOffBtn.backgroundColor = UIColor.white
        UserDefaults.standard.set(ServicesType.both.rawValue, forKey: "serviceType")
    }
    
}
extension ManageCommuteVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if self.schedule_items?.count ?? 0 == 0 {
            tableView.setEmptyView(message: "No Schedule Yet", image: UIImage(named: "noSchedule")!)
        } else {
            tableView.restore()
        }
        return self.schedule_items?.count ?? 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }

    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CommuteViewCell.identifier) as! CommuteViewCell
        cell.data = self.schedule_items?[indexPath.section]
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let id = self.schedule_items?[indexPath.section].id {
            let storyboard = UIStoryboard(name: "Schedule", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "UpdateScheduleVC") as! UpdateScheduleVC
            vc.schedule_id  = id
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
