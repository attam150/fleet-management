//
//  UpComingScheduleDetailsVC.swift
//  Fleet Management
//
//  Created by Atta khan on 25/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit

class UpComingScheduleDetailsVC: UIViewController {
    @IBOutlet weak var routeDetailsLbl: UILabel!
    @IBOutlet weak var endAddessLbl: UILabel!
    @IBOutlet weak var startAddresslbl: UILabel!
    @IBOutlet weak var durtionTripLbl: UILabel!
    @IBOutlet weak var vechileDetailLbl: UILabel!
    @IBOutlet weak var vechileNbLbl: UILabel!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    
    var schedule_data : ScheduleItems?
    var id = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.shared.setStatusBarBackgroundColor(view: view, color: UIColor.themeColor)
        // Do any additional setup after loading the view.
        
        userImg.makeRounded()
        startAddresslbl.text = schedule_data?.trip?.route?.startingAddress
        endAddessLbl.text = schedule_data?.trip?.route?.endingAddress
        usernameLbl.text = schedule_data?.driver?.fullName
        if let make = schedule_data?.trip?.vehicle?.make, let model = schedule_data?.trip?.vehicle?.model, let registrationNumber = schedule_data?.trip?.vehicle?.registrationNumber {
            vechileDetailLbl.text = make + " " + model
            vechileNbLbl.text = registrationNumber
        } else {
            vechileDetailLbl.text = ""
            vechileNbLbl.text = ""
        }
        
        if let date = schedule_data?.tripDate?.toDateString()?.toString(format: "EEEE d MMMM yyyy"), let time = schedule_data?.tripTime?.toDateString()?.toString(format: "hh:mm a") {
           durtionTripLbl.text = date + " | " + time
        }
        
        
        
        if let routeId = schedule_data?.routeId, let tripId = schedule_data?.trip?.id {
            routeDetailsLbl.text = "Duration: 1 Hr 25 Mins | Route Id: \(routeId) | Trip Id: \(tripId)"
        }
        if let schedule_id = schedule_data?.scheduleId {
            id = schedule_id
        }
        if let driverImg = schedule_data?.driver?.profilePicture {
            let img = BASE_URL_MEDIA + driverImg
            userImg.setImage(with: img, placeholder: UIImage(named: "portraituser"))
        }
    }
    
    @IBAction func navigateBackBtn(_ sender: Any) {
         _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cancelRideBtn(_ sender: Any) {
        if let data = schedule_data {
            
            var model =  DeleteScheduleModel()
            model.passengerId = UserDefaults.standard.integer(forKey: "userId")
            model.tripId = data.trip?.id
            model.date = Date().dateAndTimetoString(format: "yyyy-MM-dd'T'hh:mm:ssZ")
            model.scheduleId = data.scheduleId
            model.type = data.trip?.type
            model.status = 3
            let encode = JSONEncoder()
            let jsonData = try! encode.encode(model)
            print(jsonData)
            
            let url = URL(string: DELETE_SCHEDULE)!
            showSpinner(onView: self.view)
            WebServiceManager.sharedInstance.postRequest(params: jsonData, url: url, serviceType: "DELETE  SCHEDULE", modelType: TripModel.self, success: { (response) in
                let responseObj = response as! TripModel
                self.removeSpinner()
                if responseObj.success == true {
                    DispatchQueue.main.async {
                        self.showAlert(title: "Alert", message: "Your Ride has been cancel successfully.", controller: self) {
                            _ = self.navigationController?.popViewController(animated: true)
                        }
                    }
                }
            }, fail: { (error) in
                self.removeSpinner()
                print(error)
                DispatchQueue.main.async { () -> Void in
                    self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                    }
                }
            }, showHUD: true)
        }
        
    }
    
    
}

struct DeleteScheduleModel: Codable {
    var passengerId: Int?
    var tripId: Int?
    var scheduleId: Int?
    var date: String?
    var type: TripType? = .PICKUP
    var status: Int = 3
}

//{
//  "passengerId": 42,
//  "tripId": 35,
//  "scheduleId": 64,
//  "date": "2020-05-06T00:00:00+05:00",
//  "type": 2,
//  "status": 3
//}
