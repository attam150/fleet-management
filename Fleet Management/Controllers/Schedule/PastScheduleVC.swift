//
//  PastScheduleVC.swift
//  Fleet Management
//
//  Created by Atta khan on 25/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit

class PastScheduleVC: UIViewController {

    @IBOutlet weak var mapImgView: UIImageView!
    @IBOutlet weak var routeDetailsLbl: UILabel!
    @IBOutlet weak var endAddessLbl: UILabel!
    @IBOutlet weak var startAddresslbl: UILabel!
    @IBOutlet weak var durtionTripLbl: UILabel!
    @IBOutlet weak var vechileDetailLbl: UILabel!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    var pastScheduleItems: PastScheduleResult?
    var schedule_id: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.shared.setStatusBarBackgroundColor(view: view, color: UIColor.themeColor)
        // Do any additional setup after loading the view.
        startAddresslbl.text = ""
        endAddessLbl.text = ""
        dateLbl.text = ""
        timeLbl.text = ""
        statusLbl.text = ""
        durtionTripLbl.text = ""
        usernameLbl.text = ""
        routeDetailsLbl.text = ""
         
        getPastSchedulesDetails()
    }
    
    
    
    
    @IBAction func navigateBackBtn(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func getPastSchedulesDetails() {
           let url = URL(string: PAST_SCHEDULE_DETAILS + "?tripExecutionPassengerId=\(schedule_id)")!
           showSpinner(onView: self.view)
           WebServiceManager.sharedInstance.getRequest(params: nil, url: url, serviceType: "Get Past schedule details", modelType: PastScheduleModel.self, success: { (response) in
               let responseObj = response as! PastScheduleModel
               self.removeSpinner()
               if responseObj.success == true {
                   self.pastScheduleItems = responseObj.result
                   
                   DispatchQueue.main.async {
                       self.updateUI()
                   }
                   
               } else {
                   // print error msg
                    DispatchQueue.main.async {
                       self.showAlert(title: "Alert", message: (responseObj.error?.message)!, controller: self) {
                          
                       }
                   }
               }
           }, fail: { (error) in
               self.removeSpinner()
               print(error)
               DispatchQueue.main.async { () -> Void in
                   self.showAlert(title: "Alert", message: error.localizedDescription, controller: self) {
                   }
               }
           }, showHUD: true)
       }
    func updateUI() {
        if let data = pastScheduleItems {
            if let pickupAddress = data.trip?.tripStartAddress, let dropAddress = data.trip?.tripEndAddress, let type = data.trip?.type {
                startAddresslbl.text = pickupAddress
                endAddessLbl.text = dropAddress
            } else {
                startAddresslbl.text = ""
                endAddessLbl.text = ""
            }
            if let driverName = data.tripExecution?.driver?.fullName {
                usernameLbl.text = driverName
            }
            if let driverImg = data.tripExecution?.driver?.profilePicture {
                let imgLink = BASE_URL_MEDIA + driverImg
                userImg.setImage(with: imgLink, placeholder: nil)
            }
            
            if let make = data.tripExecution?.driver?.vehicle?.make,let model = data.tripExecution?.driver?.vehicle?.model, let registrationNumber = data.tripExecution?.driver?.vehicle?.registrationNumber {
                vechileDetailLbl.text = make + " " + model + " " + registrationNumber
            }
            
            if let tripId = data.trip?.id,let routeId = data.trip?.routeId, let passengerCount = data.trip?.passengerCount {
                routeDetailsLbl.text = "Route Id: \(routeId) | Trip Id: \(tripId)"
            }
            if let date = data.trip?.startTime?.toDateString()?.toString(format: "EEEE, dd MMMM"), let time = data.trip?.startTime?.toDateString()?.toString(format: "hh:mm a") {
                dateLbl.text = date
                timeLbl.text = time
            }
            
            if let pickLat = data.passenger?.pickupLat, let pickLong = data.passenger?.pickupLong, let dropOffLat = data.trip?.route?.endingPointLat, let dropLong = data.trip?.route?.endingPointLong, let directionPolyline = data.passenger?.directionPolyline {
                let staticMapUrl = "https://maps.googleapis.com/maps/api/staticmap?size=600x400&path=enc:\(directionPolyline)&markers=\(pickLat),\(pickLong)|\(dropOffLat),\(dropLong)&key=\(API_KEY)"
                
                
                if let url = URL(string: staticMapUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
                    do {
                        let data = try NSData(contentsOf: url, options: NSData.ReadingOptions())
                        mapImgView.image = UIImage(data: data as Data)
                    } catch {
                        mapImgView.image = UIImage()
                    }
                }
            }
            
            if let tripStartDate = data.tripExecution?.startTime?.toDateString(), let tripEndDate = data.tripExecution?.endTime?.toDateString() {
                let secondsBetweenDates = tripEndDate.timeIntervalSince(tripStartDate)
                let secondsInAnHour: Double = 60.0
                let mintuesBetweenDates = (secondsBetweenDates / secondsInAnHour).rounded(toPlaces: 0)
                let total_minutes = Int(mintuesBetweenDates)
                durtionTripLbl.text = "\(total_minutes) Minutes"
            }
            
            if let status = data.status {
                if status == 2 {
                    statusLbl.isHidden = false
                    statusLbl.text = "Completed"
                }
            }
        }
    }

}
