//
//  ScheduleCalendarVC.swift
//  Fleet Management
//
//  Created by Atta khan on 28/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit
import FSCalendar
protocol SelectedDateRang {
    func get_selected_date(_ startDate: Date, _ lastDate: Date)
}

class ScheduleCalendarVC: UIViewController, FSCalendarDelegate {

    @IBOutlet weak var calendarView: FSCalendar!
    // first date in the range
    private var firstDate: Date?
    // last date in the range
    private var lastDate: Date?
    private var datesRange: [Date]?
    var dateArray: [Date]?
    var delegate: SelectedDateRang?
    override func viewDidLoad() {
        super.viewDidLoad()
        Utility.shared.setStatusBarBackgroundColor(view: view, color: UIColor.themeColor)
        calendarView.allowsMultipleSelection = true
        calendarView.swipeToChooseGesture.isEnabled = true
        calendarView.delegate = self
        calendarView.today = nil
        if dateArray?.count ?? 0 > 0 {
            for (_ , value) in (dateArray?.enumerated())! {
                calendarView.select(value)
                //datesRange?.append(value)
            }
            if let date = dateArray {
                datesRange = date
            }
        }
        
    }
    func datesRange(from: Date, to: Date) -> [Date] {
        // in case of the "from" date is more than "to" date,
        // it should returns an empty array:
        if from > to { return [Date]() }

        var tempDate = from
        var array = [tempDate]

        while tempDate < to {
            tempDate = Calendar.current.date(byAdding: .day, value: 1, to: tempDate)!
            array.append(tempDate)
        }

        return array
    }
    

    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        // nothing selected:
        if firstDate == nil {
            firstDate = date
            datesRange = [firstDate!]

            print("datesRange contains: \(datesRange!)")

            return
        }

        // only first date is selected:
        if firstDate != nil && lastDate == nil {
            // handle the case of if the last date is less than the first date:
            if date <= firstDate! {
                calendar.deselect(firstDate!)
                firstDate = date
                datesRange = [firstDate!]

                print("datesRange contains: \(datesRange!)")

                return
            }

            let range = datesRange(from: firstDate!, to: date)

            lastDate = range.last

            for d in range {
                calendar.select(d)
            }

            datesRange = range

            print("datesRange contains: \(datesRange!)")

            return
        }

        // both are selected:
        if firstDate != nil && lastDate != nil {
            for d in calendar.selectedDates {
                calendar.deselect(d)
            }

            lastDate = nil
            firstDate = nil

            datesRange = []

            print("datesRange contains: \(datesRange!)")
        }
    }

    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
        // both are selected:

        // NOTE: the is a REDUANDENT CODE:
        if firstDate != nil && lastDate != nil {
            for d in calendar.selectedDates {
                calendar.deselect(d)
            }

            lastDate = nil
            firstDate = nil

            datesRange = []
            print("datesRange contains: \(datesRange!)")
        }
    }
    
    

    @IBAction func navigateBackBtn(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func doneBtn(_ sender: Any) {
        if let firstDate = datesRange?.first, let lastDate = datesRange?.last {
            delegate?.get_selected_date(firstDate, lastDate)
            navigationController?.popViewController(animated: true)
        } else {
            self.showAlert(title: "Alert", message: "Please select date range.", controller: self) {
            }
             return
        }
    }
    
}
