//
//  Utility.swift
//  Fleet Management
//
//  Created by Atta Khan on 17/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//
import Foundation
import UIKit

class Utility: NSObject {
    
    static let shared = Utility()
     var rootViewContoller = UIApplication.shared.keyWindow?.rootViewController
    func setStatusBarBackgroundColor(view: UIView, color: UIColor) {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = color
            view.addSubview(statusbarView)
          
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
          
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = color
        }
    }
    func setViewCornerRadius(_ view: UIView, radius: CGFloat) {
        view.layer.cornerRadius = radius
        view.layer.masksToBounds = true
        
    }
    func emptyTableView(_ tableView: UITableView) {
        tableView.separatorStyle = .none
        var emptyScreenView = Bundle.main.loadNibNamed("EmptyListView", owner: self, options: nil)?[0] as? EmptyListView
        if let aView = emptyScreenView {
            tableView.backgroundView = aView
        }
    }
    func stopTimer() {
         print("timer stop.....")
        guard timer != nil else { return }
        timer?.invalidate()
        timer = nil
    }
    func phoneCall(_ phoneNumber: String) {
        if let phoneURL = NSURL(string: ("tel://" + phoneNumber)) {

            let alert = UIAlertController(title: ("Call to your driver: \(phoneNumber)"), message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Call", style: .default, handler: { (action) in
                UIApplication.shared.open(phoneURL as URL, options: [:], completionHandler: nil)
            }))

            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            rootViewContoller?.present(alert, animated: true, completion: nil)
        }
    }
    func sendSMS(_ phoneNumber: String) {
        let sms = "sms:\(phoneNumber)&body=Hi,"
        let url = URL(string:sms)!
        let shared = UIApplication.shared

        if(shared.canOpenURL(url)){
            shared.openURL(url)
        }else{
            print("unable to send message")
        }
        //UIApplication.shared.open(URL(string: "sms:")!, options: [:], completionHandler: nil)
    }
}
