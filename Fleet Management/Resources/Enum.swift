//
//  Enum.swift
//  Fleet Management
//
//  Created by Atta khan on 03/05/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//


enum UserRole: Int, Codable {
    case ADMIN      = 1
    case DRIVER     = 2
    case PASSENGER  = 3
}

enum TripExecutionPassengerStatus: Int, Codable {
    case INPROGRESS     =   1
    case COMPLETED      =   2
    case CANCELLED      =   3
    case NOTSTARTED     =   0
    case ARRIVED        =   4
}


enum TripExecutionDriverStatus: Int, Codable {
    case PENDING            =   1
    case CHECKIN            =   2
    case MISSED             =   3
    case DROPPED            =   4
    case PASSENGER_CANCEL   =   5
    case PASSENGER_COMPLETED_CONFIRMATION = 6
    case PASSENGER_CANCEL_CONFIRMATION = 7
    case DESTINATION_ARRIVED = 8
    case DRIVER_CANCEL = 9
    
}
enum TripType: Int, Codable {
    case UNKNOWN    =   0
    case PICKUP     =   1
    case DROP       =   2
}
enum FCMSTATUS: Int {
    case TRIPSTARTED        =   1
    case CHECKIN            =   2
    case MISSED             =   3
    case DESTINATIONARRIVED =   4
    case DRIVERARRIVED      =   5
    case CANCELCONFIRMATION =   6
    case COMPLETION_CONFRIMATION = 7
    case DriverVehicleUpdated = 8
    case DriverNewTripAssigned = 9
    case DriverTripCanceled = 10
    case PassengerScheduleApproved = 11
    case PassengerScheduleRejected = 12
    case PassengerScheduleUpdateRequest = 13
    case PassengerTripCancelledByDriver = 14
    case RequestCreated = 15
    case RequestApproved = 16
    case RequestRejected = 17
    case AdminReplied = 18
    case ChatNotification = 20

}
enum WeekDay: String {
    case Monday     =   "Monday"
    case Tuesday    =   "Tuesday"
    case Wednesday  =   "Wednesday"
    case Thursday   =   "Thursday"
    case Friday     =   "Friday"
    case Saturday   =   "Saturday"
    case Sunday     =   "Sunday"
    func getDayInNumber() -> Int {
        switch self {
        case .Monday:
            return 1
        case .Tuesday:
            return 2
        case .Wednesday:
            return 3
        case .Thursday:
            return 4
        case .Friday:
            return 5
        case .Saturday:
            return 6
        case .Sunday:
            return 7
        }
    }
    func getDayFullName() -> String {
        switch self {
        case .Monday:
            return "Monday"
        case .Tuesday:
            return "Tuesday"
        case .Wednesday:
            return "Wednesday"
        case .Thursday:
            return "Thursday"
        case .Friday:
            return "Friday"
        case .Saturday:
            return "Saturday"
        case .Sunday:
            return "Sunday"
        }
    }
    func getDayShortName() -> String {
        switch self {
        case .Monday:
            return "Mon"
        case .Tuesday:
            return "Tue"
        case .Wednesday:
            return "Wed"
        case .Thursday:
            return "Thu"
        case .Friday:
            return "Fri"
        case .Saturday:
            return "Sat"
        case .Sunday:
            return "Sun"
        }
    }
}
enum PastScheduleTripStatus: Int, Codable {
    case INPROGRESS     =   1
    case COMPLETED      =   2
    case CANCELLED      =   3
    case DRIVERCANCELLED    =   4
}
enum ScheduleStatus: Int, Codable {
    case PENDING = 1
    case APPROVED = 2
}


enum RequestStatus: Int, Codable {
    case PENDING = 1
    case COMPLETED = 2
    case WITHDRAWNBYUSER = 3
    case REJECTED = 4
}

enum RequestCategory: Int, Codable {
    case Passenger = 1
    case Driver = 2
    case Schedule = 3
    case Trip = 4
}

enum RequestSubCategory: Int, Codable{
    case PassengerPickupLocationChange = 1 // Location Change Request (Pickup & Dropoff)
    case PassengerRouteChange = 2 // Route Change Request
    case PassengerRouteAndAddressChange = 3 // Location & Route Change Request
    case ScheduleUpdate = 4
    case ScheduleServiceTypeUpdate = 5
}

enum ServicesType: Int, Codable {
    case byDefault = 0
    case both = 3
    case dropOff = 2
    case pickup = 1
}
