//
//  WebServiceManager.swift
//  Fleet Management
//
//  Created by Atta Khan on 06/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit
import Alamofire

class WebServiceManager: NSObject {
    static var serviceCount = 0
    static let sharedInstance = WebServiceManager()
    func setHeader() -> HTTPHeaders {
        let token =  UserDefaults.standard.string(forKey: "token")
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(token!)",
            "Content-Type": "application/json"
        ]
        return headers
    }
    
    func fetchToken()->String{
        let token =  UserDefaults.standard.string(forKey: "token")
        return token!
    }
    
    func getRequest<D: Codable>(params: Dictionary<String, AnyObject>?, url: URL, serviceType: String, modelType: D.Type, success: @escaping ( _ servicResponse: D) -> Void, fail: @escaping ( _ error: NSError) -> Void, showHUD: Bool) {
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        //request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        if let token =  UserDefaults.standard.string(forKey: "token") {
            request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        }
        let dataTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
               print(error)
               fail(error as NSError)
           }
           
           guard let data = data else {
               //handle(.failure(.init(message: "Something went wrong")))
               return
           }
           
           let dataString = String(data: data, encoding: .utf8)
           print(dataString)
           guard let object = try? JSONDecoder().decode(D.self, from: data) else {
               //handle(.failure(.init(message: "Could not parse data")))
               return
           }
           
           success(object)
       }
       
       dataTask.resume()
        
    }
    
    func deleteRequest<D: Codable>(params: Data?, url: URL, serviceType: String, modelType: D.Type, success: @escaping ( _ servicResponse: D) -> Void, fail: @escaping ( _ error: NSError) -> Void, showHUD: Bool) {
            var request = URLRequest(url: url)
            request.httpMethod = "DELETE"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = params
            if let token =  UserDefaults.standard.string(forKey: "token") {
                request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            }
            let dataTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
                if let error = error {
                    print(error)
                    fail(error as NSError)
                    //handle(.failure(.init(message: "Your Network appears to be offline")), 1)
                    
               }
               
               guard let data = data else {
                   //handle(.failure(.init(message: "Something went wrong")))
                   return
               }
               
               let dataString = String(data: data, encoding: .utf8)
               print(dataString)
               guard let object = try? JSONDecoder().decode(D.self, from: data) else {
                   //handle(.failure(.init(message: "Could not parse data")))
                print("here")
                   return
               }
               
               success(object)
           }
           
           dataTask.resume()
            
        }
    
    func postRequest<D: Codable>(params: Data?, url: URL, serviceType: String, modelType: D.Type, success: @escaping ( _ servicResponse: D) -> Void, fail: @escaping ( _ error: NSError) -> Void, showHUD: Bool) {
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        if let token =  UserDefaults.standard.string(forKey: "token") {
            request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        }
        request.httpBody = params
        let dataTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print(error)
                fail(error as NSError)
                
           }
           guard let data = data else {
               return
           }
           
           let dataString = String(data: data, encoding: .utf8)
           print(dataString)
           guard let object = try? JSONDecoder().decode(D.self, from: data) else {
               //handle(.failure(.init(message: "Could not parse data")))
            print("here")
               return
           }
           
           success(object)
       }
       
       dataTask.resume()
        
    }
    
   func putRequest<D: Codable>(params: Data, url: URL, serviceType: String, modelType: D.Type, success: @escaping ( _ servicResponse: D) -> Void, fail: @escaping ( _ error: NSError) -> Void, showHUD: Bool) {
           var request = URLRequest(url: url)
           request.httpMethod = "PUT"
           request.setValue("application/json", forHTTPHeaderField: "Content-Type")
           if let token =  UserDefaults.standard.string(forKey: "token") {
               request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
           }
           request.httpBody = params
           let dataTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
               if let error = error {
                  print(error)
                fail(error as NSError)
              }
              
              guard let data = data else {
                  return
              }
              let dataString = String(data: data, encoding: .utf8)
              guard let object = try? JSONDecoder().decode(D.self, from: data) else {
                  return
              }
              success(object)
          }
          dataTask.resume()
       }
    
    
    
    
    func showNetworkIndicator() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        WebServiceManager.serviceCount += 1
    }
    
    func hideNetworkIndicator() {
        WebServiceManager.serviceCount -= 1
        if WebServiceManager.serviceCount == 0 {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
}

class Connectivity {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
