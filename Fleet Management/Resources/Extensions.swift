//
//  Extension.swift
//  AlMeezan
//
//  Created by Atta khan on 30/08/2019.
//  Copyright © 2019 Atta khan. All rights reserved.
//

import UIKit
import PKRevealController

extension UITableView {
    func registerCells(_ cells: [UITableViewCell.Type]) {
        cells.forEach({ register(UINib(nibName: String(describing: $0), bundle: nil), forCellReuseIdentifier: String(describing: $0)) })
    }
    
    func registerHeaderFooter(_ headerFooter: [UITableViewHeaderFooterView.Type]) {
        headerFooter.forEach({ register(UINib(nibName: String(describing: $0), bundle: nil), forHeaderFooterViewReuseIdentifier: String(describing: $0)) })
    }
    
    func dequeueReusableCell<T: UITableViewCell>(with type: T.Type, for indexPath: IndexPath) -> T {
        let cell = dequeueReusableCell(withIdentifier: String(describing: type), for: indexPath) as! T
        return cell
    }
    
    func dequeueReusableHeaderFooterView<T: UITableViewHeaderFooterView>(with type: T.Type) -> T? {
        return dequeueReusableHeaderFooterView(withIdentifier: String(describing: type)) as? T
    }
    func setEmptyView(message: String, image: UIImage) {
        let emptyView = UIView(frame: CGRect(x: self.center.x, y: self.center.y, width: self.bounds.size.width, height: self.bounds.size.height))
        let titleLabel = UILabel()
        let messageLabel = UILabel()
        let imageView = UIImageView()
        imageView.image = image
        imageView.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        
        messageLabel.textColor = UIColor.lightGray
        messageLabel.font = UIFont(name: "HelveticaNeue-Regular", size: 17)
        emptyView.addSubview(imageView)
        emptyView.addSubview(messageLabel)
        imageView.centerYAnchor.constraint(equalTo: emptyView.centerYAnchor).isActive = true
        imageView.centerXAnchor.constraint(equalTo: emptyView.centerXAnchor).isActive = true
        messageLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 20).isActive = true
        messageLabel.leftAnchor.constraint(equalTo: emptyView.leftAnchor, constant: 20).isActive = true
        messageLabel.rightAnchor.constraint(equalTo: emptyView.rightAnchor, constant: -20).isActive = true
        messageLabel.text = message
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        // The only tricky part is here:
        self.backgroundView = emptyView
        self.separatorStyle = .none
    }
    func restore() {
        self.backgroundView = nil
    }
    
    
    
}

extension UIViewController {
    //open location settings for app
    func openSettingApp() {
        let alertController = UIAlertController (title: "Fleet Management", message:"Please enable location services to continue using the app." , preferredStyle: .alert)

        let settingsAction = UIAlertAction(title: NSLocalizedString("Settings", comment: ""), style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }

            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(settingsAction)
//        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .default, handler: nil)
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .default) { (action) in
            self.minimizeOrKillApp()
        }
        alertController.addAction(cancelAction)

        present(alertController, animated: true, completion: nil)
    }
    func minimizeOrKillApp(){
        UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
        //Comment if you want to minimise app
        Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false) { (timer) in
            exit(0)
        }
    }
    
    func showNotification(with message: String?, viewss : UIView ,  autoDismiss: Bool = true,   handler: (() -> ())? = nil) {
        let notificationLabel = UILabel()
        notificationLabel.translatesAutoresizingMaskIntoConstraints = false
        notificationLabel.text = message
        notificationLabel.textColor = .black
        notificationLabel.textAlignment = .center
        notificationLabel.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        notificationLabel.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        notificationLabel.numberOfLines = 0
        
        guard let navigationController = navigationController else { return }
        
        //        let subviews = navigationController.navigationBar.subviews.filter({ $0 is UILabel })
        
        let subviews = viewss.subviews.filter({$0 is UILabel})
        
        //        navigationController.navigationBar.insertSubview(notificationLabel, at: subviews.count > 0 ? subviews.count : 0)
        self.view.insertSubview(notificationLabel, at: subviews.count > 0 ? subviews.count : 0)
        notificationLabel.centerXAnchor.constraint(equalTo: viewss.centerXAnchor).isActive = true
        notificationLabel.topAnchor.constraint(equalTo: viewss.bottomAnchor).isActive = true
        notificationLabel.widthAnchor.constraint(equalTo: viewss.widthAnchor).isActive = true
        notificationLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 32).isActive = true
        
        notificationLabel.sizeToFit()
        
        notificationLabel.transform = CGAffineTransform(translationX: 0, y: -notificationLabel.frame.height)
        
        UIView.animate(withDuration: 0.25) {
            notificationLabel.transform = .identity
        }
        
        if autoDismiss {
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                UIView.animate(withDuration: 0.25, animations: {
                    notificationLabel.alpha = 0
                    notificationLabel.transform = CGAffineTransform(translationX: 0, y: -notificationLabel.frame.height)
                }, completion: { (done) in
                    notificationLabel.removeFromSuperview()
                    handler?()
                })
            }
        }
    }
    
    func showAlert(title: String, message: String, controller: UIViewController?, dismissCompletion:@escaping (AlertViewDismissHandler)) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        //        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        //        alert.addAction(action)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action -> Void in
            //Do some other stuff
            dismissCompletion()
        }))
        if controller != nil {
            controller?.present(alert, animated: true, completion: nil)
        }else {
            present(alert, animated: true, completion: nil)
        }
        
        
    }
    
    func showConfirmationAlertViewWithTitle(title:String,message : String, dismissCompletion:@escaping (AlertViewDismissHandler))
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "NO", style: .cancel, handler: { action -> Void in
            //Do some other stuff
            
        }))
        alertController.addAction(UIAlertAction(title: "YES", style: .default, handler: { action -> Void in
            //Do some other stuff
            dismissCompletion()
        }))
        
        
        present(alertController, animated: true, completion:nil)
    }
    
    
    @IBAction func popController(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func hideNavigationBar(){
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: true)

    }

    func showNavigationBar() {
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        ai.color = UIColor.themeColor
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    func showLoaderonView(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
               spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
               let ai = UIActivityIndicatorView.init(style: .whiteLarge)
               ai.startAnimating()
               ai.center = spinnerView.center
               ai.color = UIColor.themeColor
               spinnerView.addSubview(ai)
               onView.addSubview(spinnerView)
               vSpinner = spinnerView
    }
    
    func removeLoader() {
        vSpinner?.removeFromSuperview()
        vSpinner = nil
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            vSpinner?.removeFromSuperview()
            vSpinner = nil
        }
    }

    
}
extension Data {
    mutating func append(_ string: String) {
        if let data = string.data(using: .utf8) {
            append(data)
        }
    }
}

extension UITextView :UITextViewDelegate{
    
    /// Resize the placeholder when the UITextView bounds change
    override open var bounds: CGRect {
        didSet {
            self.resizePlaceholder()
        }
    }
    
    /// The UITextView placeholder text
    public var placeholder: String? {
        get {
            var placeholderText: String?
            
            if let placeholderLabel = self.viewWithTag(100) as? UILabel {
                placeholderText = placeholderLabel.text
            }
            
            return placeholderText
        }
        set {
            if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
                placeholderLabel.text = newValue
                placeholderLabel.sizeToFit()
            } else {
                self.addPlaceholder(newValue!)
            }
        }
    }
    
    /// When the UITextView did change, show or hide the label based on if the UITextView is empty or not
    ///
    /// - Parameter textView: The UITextView that got updated
    public func textViewDidChange(_ textView: UITextView) {
        if let placeholderLabel = self.viewWithTag(100) as? UILabel {
            placeholderLabel.isHidden = self.text.count > 0
        }
    }
    
    /// Resize the placeholder UILabel to make sure it's in the same position as the UITextView text
    private func resizePlaceholder() {
        if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
            let labelX = self.textContainer.lineFragmentPadding
            let labelY = self.textContainerInset.top - 2
            let labelWidth = self.frame.width - (labelX * 2)
            let labelHeight = placeholderLabel.frame.height
            
            placeholderLabel.frame = CGRect(x: labelX, y: labelY, width: labelWidth, height: labelHeight)
        }
    }
    
    /// Adds a placeholder UILabel to this UITextView
    private func addPlaceholder(_ placeholderText: String) {
        let placeholderLabel = UILabel()
        
        placeholderLabel.text = placeholderText
        placeholderLabel.sizeToFit()
        
        placeholderLabel.font = self.font
        placeholderLabel.textColor = UIColor.lightGray
        placeholderLabel.tag = 100
        
        placeholderLabel.isHidden = self.text.count > 0
        
        self.addSubview(placeholderLabel)
        self.resizePlaceholder()
        self.delegate = self
    }
}
extension UIImage {
    func withBackground(color: UIColor, opaque: Bool = true) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, opaque, scale)

        guard let ctx = UIGraphicsGetCurrentContext() else { return self }
        defer { UIGraphicsEndImageContext() }

        let rect = CGRect(origin: .zero, size: size)
        ctx.setFillColor(color.cgColor)
        ctx.fill(rect)
        ctx.concatenate(CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: size.height))
        ctx.draw(cgImage!, in: rect)

        return UIGraphicsGetImageFromCurrentImageContext() ?? self
    }
}
extension UIImageView {
    func makeRounded() {
        self.layer.borderWidth = 1
        self.layer.masksToBounds = false
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
    func setImage(with urlString: String?, placeholder: UIImage? = nil) {
        
        let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        activityIndicator.backgroundColor = UIColor(red:0.16, green:0.17, blue:0.21, alpha:1)
        activityIndicator.layer.cornerRadius = 6
        //        activityIndicator.center = placeholder.center
        activityIndicator.hidesWhenStopped = true
        //        activityIndicator.activityIndicatorViewStyle = .whiteLarge
        activityIndicator.startAnimating()
        
        self.image = placeholder
        guard let urlString = urlString, let url = URL(string: urlString) else { return }
        image = nil
        //        showLoading()
        
        let cache = URLCache.shared
        cache.removeAllCachedResponses()
        let request = URLRequest(url: url)
        
        if let data = cache.cachedResponse(for: request)?.data, let image = UIImage(data: data) {
            //            hideLoading()
            self.image = image
        }
        else {
            let session = URLSession(configuration: URLSessionConfiguration.default)
            
            let dataTask = session.dataTask(with: request) { [weak self] (data, response, error) in
                guard let this = self else { return }
                if let response = response, let data = data {
                    let image = UIImage(data: data)
                    
                    let cachedData = CachedURLResponse(response: response, data: data)
                    cache.storeCachedResponse(cachedData, for: request)
                    DispatchQueue.main.async {
                        
                        //                        this.hideLoading()
                        this.image = image
                    }
                } else {
                    DispatchQueue.main.async {
                        
                        //                        this.hideLoading()
                        this.image = placeholder
                    }
                }
            }
            dataTask.resume()
        }
    }
    
}
extension UIColor {
    static var placeholderGray: UIColor {
        return UIColor(red: 0, green: 0, blue: 0.0980392, alpha: 0.22)
    }
    static var placeholderErrorColor: UIColor {
        return UIColor.red
    }
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    static let pNavigationBarColor  =   UIColor(red: 138.0/255.0, green: 38.0/255.0, blue: 155.0/255.0, alpha: 1.0)
    static let pSelectedBarColor    =   UIColor.init(rgb: 0xb332c9).withAlphaComponent(0.1)
    static let menuLblColor         =   UIColor.init(rgb: 0x5B5F78)
    static let themeLblColor        =   UIColor(red: 17.0/255.0, green: 17.0/255.0, blue: 17.0/255.0, alpha: 1.0)
    static let themeColor           =   UIColor(red: 42.0/255.0, green: 127/255.0, blue: 246.0/255.0, alpha: 1.0)
    static let colorOne             =   UIColor(red: 114.0/255.0, green: 31.0/255.0, blue: 129.0/255.0, alpha: 1.0)
    static let colorTwo             =   UIColor(red: 207.0/255.0, green: 92.0/255.0, blue: 228.0/255.0, alpha: 1.0)
    static let placeHolderColor     =   UIColor(red: 216.0/255.0, green: 216.0/255.0, blue: 216.0/255.0, alpha: 1.0)
    static let statusColor      = UIColor.init(rgb: 0xF4F6FA)
    
//    static var placeHolderColor: UIColor {
//        return UIColor(red: 0, green: 0, blue: 0.0980392, alpha: 0.22)
//    }
    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat ) -> UIColor{
        return UIColor(red: red / 255.0, green: green / 255.0, blue: blue / 255.0, alpha: alpha)
    }
    
}
extension UIView{
    func anchor(top: NSLayoutYAxisAnchor?, paddingTop: CGFloat, bottom: NSLayoutYAxisAnchor?, paddingBottom: CGFloat, left: NSLayoutXAxisAnchor?, paddingLeft: CGFloat, right: NSLayoutXAxisAnchor?, paddingRight: CGFloat, width: CGFloat, height: CGFloat) {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: paddingTop).isActive = true
        }
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: -paddingBottom).isActive = true
        }
        if let right = right {
            rightAnchor.constraint(equalTo: right, constant: -paddingRight).isActive = true
        }
        if let left = left {
            leftAnchor.constraint(equalTo: left, constant: paddingLeft).isActive = true
        }
        if width != 0 {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        if height != 0 {
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
    }
    func roundCorners(_ corners: CACornerMask, radius: CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
        self.layer.maskedCorners = corners
        self.layer.cornerRadius = radius
//        self.layer.borderWidth = borderWidth
//        self.layer.borderColor = borderColor.cgColor
        //self.layer.applySketchShadow(color: .lightGray, alpha: 0.5, x: 0, y: 0, blur: 16, spread: 1)
    }
    
    
    
    func rounCornersWithGradientBackgroud(_ corners: CACornerMask, radius: CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
        self.layer.maskedCorners = corners
        self.layer.cornerRadius = radius
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
        self.layer.applySketchShadow(color: .lightGray, alpha: 0.5, x: 0, y: 0, blur: 16, spread: 1)
        self.layer.setGradientBackground(corners, radius: radius, colorOne: UIColor.themeColor, colorTwo: UIColor.themeColor)
    }
    
    func dropShadowAllSides(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 0, scale: Bool = true) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = offSet
        self.layer.shadowRadius = radius
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
   
    
//    gradientLayer.frame = self.view6.bounds
    //        let color1 = UIColor.yellow.cgColor
    //        let color2 = UIColor(red: 1.0, green: 0, blue: 0, alpha: 1.0).cgColor
    //        let color3 = UIColor.clear.cgColor
    //        let color4 = UIColor(white: 0.0, alpha: 0.7).cgColor
    //        gradientLayer.colors = [color1, color2, color3, color4]
    //        gradientLayer.locations = [0.0, 0.25, 0.75, 1.0]
    //        self.view6.layer.addSublayer(gradientLayer)
    
    
    func animShow(){
        UIView.animate(withDuration: 2, delay: 0, options: [.curveEaseIn],
                       animations: {
                        //self.center.y -= self.bounds.height
                        self.layoutIfNeeded()
        }, completion: nil)
        self.isHidden = false
    }
    func animHide(){
        UIView.animate(withDuration: 2, delay: 0, options: [.curveLinear],
                       animations: {
                        //self.center.y += self.bounds.height
                        self.layoutIfNeeded()

        },  completion: {(_ completed: Bool) -> Void in
        self.isHidden = true
            })
    }
    
}


extension CALayer{
    
    func removeShadow() {
        shadowOffset = .zero
        shadowColor = UIColor.clear.cgColor
        cornerRadius = 0
        shadowRadius = 0
        shadowOpacity = 0
    }
    func setGradientBackground(_ corners: CACornerMask, radius: CGFloat, colorOne: UIColor, colorTwo: UIColor) {
        removeShadow()
        cornerRadius = radius
        maskedCorners = corners
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.colors = [colorOne.cgColor, colorTwo.cgColor]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.startPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.0)
        insertSublayer(gradientLayer, at: 0)
    }
    
    
    func applySketchShadow(color: UIColor = .black, alpha: Float = 0.5, x: CGFloat = 0, y: CGFloat = 3, blur: CGFloat = 16, spread: CGFloat = 0) {
        //removeShadow()
        shadowColor = color.cgColor
        shadowOpacity = alpha
        shadowOffset = CGSize(width: x, height: y)
        shadowRadius = blur / 2.0
        if spread == 0 {
            shadowPath = nil
            
        } else {
            let dx = -spread
            let rect = bounds.insetBy(dx: dx, dy: dx)
            shadowPath = UIBezierPath(rect: rect).cgPath
        }
        
    }
}
extension String {
    var isValidEmail: Bool {
        return NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}").evaluate(with: self)
    }
    func stringAt(_ i: Int) -> String {
        return String(Array(self)[i])
    }

    func charAt(_ i: Int) -> Character {
        return Array(self)[i]
    }

    var length: Int {
        return count
    }

    subscript (i: Int) -> String {
        return self[i ..< i + 1]
    }

    func substring(fromIndex: Int) -> String {
        return self[min(fromIndex, length) ..< length]
    }

    func substring(toIndex: Int) -> String {
        return self[0 ..< max(0, toIndex)]
    }

    subscript (r: Range<Int>) -> String {
        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
        upper: min(length, max(0, r.upperBound))))
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        return String(self[start ..< end])
    }
    
    func validateMobileNumber() -> Bool {
        var validateNumber: Bool = false
        var menuImageArray: [String] = [String]()
        var list: [String] = ["331", "332", "333", "334", "335", "336", "337", "338","300", "301", "302", "303", "304", "305", "306", "307","320", "321", "322", "323", "324", "325", "326", "327", "328","341", "342", "343", "344", "348", "345", "346", "347", "347", "348","310", "311", "312", "313", "314", "315", "316", "317"]
        if self.count == 11 {
            if self[0] == "0" {
                var code = self[1..<4]
                validateNumber = list.contains(code)
            } else {
                validateNumber = false
            }
        } else if self.count == 13 {
            if self[0] == "9" && self[1] == "2" {
                var code = self[2..<6]
                validateNumber = list.contains(code)
            } else {
               validateNumber = false
            }
        }
        
        return validateNumber
        
    }
    
//    func isValidEmail() -> Bool {
//        // here, `try!` will always succeed because the pattern is valid
//        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
//        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
//    }
//    
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + self.lowercased().dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    //yyyy-MM-dd'T'HH:mm:ss.SSSZ
    func toDate(withFormat format: String = "yyyy-MM-dd'T'HH:mm:ssZ")-> Date?{
        let dateFormatter = DateFormatter()
//        dateFormatter.locale = Locale(identifier: "fa-IR")
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: self)   
        return date
    }
    func toDateString()-> Date?{
        let dateFormatter = DateFormatter()
        if let myDate = dateFormatter.dateFromMultipleFormats(fromString: self) {
            print("success!")
            return myDate
        } else {
            return nil
        }
    }
    var htmlDecoded: String {
        let decoded = try? NSAttributedString(data: Data(utf8), options: [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
            ], documentAttributes: nil).string
        
        return decoded ?? self
    }
    func numberFormatter() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.minimumFractionDigits = 2
        if let intValue = Int(self) {
            return numberFormatter.string(from: NSNumber(value: intValue)) ?? "0"
        }
        if let value = Double(self){
            let number = numberFormatter.string(from: NSNumber(value: value)) ?? "0.0"
            return number
        }
        return ""
    }

    func toCurrencyFormat(withFraction fraction: Bool) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.roundingMode = .down
        numberFormatter.numberStyle = .decimal
        if fraction {
            numberFormatter.minimumFractionDigits = 2
        }
        if let intValue = Int(self) {
            return numberFormatter.string(from: NSNumber(value: intValue)) ?? "0"
        }
        if let value = Double(self){
            let number = numberFormatter.string(from: NSNumber(value: value)) ?? "0.0"
            return number
        }
      return ""
    }
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
}
extension Int {
    func format(f: String) -> String {
        return String(format: "%\(f)d", self)
    }
}
extension Float {
    
    var kmFormatted: String {
        if self >= 10000, self <= 999999 {
            return String(format: "%.1fk", locale: Locale.current, self/1000).replacingOccurrences(of: ".0", with: "")
        }
        if self >= 999999, self <= 99999999{
            return String(format: "%.1fM", locale: Locale.current, self/1000000).replacingOccurrences(of: ".0", with: "")
        }
        if self >= 99999999 {
            return String(format: "%.1fB", locale: Locale.current, self/1000000000).replacingOccurrences(of: ".0", with: "")
        }
        return String(format: "%.0f", locale: Locale.current, self)
    }
}
extension Double {
    
    mutating func roundToPlaces(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return Darwin.round(self * divisor) / divisor
    }
    
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }

    func format(f: String) -> String {
        return String(format: "%\(f)f", self)
    }
    var kmFormatted: String {
        
        if self >= 10000, self <= 999999 {
            return String(format: "%.2fk", locale: Locale.current, self/1000).replacingOccurrences(of: ".0", with: "")
        }
        if self >= 999999, self <= 999999999 {
            return String(format: "%.2fM", locale: Locale.current, self/1000000).replacingOccurrences(of: ".0", with: "")
        }
        if self >= 999999999 {
            return String(format: "%.2fB", locale: Locale.current, self/1000000000).replacingOccurrences(of: ".0", with: "")
        }
        return String(format: "%.2f", locale: Locale.current, self)
    }
    
}
extension DateFormatter {

    func dateFromMultipleFormats(fromString dateString: String) -> Date? {
        var formats: [String] = [
        "yyyy-MM-dd'T'HH:mm:sssZ",
        "yyyy-MM-dd",
        "yyyy-MM-dd'T'HH:mm:ssZ",
        "yyyy-MM-dd hh:mm:ss",
        "dd-MM-yyyy"
        ]
    for format in formats {
        self.dateFormat = format
        if let date = self.date(from: dateString) {
                return date
            }
        }
        return nil
    }
}
extension Date {
    
    func toString(format: String = "yyyy-MM-dd") -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
    func dateAndTimetoString(format: String = "yyyy-MM-dd HH:mm") -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
    func timeIn24HourFormat() -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .none
        formatter.dateFormat = "HH:mm"
        return formatter.string(from: self)
    }
    
    func startOfMonth() -> Date {
        var components = Calendar.current.dateComponents([.year,.month], from: self)
        components.day = 1
        let firstDateOfMonth: Date = Calendar.current.date(from: components)!
        return firstDateOfMonth
    }
    
    func endOfMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
    }
    
    func nextDate() -> Date {
        let nextDate = Calendar.current.date(byAdding: .day, value: 1, to: self)
        return nextDate ?? Date()
    }
    
    func previousDate() -> Date {
        let previousDate = Calendar.current.date(byAdding: .day, value: -1, to: self)
        return previousDate ?? Date()
    }
    
    func addMonths(numberOfMonths: Int) -> Date {
        let endDate = Calendar.current.date(byAdding: .month, value: numberOfMonths, to: self)
        return endDate ?? Date()
    }
    
    func removeMonths(numberOfMonths: Int) -> Date {
        let endDate = Calendar.current.date(byAdding: .month, value: -numberOfMonths, to: self)
        return endDate ?? Date()
    }
    
    func removeYears(numberOfYears: Int) -> Date {
        let endDate = Calendar.current.date(byAdding: .year, value: -numberOfYears, to: self)
        return endDate ?? Date()
    }
    
    func getHumanReadableDayString() -> String {
        let weekdays = [
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday"
        ]
        
        let calendar = Calendar.current.component(.weekday, from: self)
        return weekdays[calendar - 1]
    }
    func timeSinceDate(fromDate: Date) -> String {
        let earliest = self < fromDate ? self  : fromDate
        let latest = (earliest == self) ? fromDate : self
        
        let components:DateComponents = Calendar.current.dateComponents([.minute,.hour,.day,.weekOfYear,.month,.year,.second], from: earliest, to: latest)
        let year = components.year  ?? 0
        let month = components.month  ?? 0
        let week = components.weekOfYear  ?? 0
        let day = components.day ?? 0
        let hours = components.hour ?? 0
        let minutes = components.minute ?? 0
        let seconds = components.second ?? 0
        
        
        if year >= 2{
            return "\(year) years ago"
        }else if (year >= 1){
            return "1 year ago"
        }else if (month >= 2) {
            return "\(month) months ago"
        }else if (month >= 1) {
            return "1 month ago"
        }else  if (week >= 2) {
            return "\(week) weeks ago"
        } else if (week >= 1){
            return "1 week ago"
        } else if (day >= 2) {
            return "\(day) days ago"
        } else if (day >= 1){
            return "1 day ago"
        } else if (hours >= 2) {
            return "\(hours) hours ago"
        } else if (hours >= 1){
            return "1 hour ago"
        } else if (minutes >= 2) {
            return "\(minutes) minutes ago"
        } else if (minutes >= 1){
            return "1 minute ago"
        } else if (seconds >= 3) {
            return "\(seconds) seconds ago"
        } else {
            return "Just now"
        }
        
    }
    
    func interval(ofComponent comp: Calendar.Component, fromDate date: Date) -> Int {

        let currentCalendar = Calendar.current
        guard let start = currentCalendar.ordinality(of: comp, in: .era, for: date) else { return 0 }
        guard let end = currentCalendar.ordinality(of: comp, in: .era, for: self) else { return 0 }

        return end - start
    }
    
    static func dates(from fromDate: Date, to toDate: Date) -> [Date] {
        var dates: [Date] = []
        var date = fromDate

        while date <= toDate {
            dates.append(date)
            guard let newDate = Calendar.current.date(byAdding: .day, value: 1, to: date) else { break }
            date = newDate
        }
        return dates
    }
}
extension UILabel {
    func adjustsFontSizeForLbl() {
        minimumScaleFactor = 0.5    //or whatever suits your need
        adjustsFontSizeToFitWidth = true
        numberOfLines = 1
    }
    var substituteFontName : String {
        get { return self.font.fontName }
        set { self.font = UIFont(name: newValue, size: self.font.pointSize) }
    }
    
}
extension UISegmentedControl {
    
    func defaultConfiguration(font: UIFont = UIFont.systemFont(ofSize: 11), color: UIColor = UIColor.black) {
        let defaultAttributes = [
            NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue): font,
            NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): color
        ]
        setTitleTextAttributes(defaultAttributes, for: .normal)
    }
    
    func selectedConfiguration(font: UIFont = UIFont.boldSystemFont(ofSize: 11), color: UIColor = UIColor.white) {
        let selectedAttributes = [
            NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue): font,
            NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): color
        ]
        setTitleTextAttributes(selectedAttributes, for: .selected)
        if #available(iOS 13.0, *) {
            selectedSegmentTintColor = UIColor.themeColor
        } else {
            // Fallback on earlier versions
            tintColor = UIColor.themeColor
        }
    }
}

extension UITextField {
    func findLocaleByCurrencyCode(_ currencyCode: String) -> Locale? {
        let locales = Locale.availableIdentifiers
        var locale: Locale?
        for   localeId in locales {
            locale = Locale(identifier: localeId)
            if let code = (locale! as NSLocale).object(forKey: NSLocale.Key.currencyCode) as? String {
                if code == currencyCode {
                    return locale
                }
            }
        }
        return locale
    }
    
}
extension Notification.Name {
    static let notificationReceived = Notification.Name("REMOTE_NOTIFICATION_RECEIVED")
    static let appTimeout = Notification.Name("AppTimout")
    static let sessionExpire = Notification.Name("SESSION_EXPIRE")
    static let updateUserProfile = Notification.Name("UPDATE_USER_PROFILE")
    static let udpateUserImg = Notification.Name("UPDATE_USER_IMAGE")
}
extension Sequence where Iterator.Element: Equatable {
    func unique() -> [Iterator.Element] {
        return reduce([], { collection, element in collection.contains(element) ? collection : collection + [element] })
    }
    func uniqueList() -> [Iterator] {
        return reduce([], { collection, element in collection.contains(element) ? collection : collection + [element] }) as! [Self.Iterator]
    }
}

extension Array {
    
    func unique<T:Hashable>(by: ((Element) -> (T)))  -> [Element] {
        var set = Set<T>() //the unique list kept in a Set for fast retrieval
        var arrayOrdered = [Element]() //keeping the unique list of elements but ordered
        for value in self {
            if !set.contains(by(value)) {
                set.insert(by(value))
                arrayOrdered.append(value)
            }
        }
        return arrayOrdered
    }
}
extension Collection where Element: Hashable {
    var orderedSet: [Element] {
        var set: Set<Element> = []
        return reduce(into: []){ set.insert($1).inserted ? $0.append($1) : ()  }
    }
}
extension UIWindow {
    /// Returns the currently visible view controller if any reachable within the window.
    public var visibleViewController: UIViewController? {
        return UIWindow.visibleViewController(from: rootViewController)
    }

    /// Recursively follows navigation controllers, tab bar controllers and modal presented view controllers starting
    /// from the given view controller to find the currently visible view controller.
    ///
    /// - Parameters:
    ///   - viewController: The view controller to start the recursive search from.
    /// - Returns: The view controller that is most probably visible on screen right now.
    public static func visibleViewController(from viewController: UIViewController?) -> UIViewController? {
        switch viewController {
        case let navigationController as UINavigationController:
            return UIWindow.visibleViewController(from: navigationController.visibleViewController ?? navigationController.topViewController)

        case let tabBarController as UITabBarController:
            return UIWindow.visibleViewController(from: tabBarController.selectedViewController)

        case let presentingViewController where viewController?.presentedViewController != nil:
            return UIWindow.visibleViewController(from: presentingViewController?.presentedViewController)

        default:
            return viewController
        }
    }
}
	
