//
//  Constant.swift
//  Fleet Management
//
//  Created by Atta Khan on 06/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//
import Foundation
import UIKit
import Firebase
import FirebaseFirestore

let SCREEN_WIDTH = UIScreen.main.bounds.size.width
let SCREEN_HEGHT = UIScreen.main.bounds.size.height
let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
let homeStoryboard = UIStoryboard(name: "Home", bundle: nil)

var vSpinner : UIView?
var timer : Timer? = nil {
    willSet {
        timer?.invalidate()
    }
}
let API_KEY = "AIzaSyDa1_jT5uMTPoRxuXqbFHvCY395LynHEtM"

//let BASE_URL_MEDIA = "https://fmapi.360scrm.com/"
//let BASE_URL = "https://fmapi.360scrm.com/api/"


// dev url
let BASE_URL_MEDIA = "https://fmapistaging.360scrm.com/"
let BASE_URL = "https://fmapistaging.360scrm.com/api/"

let SERVICES_END_POINT = "services/app/"

let USER_LOGIN          =   BASE_URL + "TokenAuth/Authenticate"
let REGISTER            =   BASE_URL + SERVICES_END_POINT + "Account/Register"
let GET_ROUTES_CITY     =   BASE_URL + SERVICES_END_POINT + "Route/GetAll"
let GET_ROUTES_RADIUS   =   BASE_URL + SERVICES_END_POINT + "Route/GetRouteInRadius"
let GET_OFFICE_BUILDING =   BASE_URL + SERVICES_END_POINT + "OfficeBuilding/GetAll"
let GET_PROFILE_DATA    =   BASE_URL + SERVICES_END_POINT + "Passenger/Get"
let UPDATE_USER_PROFILE =   BASE_URL + SERVICES_END_POINT + "Passenger/Update"
let CHANGE_PASSWORD     =   BASE_URL + SERVICES_END_POINT + "User/ChangePassword"
let UPLOAD_IMAGE        =   BASE_URL + "Media/SaveProfilePicture"
let FORGOT_PASSWORD     =   BASE_URL + SERVICES_END_POINT + "Account/ForgotPassword"
let RESET_PASSWORD      =   BASE_URL + SERVICES_END_POINT + "Account/ResetForgotPassword"
let GET_TRIPS           =   BASE_URL + SERVICES_END_POINT + "Trip/GetAllAvailableTrips"

/* Trip Execution MODULE WEB API END POINT */
let CHECK_ONGOING_TRIP  =   BASE_URL + SERVICES_END_POINT + "TripExecution/GetPassengerOngoingTrip"
let TRIP_EXECUTION      =   BASE_URL + SERVICES_END_POINT + "TripExecution/Get"
let UPDATE_DRIVER_LOCATION  = BASE_URL + SERVICES_END_POINT + "TripExecution/GetDriverLastLocation"
let TRIP_HISTORY        =   BASE_URL + SERVICES_END_POINT + "TripExecution/GetTripsHistoryForPassenger"
let UPDATE_PASSENGER_BOARDING_STATUS = BASE_URL + SERVICES_END_POINT + "TripExecution/UpdatePassengerBoardingStatus"
let PAST_SCHEDULE_DETAILS = BASE_URL + SERVICES_END_POINT + "TripExecution/GetTripHistoryForPassengerSpecific"


/* SCHEDULE MODULE WEB API END POINT */

let CREATE_SCHEDULE     =   BASE_URL + SERVICES_END_POINT + "Schedule/Create"
let GET_ALL_SCHEDULE = BASE_URL + SERVICES_END_POINT + "Schedule/GetAll"
let GET_SCHEDULE = BASE_URL + SERVICES_END_POINT + "Schedule/Get"
let UPDATE_SCHEDULE = BASE_URL + SERVICES_END_POINT + "Schedule/Update"
let DELETE_SCHEDULE = BASE_URL + SERVICES_END_POINT + "TripExecution/PassengerCancelTrip"
let GET_UPCOMING_SCHEDULES = BASE_URL + SERVICES_END_POINT + "Schedule/GetUpcomingSchedules"

/* FIREBASE MODULE WEB API END POINT */
let CREATE_TOKEN  = BASE_URL + SERVICES_END_POINT + "Firebase/CreateToken"
let GET_ALL_NOTIFICATION = BASE_URL + SERVICES_END_POINT + "Notification/GetAll"
let DELETE_TOKEN =  BASE_URL + SERVICES_END_POINT + "Firebase/DeleteToken"
let SEND_NOTIFICATION = BASE_URL + SERVICES_END_POINT + "Firebase/SendNotification"

/* REQUEST MODULES WEB API END POINT */

let GET_ALL_REQUEST = BASE_URL + SERVICES_END_POINT + "Request/GetUserRequests"
let GET_REQUEST_DETAILS = BASE_URL + SERVICES_END_POINT + "Request/GetUserRequestSpecific"
let DELETE_REQUEST_BY_USER = BASE_URL + SERVICES_END_POINT + "Request/DeleteRequest"
let SEND_MESSAGE = BASE_URL + SERVICES_END_POINT + "Request/SendMessage"


/* /api/services/app/CompanyEntity/GetAll*/
let GET_ENTITY = BASE_URL + SERVICES_END_POINT + "CompanyEntity/GetAll"
let GET_DEPARTMENT = BASE_URL + SERVICES_END_POINT + "Department/GetAll"


// Setting App End point

let GET_SETTING_MOBILE =    BASE_URL + SERVICES_END_POINT + "Setting/GetSettingsForMobile"
let GET_TOGGLE_SETTING =    BASE_URL + SERVICES_END_POINT + "Setting/ToggleSetting"




let db = Firestore.firestore()
