//
//  main.swift
//  Fleet Management
//
//  Created by Atta khan on 06/05/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

UIApplicationMain(
CommandLine.argc,
UnsafeMutableRawPointer(CommandLine.unsafeArgv)
    .bindMemory(
        to: UnsafeMutablePointer<Int8>.self,
        capacity: Int(CommandLine.argc)),
NSStringFromClass(TimerUIApplication.self),
NSStringFromClass(AppDelegate.self)
)
