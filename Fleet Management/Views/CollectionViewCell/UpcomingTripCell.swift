//
//  UpcomingTripCell.swift
//  Fleet Management
//
//  Created by Atta khan on 28/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit

class UpcomingTripCell: UICollectionViewCell {
    @IBOutlet weak var detailBtn: UIButton!
    @IBOutlet weak var pickUpRideLbl: UILabel!
    @IBOutlet weak var pickUpTimeLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var endingAddressLbl: UILabel!
    @IBOutlet weak var startAddressLbl: UILabel!
    @IBOutlet weak var routeIdLbl: UILabel!
    @IBOutlet weak var vehicleLbl: UILabel!
    
    
    var data: ScheduleItems? {
        didSet {
         guard let item = data else { return }
            if let driverName = item.trip?.driverName, let driveSurName = item.trip?.driverSurname {
                userNameLbl.text = driverName + " " + driveSurName
            }
            if let date = item.tripDate?.toDateString()?.toString(format: "EEEE d MMMM yyyy"), let time = item.tripTime?.toDateString()?.toString(format: "hh:mm a") {
            if let tripType = item.trip?.type {
                 if tripType == .DROP {
                     pickUpRideLbl.text = "Drop Off Ride : \(time)"
                 } else {
                     pickUpRideLbl.text = "PickUp Ride : \(time)"
                 }
             }
            pickUpTimeLbl.text = date
         }
            
            if let make = item.trip?.vehicle?.make, let model = item.trip?.vehicle?.model, let registrationNumber = item.trip?.vehicle?.registrationNumber {
                vehicleLbl.text = make + " " + model + " " + registrationNumber
            }
            if let routeId = item.routeId, let tripId = item.trip?.id {
                routeIdLbl.text = "\(tripId)"
            }
            if let startAddress = item.trip?.tripStartAddress, let endAddress = item.trip?.tripEndAddress {
                endingAddressLbl.text = endAddress
                startAddressLbl.text =  startAddress
            }
            
            if let driverImg = item.driver?.profilePicture {
                let img = BASE_URL_MEDIA + driverImg
                userImgView.setImage(with: img, placeholder: UIImage(named: "portraituser"))
            }
        }
    }
}
