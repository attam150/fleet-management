//
//  UpcomingTripViewCell.swift
//  Fleet Management
//
//  Created by Atta khan on 28/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit

class UpcomingTripViewCell: UICollectionViewCell {
    static var identifier: String{
        return String(describing: self)
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
