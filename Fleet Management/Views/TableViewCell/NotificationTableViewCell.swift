//
//  NotificationTableViewCell.swift
//  Fleet Management
//
//  Created by Atta Khan on 20/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var messageLbl: UILabel!
    
    @IBOutlet weak var tripImgView: UIImageView!
    @IBOutlet weak var indicatorImgView: UIImageView!
    @IBOutlet weak var usernamelbl: UILabel!
    
    @IBOutlet weak var tripLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    static var identifier: String{
        return String(describing: self)
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var data: NotificationItems? {
        didSet {
         guard let item = data else { return }
            if item.userType == 1 {
                messageLbl.textColor = UIColor.rgb(red: 42, green: 127, blue: 246, alpha: 1)
                usernamelbl.text = "Admin"
                indicatorImgView.image = UIImage(named: "blue")
            } else {
                messageLbl.textColor = UIColor.rgb(red: 34, green: 35, blue: 36, alpha: 1)
                usernamelbl.text = item.username
                indicatorImgView.image = UIImage(named: "black")
            }
            messageLbl.text = item.message
            
         if let date = item.creationTime?.toDateString()?.toString(format: "dd-MM-yyyy"), let time = item.creationTime?.toDateString()?.toString(format: "hh:mm a") {
             let dateTime = date + time
             print(dateTime)
             dateLbl.text = date
         }
            if let trip_id = item.tripId {
                tripLbl.text = "\(trip_id)"
                tripImgView.isHidden = false
            } else {
                tripImgView.isHidden = true
            }
            
            
        }
    }
    
    
}
