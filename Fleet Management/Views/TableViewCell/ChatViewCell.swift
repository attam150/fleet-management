//
//  ChatViewCell.swift
//  Fleet Management
//
//  Created by Atta khan on 31/08/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit

class ChatViewCell: UITableViewCell {

    var messageTextView: UITextView = {
        let txView = UITextView()
        txView.text = "some text goes here...."
        return txView
    }()
    
    var bubbleView: UIView = {
        let view = UIView()
        view.heightAnchor.constraint(equalToConstant: 50)
        return view
    }()

    var bubbleViewRightAnchor: NSLayoutConstraint?
    var bubbleViewLeftAnchor: NSLayoutConstraint?
    var bubbleWidthAnchor: NSLayoutConstraint!
    
    var timeLbl: UILabel = {
        let label = UILabel()
        return label
    }()
    
    var bubbleViewLeading: NSLayoutConstraint?
    var bubbleViewTrailing: NSLayoutConstraint?
    var bubbleViewWidth: NSLayoutConstraint?
    static var identifier: String{
        return String(describing: self)
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        addSubview(messageTextView)
        addSubview(bubbleView)
        addSubview(timeLbl)
        messageTextView.isEditable = false
        messageTextView.translatesAutoresizingMaskIntoConstraints = false
        bubbleView.translatesAutoresizingMaskIntoConstraints = false
        timeLbl.translatesAutoresizingMaskIntoConstraints = false
        timeLbl.isHidden = true
        
        bubbleViewLeading = bubbleView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20)
        bubbleViewLeading?.isActive = true
        
        bubbleViewTrailing = bubbleView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20)
        bubbleViewTrailing?.isActive = false
        
        bubbleView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16).isActive = true
        bubbleView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -16).isActive = true
        bubbleViewWidth = bubbleView.widthAnchor.constraint(equalToConstant: 250)
        bubbleViewWidth?.isActive = true
        // x,y, w, h
        
        messageTextView.leadingAnchor.constraint(equalTo: self.bubbleView.leadingAnchor, constant: 8).isActive = true
        messageTextView.trailingAnchor.constraint(equalTo: self.bubbleView.trailingAnchor, constant: -8).isActive = true
        messageTextView.topAnchor.constraint(equalTo: self.bubbleView.topAnchor, constant: 4).isActive = true
        messageTextView.bottomAnchor.constraint(equalTo: self.bubbleView.bottomAnchor, constant: 0).isActive = true
        
        timeLbl.trailingAnchor.constraint(equalTo: self.bubbleView.trailingAnchor, constant: -8).isActive = true
        timeLbl.bottomAnchor.constraint(equalTo: self.bubbleView.bottomAnchor, constant: 16).isActive = true
        //timeLbl.centerYAnchor.constraint(equalTo: self.adminMsgIcon.centerYAnchor, constant: 0).isActive = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
