//
//  ScheduleTableViewCell.swift
//  Fleet Management
//
//  Created by Atta Khan on 20/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit

class ScheduleTableViewCell: UITableViewCell {
    static var identifier: String{
        return String(describing: self)
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    @IBOutlet weak var routeLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var startTimeLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        statusLbl.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var past_data: PastScheduleItems? {
        didSet {
         guard let item = past_data else { return }
         userNameLbl.text = item.driverName
            let type = item.tripType
            if type == .PICKUP {
                if let date = item.pickupTime?.toDateString()?.toString(format: "dd-MM-yyyy"), let time = item.pickupTime?.toDateString()?.toString(format: "hh:mm a") {
                    startTimeLbl.text = date + " | " + time
                }
            } else {
                if let date = item.dropTime?.toDateString()?.toString(format: "dd-MM-yyyy"), let time = item.pickupTime?.toDateString()?.toString(format: "hh:mm a") {
                    startTimeLbl.text = date + " | " + time
                }
            }
            
         
            if let routeId = item.routeId, let tripId = item.tripId {
             routeLbl.text = "Route Id: \(routeId) | Trip Id: \(tripId)"
         }
         
            if let status = item.status {
                statusLbl.isHidden = false
                switch status {
                case .INPROGRESS:
                    statusLbl.text = "In progress"
                    statusLbl.textColor = UIColor.init(rgb: 0x2A7FF6)
                case .CANCELLED:
                    statusLbl.text = "Cancelled"
                    statusLbl.textColor = UIColor.init(rgb: 0xF82B47)
                case .DRIVERCANCELLED:
                    statusLbl.text = "Cancelled by driver"
                    statusLbl.textColor = UIColor.init(rgb: 0xF82B47)
                case .COMPLETED:
                    statusLbl.text = "Completed"
                    statusLbl.textColor = UIColor.init(rgb: 0x0BA869)
                default:
                    statusLbl.isHidden = true
                    break
                }
                
            }
            
            
        }
    }
    var  data: ScheduleItems? {
           didSet {
            guard let item = data else { return }
            userNameLbl.text = item.name
            statusLbl.isHidden = true
            if let date = item.tripDate?.toDateString()?.toString(format: "dd-MM-yyyy"), let time = item.tripTime?.toDateString()?.toString(format: "hh:mm a") {
                let dateTime = date + time
                print(dateTime)
                startTimeLbl.text = date + " | " + time
            }
            if let routeId = item.routeId, let tripId = item.trip?.id {
                routeLbl.text = "Route Id: \(routeId) | Trip Id: \(tripId)"
            }
           }
       }
    
}
