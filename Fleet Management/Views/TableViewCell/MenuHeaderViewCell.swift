//
//  MenuHeaderViewCell.swift
//  Fleet Management
//
//  Created by Atta Khan on 02/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit

class MenuHeaderViewCell: UITableViewCell {
    
    @IBOutlet weak var userImg: UIImageView!
    
    @IBOutlet weak var usernameLbl: UILabel!
    
    @IBOutlet weak var tripIdLbl: UILabel!
    
    @IBOutlet weak var editprofileBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
