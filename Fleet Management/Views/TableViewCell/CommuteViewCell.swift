//
//  CommuteViewCell.swift
//  Fleet Management
//
//  Created by Atta khan on 13/05/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit

class CommuteViewCell: UITableViewCell {
    
    
    @IBOutlet weak var scheduleDateLbl: UILabel!
    
    @IBOutlet weak var scheduleStatusLbl: UILabel!
    
    @IBOutlet weak var mondayBtn: UIButton!
    @IBOutlet weak var thuesdayBtn: UIButton!
    @IBOutlet weak var wedensdayBtn: UIButton!
    @IBOutlet weak var thursdayBtn: UIButton!
    @IBOutlet weak var fridayBtn: UIButton!
    @IBOutlet weak var saturdayBtn: UIButton!
    @IBOutlet weak var sundayBtn: UIButton!
    
    
    static var identifier: String{
        return String(describing: self)
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        mondayBtn.isHidden      =   true
        thuesdayBtn.isHidden    =   true
        wedensdayBtn.isHidden   =   true
        thursdayBtn.isHidden    =   true
        fridayBtn.isHidden      =   true
        saturdayBtn.isHidden    =   true
        sundayBtn.isHidden      =   true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var data: ScheduleResult? {
        didSet {
            guard let item = data else { return }
            if let startDate = item.scheduleStartDate?.toDateString()?.toString(format: "dd-MM-yyyy"), let endDate = item.scheduleEndDate?.toDateString()?.toString(format: "dd-MM-yyyy") {
                scheduleDateLbl.text = startDate + " | " + endDate
            }
            
            
            if let status = item.scheduleStatus {
                switch status {
                case .PENDING:
                    scheduleStatusLbl.text = "Pending"
                case .APPROVED:
                    scheduleStatusLbl.text = "Approved"
                default:
                    scheduleStatusLbl.text = ""
                }
            }
            if let scheduleDays = item.scheduleSpecifics {
                if scheduleDays.count == 1 {
                    mondayBtn.isHidden      =   false
                    thuesdayBtn.isHidden    =   true
                    wedensdayBtn.isHidden   =   true
                    thursdayBtn.isHidden    =   true
                    fridayBtn.isHidden      =   true
                    saturdayBtn.isHidden    =   true
                    sundayBtn.isHidden      =   true
                    let weekday = scheduleDays[0].weekday
                    let dayName = WeekDay(rawValue: weekday)
                    let name = dayName?.getDayShortName()
                    mondayBtn.setTitle(name, for: .normal)
                } else if scheduleDays.count == 2 {
                    mondayBtn.isHidden      =   false
                    thuesdayBtn.isHidden    =   false
                    wedensdayBtn.isHidden   =   true
                    thursdayBtn.isHidden    =   true
                    fridayBtn.isHidden      =   true
                    saturdayBtn.isHidden    =   true
                    sundayBtn.isHidden      =   true
                    let weekday1 = scheduleDays[0].weekday
                    let dayName1 = WeekDay(rawValue: weekday1)
                    let name1 = dayName1?.getDayShortName()
                    mondayBtn.setTitle(name1, for: .normal)
                    let weekday2 = scheduleDays[1].weekday
                    let dayName2 = WeekDay(rawValue: weekday2)
                    let name2 = dayName2?.getDayShortName()
                    thuesdayBtn.setTitle(name2, for: .normal)
                }  else if scheduleDays.count == 3 {
                    mondayBtn.isHidden      =   false
                    thuesdayBtn.isHidden    =   false
                    wedensdayBtn.isHidden   =   false
                    thursdayBtn.isHidden    =   true
                    fridayBtn.isHidden      =   true
                    saturdayBtn.isHidden    =   true
                    sundayBtn.isHidden      =   true
                    let weekday1 = scheduleDays[0].weekday
                    let dayName1 = WeekDay(rawValue: weekday1)
                    let name1 = dayName1?.getDayShortName()
                    mondayBtn.setTitle(name1, for: .normal)
                    let weekday2 = scheduleDays[1].weekday
                    let dayName2 = WeekDay(rawValue: weekday2)
                    let name2 = dayName2?.getDayShortName()
                    thuesdayBtn.setTitle(name2, for: .normal)
                    let weekday3 = scheduleDays[2].weekday
                    let dayName3 = WeekDay(rawValue: weekday3)
                    let name3 = dayName3?.getDayShortName()
                    wedensdayBtn.setTitle(name3, for: .normal)
                }  else if scheduleDays.count == 4 {
                    mondayBtn.isHidden      =   false
                    thuesdayBtn.isHidden    =   false
                    wedensdayBtn.isHidden   =   false
                    thursdayBtn.isHidden    =   false
                    fridayBtn.isHidden      =   true
                    saturdayBtn.isHidden    =   true
                    sundayBtn.isHidden      =   true
                    let weekday1 = scheduleDays[0].weekday
                    let dayName1 = WeekDay(rawValue: weekday1)
                    let name1 = dayName1?.getDayShortName()
                    mondayBtn.setTitle(name1, for: .normal)
                    let weekday2 = scheduleDays[1].weekday
                    let dayName2 = WeekDay(rawValue: weekday2)
                    let name2 = dayName2?.getDayShortName()
                    thuesdayBtn.setTitle(name2, for: .normal)
                    let weekday3 = scheduleDays[2].weekday
                    let dayName3 = WeekDay(rawValue: weekday3)
                    let name3 = dayName3?.getDayShortName()
                    wedensdayBtn.setTitle(name3, for: .normal)
                    let weekday4 = scheduleDays[3].weekday
                    let dayName4 = WeekDay(rawValue: weekday4)
                    let name4 = dayName4?.getDayShortName()
                    thursdayBtn.setTitle(name4, for: .normal)
                }  else if scheduleDays.count == 5 {
                    mondayBtn.isHidden      =   false
                    thuesdayBtn.isHidden    =   false
                    wedensdayBtn.isHidden   =   false
                    thursdayBtn.isHidden    =   false
                    fridayBtn.isHidden      =   false
                    saturdayBtn.isHidden    =   true
                    sundayBtn.isHidden      =   true
                    let weekday1 = scheduleDays[0].weekday
                    let dayName1 = WeekDay(rawValue: weekday1)
                    let name1 = dayName1?.getDayShortName()
                    mondayBtn.setTitle(name1, for: .normal)
                    let weekday2 = scheduleDays[1].weekday
                    let dayName2 = WeekDay(rawValue: weekday2)
                    let name2 = dayName2?.getDayShortName()
                    thuesdayBtn.setTitle(name2, for: .normal)
                    let weekday3 = scheduleDays[2].weekday
                    let dayName3 = WeekDay(rawValue: weekday3)
                    let name3 = dayName3?.getDayShortName()
                    wedensdayBtn.setTitle(name3, for: .normal)
                    let weekday4 = scheduleDays[3].weekday
                    let dayName4 = WeekDay(rawValue: weekday4)
                    let name4 = dayName4?.getDayShortName()
                    thursdayBtn.setTitle(name4, for: .normal)
                    let weekday5 = scheduleDays[4].weekday
                    let dayName5 = WeekDay(rawValue: weekday5)
                    let name5 = dayName5?.getDayShortName()
                    fridayBtn.setTitle(name5, for: .normal)
                }  else if scheduleDays.count == 6 {
                    mondayBtn.isHidden      =   false
                    thuesdayBtn.isHidden    =   false
                    wedensdayBtn.isHidden   =   false
                    thursdayBtn.isHidden    =   false
                    fridayBtn.isHidden      =   false
                    saturdayBtn.isHidden    =   false
                    sundayBtn.isHidden      =   true
                    let weekday1 = scheduleDays[0].weekday
                    let dayName1 = WeekDay(rawValue: weekday1)
                    let name1 = dayName1?.getDayShortName()
                    mondayBtn.setTitle(name1, for: .normal)
                    let weekday2 = scheduleDays[1].weekday
                    let dayName2 = WeekDay(rawValue: weekday2)
                    let name2 = dayName2?.getDayShortName()
                    thuesdayBtn.setTitle(name2, for: .normal)
                    let weekday3 = scheduleDays[2].weekday
                    let dayName3 = WeekDay(rawValue: weekday3)
                    let name3 = dayName3?.getDayShortName()
                    wedensdayBtn.setTitle(name3, for: .normal)
                    let weekday4 = scheduleDays[3].weekday
                    let dayName4 = WeekDay(rawValue: weekday4)
                    let name4 = dayName4?.getDayShortName()
                    thursdayBtn.setTitle(name4, for: .normal)
                    let weekday5 = scheduleDays[4].weekday
                    let dayName5 = WeekDay(rawValue: weekday5)
                    let name5 = dayName5?.getDayShortName()
                    fridayBtn.setTitle(name5, for: .normal)
                    let weekday6 = scheduleDays[5].weekday
                    let dayName6 = WeekDay(rawValue: weekday6)
                    let name6 = dayName6?.getDayShortName()
                    saturdayBtn.setTitle(name6, for: .normal)
                }  else if scheduleDays.count == 7 {
                    mondayBtn.isHidden      =   false
                    thuesdayBtn.isHidden    =   false
                    wedensdayBtn.isHidden   =   false
                    thursdayBtn.isHidden    =   false
                    fridayBtn.isHidden      =   false
                    saturdayBtn.isHidden    =   false
                    sundayBtn.isHidden      =   false
                    let weekday1 = scheduleDays[0].weekday
                    let dayName1 = WeekDay(rawValue: weekday1)
                    let name1 = dayName1?.getDayShortName()
                    mondayBtn.setTitle(name1, for: .normal)
                    let weekday2 = scheduleDays[1].weekday
                    let dayName2 = WeekDay(rawValue: weekday2)
                    let name2 = dayName2?.getDayShortName()
                    thuesdayBtn.setTitle(name2, for: .normal)
                    let weekday3 = scheduleDays[2].weekday
                    let dayName3 = WeekDay(rawValue: weekday3)
                    let name3 = dayName3?.getDayShortName()
                    wedensdayBtn.setTitle(name3, for: .normal)
                    let weekday4 = scheduleDays[3].weekday
                    let dayName4 = WeekDay(rawValue: weekday4)
                    let name4 = dayName4?.getDayShortName()
                    thursdayBtn.setTitle(name4, for: .normal)
                    let weekday5 = scheduleDays[4].weekday
                    let dayName5 = WeekDay(rawValue: weekday5)
                    let name5 = dayName5?.getDayShortName()
                    fridayBtn.setTitle(name5, for: .normal)
                    let weekday6 = scheduleDays[5].weekday
                    let dayName6 = WeekDay(rawValue: weekday6)
                    let name6 = dayName6?.getDayShortName()
                    saturdayBtn.setTitle(name6, for: .normal)
                    let weekday7 = scheduleDays[6].weekday
                    let dayName7 = WeekDay(rawValue: weekday7)
                    let name7 = dayName7?.getDayShortName()
                    sundayBtn.setTitle(name7, for: .normal)

                }
            }
        }
    }
    
}
