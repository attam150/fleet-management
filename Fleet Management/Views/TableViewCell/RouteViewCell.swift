//
//  RouteViewCell.swift
//  Fleet Management
//
//  Created by Atta Khan on 15/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit

class RouteViewCell: UITableViewCell {

    @IBOutlet weak var tickBtn: UIButton!
    @IBOutlet weak var endingLbl: UILabel!
    @IBOutlet weak var startingLbl: UILabel!
    @IBOutlet weak var routeImg: UIImageView!
    @IBOutlet weak var dropOffLocation: UILabel!
    @IBOutlet weak var startingAddressLbl: UILabel!
    @IBOutlet weak var idLbl: UILabel!
    @IBOutlet weak var routeLbl: UILabel!
    static var identifier: String{
        return String(describing: self)
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            contentView.backgroundColor = UIColor.themeColor
            endingLbl.textColor = UIColor.white
            startingLbl.textColor = UIColor.white
            routeLbl.textColor = UIColor.white
            idLbl.textColor = UIColor.white
            startingAddressLbl.textColor = UIColor.white
            dropOffLocation.textColor = UIColor.white
            routeImg.image = UIImage(named: "group710")
            tickBtn.isHidden = false
        } else {
            contentView.backgroundColor = UIColor.clear
            routeLbl.textColor = UIColor.themeLblColor
            idLbl.textColor = UIColor.lightGray
            startingAddressLbl.textColor = UIColor.themeLblColor
            dropOffLocation.textColor = UIColor.themeLblColor
            endingLbl.textColor = UIColor.lightGray
            startingLbl.textColor = UIColor.lightGray
            routeImg.image = UIImage(named: "group707")
            tickBtn.isHidden = true
        }
        // Configure the view for the selected state
    }
    var data: Route? {
        didSet {
            guard let item = data else { return }
            
            routeLbl.text = item.name
            idLbl.text = "\(item.id!)"
            
            startingAddressLbl.text = item.startingAddress
            dropOffLocation.text = item.endingAddress
            
            
            
        }
    }

    var trip_data: Trip? {
        didSet {
            guard let item = trip_data else { return }
            
            routeLbl.text = item.name
            idLbl.text = item.startTime?.toDateString()?.toString(format: "hh:mm a")
            
            startingAddressLbl.text = item.tripStartAddress
            dropOffLocation.text = item.tripEndAddress
            
            
            
        }
    }
}
