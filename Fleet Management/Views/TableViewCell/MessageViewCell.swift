//
//  MessageViewCell.swift
//  Fleet Management
//
//  Created by Atta khan on 10/06/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit

class MessageViewCell: UITableViewCell {

    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var bubbleView: UIView!
    @IBOutlet weak var userIcon: UIImageView!
    @IBOutlet weak var userLbl: UILabel!
    @IBOutlet weak var adminMsgIcon: UIImageView!
    @IBOutlet weak var adminLbl: UILabel!
    @IBOutlet weak var bubbleViewRightAnchor: NSLayoutConstraint?
    @IBOutlet weak var bubbleViewLeftAnchor: NSLayoutConstraint?
    @IBOutlet weak var bubbleWidthAnchor: NSLayoutConstraint!
    
    @IBOutlet weak var timeLbl: UILabel!
    
    var bubbleViewLeading: NSLayoutConstraint?
    var bubbleViewTrailing: NSLayoutConstraint?
    var bubbleViewWidth: NSLayoutConstraint?
    static var identifier: String{
        return String(describing: self)
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        messageTextView.isEditable = false
        messageTextView.translatesAutoresizingMaskIntoConstraints = false
        bubbleView.translatesAutoresizingMaskIntoConstraints = false
        userIcon.translatesAutoresizingMaskIntoConstraints = false
        userLbl.translatesAutoresizingMaskIntoConstraints = false
        adminMsgIcon.translatesAutoresizingMaskIntoConstraints = false
        adminLbl.translatesAutoresizingMaskIntoConstraints = false
        timeLbl.translatesAutoresizingMaskIntoConstraints = false
        timeLbl.isHidden = true
        adminMsgIcon.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20).isActive = true
        adminMsgIcon.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16).isActive = true
        
        
        adminLbl.leadingAnchor.constraint(equalTo: adminMsgIcon.trailingAnchor, constant: 8).isActive = true
        adminLbl.centerYAnchor.constraint(equalTo: adminMsgIcon.centerYAnchor).isActive = true
     
        userIcon.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20).isActive = true
        userIcon.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16).isActive = true
        
        userLbl.trailingAnchor.constraint(equalTo: userIcon.trailingAnchor, constant: -16).isActive = true
        userLbl.centerYAnchor.constraint(equalTo: userIcon.centerYAnchor).isActive = true
        
        
        bubbleViewLeading = bubbleView.leadingAnchor.constraint(equalTo: self.adminMsgIcon.leadingAnchor, constant: 0)
        bubbleViewLeading?.isActive = true
        
        bubbleViewTrailing = bubbleView.trailingAnchor.constraint(equalTo: self.userIcon.trailingAnchor)
        bubbleViewTrailing?.isActive = false
        
        bubbleView.topAnchor.constraint(equalTo: self.adminMsgIcon.bottomAnchor, constant: 8).isActive = true
        bubbleView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -16).isActive = true
        bubbleViewWidth = bubbleView.widthAnchor.constraint(equalToConstant: 250)
        bubbleViewWidth?.isActive = true
        // x,y, w, h
        
        messageTextView.leadingAnchor.constraint(equalTo: self.bubbleView.leadingAnchor, constant: 8).isActive = true
        messageTextView.trailingAnchor.constraint(equalTo: self.bubbleView.trailingAnchor, constant: -8).isActive = true
        messageTextView.topAnchor.constraint(equalTo: self.bubbleView.topAnchor, constant: 4).isActive = true
        messageTextView.bottomAnchor.constraint(equalTo: self.bubbleView.bottomAnchor, constant: 0).isActive = true
        
        timeLbl.trailingAnchor.constraint(equalTo: self.bubbleView.trailingAnchor, constant: -8).isActive = true
        timeLbl.bottomAnchor.constraint(equalTo: self.bubbleView.bottomAnchor, constant: 16).isActive = true
        //timeLbl.centerYAnchor.constraint(equalTo: self.adminMsgIcon.centerYAnchor, constant: 0).isActive = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
