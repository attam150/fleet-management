//
//  MenuViewCell.swift
//  Fleet Management
//
//  Created by Atta Khan on 02/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit

class MenuViewCell: UITableViewCell {
    
    
    @IBOutlet weak var menuImg: UIImageView!
    @IBOutlet weak var menuLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
