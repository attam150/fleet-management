//
//  RequestTableViewCell.swift
//  Fleet Management
//
//  Created by Atta Khan on 20/04/2020.
//  Copyright © 2020 Atta Khan. All rights reserved.
//

import UIKit

class RequestTableViewCell: UITableViewCell {
    
    @IBOutlet weak var requestDetailsLbl: UILabel!
    @IBOutlet weak var requestIdLbl: UILabel!
    
    @IBOutlet weak var statusBtn: UIButton!
    
    @IBOutlet weak var remvoeBtn: UIButton!
    
    @IBOutlet weak var tapBtn: UIButton!
    
    static var identifier: String{
        return String(describing: self)
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
